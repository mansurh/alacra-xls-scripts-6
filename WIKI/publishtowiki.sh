#!/bin/ksh
dest=localhost
SQLFILE="load.sql"

rm -f $SQLFILE

cat >$SQLFILE <<EOF
USE wikidb_public;
EOF

dumptables="archive blobs brokenlinks categorylinks cur hitcounter image imagelinks interwiki ipblocks links linkscc logging math old oldimage searchindex user user_newtalk user_rights validate watchlist site_stats"
cachetables="querycache objectcache"

echo "Dumping from source DB"

# now dump to the file
mysqldump --host=alacramonitor.alacra.com --user=wikidb --password=alacra wikidb ${dumptables} >>${SQLFILE}

mysqldump --no-data --host=alacramonitor.alacra.com --user=wikidb --password=alacra wikidb ${cachetables} >>${SQLFILE}

echo "Loading into public DB on $dest"
mysql --host=${dest} <${SQLFILE}


