isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=12\
 updatetext xls..tearsheet.contents @ptr 0 NULL \
'TearSheet Frame outer_shell;\
TearSheet Frame inner_shell;\
TearSheet Label header;\
header setStyle TITLE ;\
header setText \"BMS WW MEDICINES GROUP\";\
TearSheet Label header1;\
header1 setStyle TITLE ;\
header1 setText \"Competitor Profiles\";\
TearSheet Label header2;\
header2 setStyle TITLE ;\
header2 setText \"Financial Summary\";\
TearSheet Label footer;\
footer setStyle FOOTER ;\
footer setText \"Copyright, 1998 Data Downlink Corporation\";\
TearSheet MGFSPctChg pct;\
inner_shell add pct header2 0;\
'"


isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=12\
 updatetext xls..tearsheet.contents @ptr NULL 0 '\
outer_shell add header 0 0;\
outer_shell add header1 header 0;\
outer_shell add header2 header1 0;\
outer_shell add inner_shell header2 0;\
outer_shell add footer inner_shell 0;\
outer_shell break;\
SS columnWidth 0 32;\
SS columnWidth 1 10;\
SS columnWidth 2 10;\
SS columnWidth 3 10;\
SS columnWidth 4 10;\
SS columnWidth 5 10;\
SS columnWidth 6 10;\
SS columnWidth 7 5;\
SS columnWidth 8 10;\
SS columnWidth 9 10;\
pct setIndustrySIC 2834;\
foreach company \$companyIDs {\
pct setCompany \$company;\
Render outer_shell [SS maxRow] 0;};\
SS output;'"
