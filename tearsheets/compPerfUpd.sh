isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=15\
 updatetext xls..tearsheet.contents @ptr 0 NULL \
'TearSheet Frame outer_shell;\
TearSheet Frame inner_shell;\
TearSheet Label header;\
header setStyle TITLE ;\
header setText \"BMS WW Medicines Group\";\
TearSheet Label header1;\
header1 setStyle TITLE ;\
header1 setText \"Competitor Profiles\";\
TearSheet Label header2;\
header2 setStyle TITLE ;\
header2 setText \"Performance Measures\";\
TearSheet Label footer; \
footer setStyle FOOTER ; \
footer setText \"Copyright, 1998 Data Downlink Corporation\";\
SS columnWidth 0 32;\
SS columnWidth 1 12;\
SS columnWidth 2 12;\
SS columnWidth 3 12;\
SS columnWidth 4 12;\
SS columnWidth 5 12;\
SS columnWidth 6 12;\
SS columnWidth 7 12;\
'"

isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=15\
 updatetext xls..tearsheet.contents @ptr NULL 0 \
'\
TearSheet NelsonProfile profile; \
profile switchField address off; \
profile switchField phone off; \
profile switchField fax off; \
profile switchField ticker off; \
profile switchField exchange off; \
profile switchField shares off; \
profile switchField industry off; \
profile switchField marketCap off; \
profile switchField following off; \
profile switchField currency off; \
profile switchField url off; \
profile switchField description off; \
'"


isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=15\
 updatetext xls..tearsheet.contents @ptr NULL 0 \
'\
TearSheet MGFSRatios ratios;\
ratios setSortOrder asc;\
TearSheet MGFSStats stats;\
stats switchField Latest_Close off;\
stats switchField %_of_52-Week_High off;\
stats switchField Liquidity_Ratio_(000) off;\
stats switchField Number_Instit._Holding_Shares off;\
stats switchField Shares_Held_by_Instit._% off;\
stats switchField Latest_12_Mos._EPS_Change_% off;\
stats switchField 5-Yr_Revenue_Growth,_% off;\
stats switchField 5-Yr_EPS_Growth_Rate,_% off;\
stats switchField 5-Yr_Dividend_Growth_Rate,_% off;\
stats switchField Current_Dividend_Rate off;\
stats switchField Current_Dividend_Yield off;\
stats switchField Beta_(60_Month) off;\
TearSheet MGFSIndPct indPct;\
TearSheet Label statsHeader;\
statsHeader setStyle TITLE ;\
statsHeader setText \"Stock/Dividend Information\";\
TearSheet Label indPctHeader;\
indPctHeader setStyle TITLE ;\
indPctHeader setText \"Company as % of Industry\";\
'"

isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=15\
 updatetext xls..tearsheet.contents @ptr NULL 0 \
'\
outer_shell add header 0 0;\
outer_shell add header1 header 0;\
outer_shell add header2 header1 0;\
inner_shell add profile 0 0;\
inner_shell add ratios profile 0;\
inner_shell add statsHeader ratios+1 0;\
inner_shell add stats statsHeader 0;\
inner_shell add indPctHeader stats+1 0;\
inner_shell add indPct indPctHeader 0;\
outer_shell add inner_shell header2 0;\
outer_shell add footer inner_shell 0;\
outer_shell break;\
foreach company \$companyIDs {\
profile setCompany \$company ; \
ratios setCompany \$company ; \
stats setCompany \$company ; \
indPct setCompany \$company ; \
Render outer_shell [SS maxRow] 0; \
};\
SS output; '"



