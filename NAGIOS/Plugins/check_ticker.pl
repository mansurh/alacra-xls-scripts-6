#!/usr/bin/perl

use strict;

#use DateTime;
use Date::Calc qw(:all);

my $hostname = undef;
if ($#ARGV >= 0) {
	$hostname = $ARGV[0];
}

my $eventtype = "1";
if ($#ARGV >= 1) {
	$eventtype = $ARGV[1];
}

my $seconds_to_stale = 1800;
if ($#ARGV >= 2) {
	$seconds_to_stale = $ARGV[2];
}

use LWP::Simple;
my $url = "http://$hostname/asp/analystcomments/analystcomments.aspx?ptype=traditional&etype=$eventtype&accepts=text/xml";
my $input = get $url;

if ($input =~ /\<serverdate\>([^ ]+) ([^ ]+) (AM|PM)\<\/serverdate\>/) {
	my $datestr = $1;
	my $timestr = $2;
	my $ampm = $3;
	my @datetoks = split(/\//, $datestr);
	my @timetoks = split(/\:/, $timestr);
	if ($ampm eq "PM" && $timetoks[0] != 12) {
		$timetoks[0] = $timetoks[0] + 12;
	}
	my $duration_seconds =$seconds_to_stale +1;
#	my $dt = DateTime->new( year => $datetoks[2],
#		month => $datetoks[0],
#		day => $datetoks[1],
#		hour => $timetoks[0],
#		minute => $timetoks[1],
#		second => $timetoks[2],
#		time_zone => 'America/Chicago'
#	);
#	my $duration = DateTime->now( time_zone => 'America/Chicago' )->subtract_datetime_absolute( $dt );
#	$duration_seconds = $duration->seconds;

	my ($year, $month, $day, $hour, $min, $sec) = Today_and_Now();
	$duration_seconds = (((Date_to_Days($year, $month, $day)*24+$hour)*60+$min)*60+$sec) - (((Date_to_Days($datetoks[2], $datetoks[0], $datetoks[1])*24+$timetoks[0])*60+$timetoks[1])*60+$timetoks[2]);

	if ($duration_seconds <= $seconds_to_stale) {
		print("OK - $hostname $eventtype Cache is $duration_seconds seconds old\n");
		exit 0;
	}else{
		print("Warning - $hostname $eventtype Cache is $duration_seconds seconds old, which older than $seconds_to_stale\n");
		exit 1;
	}
}else{
	print("Failed finding/parsing input for serverdate from $hostname $eventtype\n");
	exit 2;
}

print("Failed, unexpectedly $hostname $eventtype\n");
exit 2;
