XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 2 ]
then
    echo "usage: $0 projname filename.type [countthreashhold] [ftp server] [reg name]"
    exit 1
fi

projname=$1
files=$2
countthreashhold=5
if [ $# -gt 2 ]
then
	typeset -i countthreashhold=$3
fi

regname=$1
if [ $# -gt 4 ]
then
	regname=$5
fi

user=`get_xls_registry_value ${regname} User 6`
pass=`get_xls_registry_value ${regname} Password 6`
ftpserver=`get_xls_registry_value ${regname} Server 6`
if [ $# -gt 3 ]
then
	ftpserver=$4
fi
sftpkey=`get_xls_registry_value ${regname} Key 6`
ftpfolder=`get_xls_registry_value ${regname} Outbound 6`
clienttype=`get_xls_registry_value ${regname} ClientType 6`

usecurl=0
if [ "$clienttype" = "curl" ]; then usecurl=1; fi;

# allow for jobstream name to be in the registry, in case client has multi platforms
jobstreamfile=${projname}_load
regjobstream=`get_xls_registry_value ${regname} Jobstream 6`
if [ "${regjobstream}" != "" ]
then
	jobstreamfile=${regjobstream}
fi


main() {
  filelist=${ftpserver}.${projname}.filelist
  counterfile=${ftpserver}.${projname}.counter
  typeset -i counter=0

  # make sure our download directory exists
  mkdir -p ${XLSDATA}/${projname}/download
  cd $XLSDATA/$projname/download
  if [ $? -ne 0 ]
  then
	echo "$0: error in cd to $XLSDATA/$projname/download, exiting ..." 
	return 3
  fi

  #Go to the root directory
  cd $XLSDATA/$projname/

  #We'll keep the number of failures in the counter file
  if [ ! -e ${counterfile} ]
  then
	counter=1
  else
	typeset -i counter=`head -1 ${counterfile}`
	counter=`expr $counter + 1`
  fi


  #if file exists on server, it might mean the database is being loaded
  # ${filelist} is used as a checkpoint to make sure that new database load doesn't start
  #before old one finished.
  if [ -e ${filelist} ] 
  then
	echo "$0: " `date +%Y%m%d-%H:%M` 
	echo "$0: ${filelist} exists on ${COMPUTERNAME}, exiting ..." 
	echo "Failed to create a filelist for ${projname}"
	return 1
  fi

  # STEP 1: Check for existing filelist file on ftp server. If exists, quit out

  ftpcmdfile=getfilelist.ftp
  rm -f ${ftpcmdfile}
  if [ $usecurl -eq 1 ]
  then
    cmd="curl -s -k -O sftp://${user}:${pass}@${ftpserver}/${ftpfolder}/${filelist}"
  elif [ "${sftpkey}" = "" ]
  then
    echo "user $user" > ${ftpcmdfile}
    echo "$pass" >> ${ftpcmdfile}
    echo "binary" >> ${ftpcmdfile}
	if [ "${ftpfolder}" != "" ]
	then
		echo "cd ${ftpfolder}" >> ${ftpcmdfile}
	fi
    echo "get ${filelist}" >> ${ftpcmdfile}
    echo "quit" >> ${ftpcmdfile}
    cmd="ftp -i -n -s:${ftpcmdfile} ${ftpserver}"
  else
    cmd="curl --key $XLS/src/${sftpkey} -s -k -O sftp://${user}@${ftpserver}/${ftpfolder}/${filelist}"
  fi

  echo "$cmd"
  $cmd

  if [ -s ${filelist} ]
  then
	echo "$0: ${filelist} not empty on ${ftpserver}, exiting ... " 
	rm -f ${filelist}
	return 1
  fi


  # STEP 2: Create the filelist file and put it on the ftp server

  ftpcmdfile=getfilelist2.ftp
  rm -f ${ftpcmdfile}
  if [ $usecurl -eq 1 -a "${pass}" != "" ]
  then
        curl -s -k --list-only "sftp://${user}:${pass}@${ftpserver}/${ftpfolder}/" > ${filelist}.tmp
	if [ $? -ne 0 ]; then echo "error in curl, exiting..."; return 1; fi;
	egrep "$files" ${filelist}.tmp > ${filelist}
	cmd=""
  elif [ "${sftpkey}" = "" ]
  then
    echo "user $user " > ${ftpcmdfile}
    echo "$pass" >> ${ftpcmdfile}
    echo "binary " >> ${ftpcmdfile}
    if [ "${ftpfolder}" != "" ]
    then
	echo "cd ${ftpfolder}" >> ${ftpcmdfile}
    fi
    echo "del ${filelist}" >> ${ftpcmdfile}
    echo "mls ${files} ${filelist}" >> ${ftpcmdfile}
    echo "put ${filelist}" >> ${ftpcmdfile}
    echo "quit" >> ${ftpcmdfile}
    cmd="ftp -i -n -s:${ftpcmdfile} ${ftpserver}"
  else
	curl --key $XLS/src/${sftpkey} -s -k --list-only sftp://${user}@${ftpserver}/${ftpfolder}/ > ${filelist}.tmp
	if [ $? -ne 0 ]; then echo "error in curl, exiting..."; return 1; fi;
	egrep "$files" ${filelist}.tmp > ${filelist}
	cmd=""
  fi

  echo $cmd
  $cmd > ftpput.log 2>&1
  returncode=$?
  cat ftpput.log

  if [ $returncode -ne 0 ]
  then
	echo "$0: " `date +%Y%m%d-%H:%M` 
	echo "$0: error in ${ftpcmdfile} on ${ftpserver}, exiting ... " 
	return $returncode
  fi

  errorstr="(Connection closed by remote host|Access is denied)"

  egrep -q "$errorstr" ftpput.log
  if [ $? -eq 0 -o $returncode -ne 0 ]
  then
	echo "$0: " `date +%Y%m%d-%H:%M` 
	echo "$0: error in uploading to ${ftpserver}, exiting ... " 
	return 1
  fi

  # if no list or an empty list, fail, perhaps try again

  if [ ! -s ${filelist} ] 
  then
	echo "$0: " `date +%Y%m%d-%H:%M` 
	echo "$0: ${filelist} is empty  on ${ftpserver}, exiting ..." 
	rm -f ${filelist}
	echo $counter > ${counterfile}
	#check to see if we failed more than ${countthreashhold} times, if so alert admins (but only every 3rd time)
	if [ $counter -gt ${countthreashhold}  -a `expr $counter % 3` -eq 0 ]
	then
		echo "Error: failed to create files for ${projname} ${counter} times."
	else
		echo "Warning: no files were sent for ${projname}, failed ${counter} times out of ${countthreashhold}."
		return 0
	fi
	return 1
  else
    echo "File List:"
    cat ${filelist}
    echo " "
  fi


  # sftp doesn't support outputting ls or mls to a local file, so we must put the filelist up there in a separate step
  if [ $usecurl -eq 1 ]
  then
    cmd="curl -s -k --list-only sftp://${user}:${pass}@${ftpserver}/${ftpfolder}/ -T ${filelist}"
    $cmd > ftpput.log
    returncode=$?
    cat ftpput.log
    if [ $returncode -ne 0 ]
    then
      echo "$0: " `date +%Y%m%d-%H:%M` 
      echo "$0: error in ${ftpcmdfile} on ${ftpserver}, exiting ... " 
      return $returncode
    fi
  elif [ "${sftpkey}" != "" ]
  then
    cmd="curl --key $XLS/src/${sftpkey} -s -k --list-only sftp://${user}@${ftpserver}/${ftpfolder}/ -T ${filelist}"
    $cmd > ftpput.log
    returncode=$?
    cat ftpput.log
    if [ $returncode -ne 0 ]
    then
      echo "$0: " `date +%Y%m%d-%H:%M` 
      echo "$0: error in ${ftpcmdfile} on ${ftpserver}, exiting ... " 
      return $returncode
    fi
  fi

  #file list was put on ftp server successfully
  #remove the file, so if the loading scripts fail (because one database is offline), our
  #future getfilelist.sh calls don't fail
  rm -f ${filelist}
  rm -f ${counterfile}
  loadjob $XLS/src/scripts/jobstreams/${jobstreamfile}
  return $rc
}

mkdir -p $XLSDATA/$projname/download
logfile=$XLSDATA/$projname/ManageFtp`date +%m%d%H`.log
(main) >> ${logfile} 2>&1
if [ $? -ne 0 ]
then
	blat ${logfile} -t administrators@alacra.com -s "Error in getfilelist.sh for ${projname}" 
	exit 1
fi
exit 0
