if [ $# -lt 3 ]
then
    echo "usage: $0 user password projname [ftp]"
    exit 1
fi

user=$1
pass=$2
projname=$3

ftpserver="ftp.alacra.com"
if [ $# -gt 3 ]
then
	ftpserver=$4
fi

logfile=$XLSDATA/$projname/ManageFtp`date +%m%d`.log
filelist=${ftpserver}.${projname}.filelist

mkdir -p $XLSDATA/$projname/download
cd $XLSDATA/$projname/download
if [ $? -ne 0 ]
then
	echo "$0: error in cd to $XLSDATA/$projname/download, exiting ..." >> ${logfile}
	exit 3
fi

#Go to the root directory
cd $XLSDATA/$projname/

echo "user $user" > getfilelist.ftp
echo "$pass" >> getfilelist.ftp
echo "binary" >> getfilelist.ftp
echo "get ${filelist}" >> getfilelist.ftp
echo "quit" >> getfilelist.ftp

ftp -i -n -s:getfilelist.ftp ${ftpserver} >> ${logfile} 2>&1
if [ $? -ne 0 ]
then
	echo "$0: eror in getting ${filelist} from ${ftpserver} " >> ${logfile}
	exit 1
fi

if [ ! -s ${filelist} ]
then
  echo "$0: ${filelist} is empty on ${ftpserver} exiting ... " >> ${logfile}
  rm -f ${filelist}
  exit 1
fi

awk -f $XLS/src/scripts/ManageFTP/get.awk -v PASS=$pass -v USER=$user ${filelist} > getfiles.ftp
returncode=$?
if [ $returncode -ne 0 ]
then
	echo "$0: error in get.awk, exiting ... " >> ${logfile}
	exit $returncode
fi

cd $XLSDATA/$projname/download
ftp -i -n -s:$XLSDATA/$projname/getfiles.ftp ${ftpserver}

returncode=$?
if [ $returncode -ne 0 ]
then
	echo "$0: error in ftp, exiting ... " >> ${logfile}
	exit $returncode
fi

exit 0