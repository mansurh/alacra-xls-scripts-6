BEGIN {
FS="\t"
OFS=" "
verb="del"
if (DELCMD != "") verb=DELCMD
print "user " USER 
print PASS
print "binary"
if (DIR != "") print "cd " DIR
}

{
str=$1
sub("\r$", "", $1)
print verb " \"" $1 "\""
}

END {
print verb " " FILENAME
print "quit"
}