#!/usr/bin/env python3

import lib.yaml_parser as yaml_parser
import argparse
import os
import pyodbc
import re
# import shutil
import subprocess as sproc
import sys
import tempfile
import zipfile
import zlib
from lib.globals import globals
from lib.colors import colors
from lib.print import cprint
from lib.tasks import create_task
from lib.locker import add_document
from lib.configs import creds


whoami = sproc.check_output(['whoami'])
# whoami = os.environ['USERNAME']
globals.whoami = whoami.decode().strip()
if globals.whoami == 'alacra.net\\administrator':
    print('Running MaxBot in an admin window is not allowed')
    sys.exit(1)



parser = argparse.ArgumentParser(
    prog = 'MaxBot.py',
    description = """
DESCRIPTION:

    Takes yaml-style config file and interprets it, then executes SQL from
    manifest files specified there in the order that they occur in the supplied
    manifest. The tasks are run in the order that they appear in the config
    file. """, 
    epilog = """

  Each level is indented by 2 spaces, no more, no less. No 
  tabs. 
  
  Example config file:

  global:
    path: "$/xls/src/scripts/packageSQL"
    sleeptime: 60
    waitmsg: "Press enter when replication done"
  tasks:
    - wait: "ok go when ready"
    - runsql:
        manifest: "${path}/web/ace/Everglades/sql_manifest.txt"
        wait: "Hit enter when Master has replicated."
    - sleep: ${sleeptime}
    - runsql:
        manifest: "${path}/web/ace/Everglades/sql_manifest_2.txt"
        wait: "Hit enter. This shows up after any group of MASTER files"
    
TASKS:

  Tasks must be specified as a yaml array as above.
  Supported tasks are

  runsql:
    This expects a required parameter "manifest" which should be the path to 
    a text file with a listing of one SQL file per line. Each SQL file should 
    have the format "packagename[__sequence]__database__type[__comment].sql".
    
    It also accepts it's own nested "wait" and/or "sleep" parameter, which is
    for waiting or sleeping after each DATABASE + MASTER batch. 

    For example, if the first runsql command's manifest in the example had the 
    following contents:

      web/ace/everglades__0000__rules__MASTERONLY__DDL_external.sql
      web/ace/everglades__0001__rules__MASTERONLY__DDL_external.sql
      web/ace/everglades__0004__xls__ALLCOPIES__filter.sql
      web/ace/everglades__0002__ace__MASTERONLY__DDL__saved_filter.sql
      web/ace/everglades__0003__ace__ALLCOPIES__SP_saved_filter.sql

    it would run the first two sqls, do a wait, then run the 3rd and 4th files,
    do a wait, and then run the remaining sql file.

    Despite that sequence numbers are allowed, they are not used by the wrapper. 
    Only the order of the files in the manifest matter.
   
  sleep: 
    Sleep for specified number of seconds. 

  wait:
    Wait for user input, with the propmt provided in the yaml file.

BUGS:

  Please report bugs to /dev/null 

""",
    formatter_class = argparse.RawTextHelpFormatter)

parser.add_argument("config_file", help='''
path to config yaml file. See example below
''')
parser.add_argument("-v", "--verbose", help="verbose", action="store_true")
parser.add_argument("-d", "--dry-run", help="dry run: echos the SQL commands it would otherwise run", action="store_true")
parser.add_argument("-nw", "--no-wait", help="Ignores any wait prompts and sleeps that may be specified in the yaml, including implicit ones like the post-master replication wait prompt", action="store_true")
parser.add_argument("-nc", "--no-credentials", help="assume user/pass is db name + 'dbo'. Only respected when moving to dev.", action="store_true")
parser.add_argument("--rerun", help="rerun; runs version with the label of the target environment (rather than the default of running files with the label from the previous environment)", action="store_true")
parser.add_argument("-nls", "--no-label-suffix", help="don't use label suffix when labeling files (old way).", action="store_true")

globals.args = parser.parse_args()
if not globals.args.config_file.startswith('$'):
    globals.args.config_file = sproc.check_output(['cygpath', globals.args.config_file])
    globals.args.config_file = os.path.abspath(globals.args.config_file.decode().strip())


# zfname = tempfile.TemporaryFile(prefix='maxbot_', suffix='.zip').name
zfname = tempfile.TemporaryFile(prefix='maxbot_').name + '.zip'
with zipfile.ZipFile(zfname, mode='w') as zf:
    globals.zipfile = zf
    with tempfile.NamedTemporaryFile(prefix='maxbot_', 
                                     suffix='_' + os.path.split(os.path.dirname(globals.args.config_file))[1] + '.log', 
                                     delete=False, mode='w') as logfile:
        
        globals.set_logfile(logfile)

        if globals.args.dry_run:
            cprint.print("dry-run is on")
        
        if globals.args.no_label_suffix:
            cprint.print("no-label-suffix is ON")

        if globals.args.verbose:
            cprint.print(globals.args)
            cprint.print('Logfile:', logfile.name)
        
        try:
            os.mkdir(globals.tmpbasedir)
        except FileExistsError:
            pass

        yaml_parser.parse()

        globals.get_env()

        globals.get_env_suffix()
        
        tasks = yaml_parser.get_tasks()          
        task_objs = []
        for task in tasks:
            t = create_task(task)
            task_objs.append(t)
            t.init()

        for t in task_objs:
            t.execute()


        cprint.print('Logfile:', logfile.name)
    
    zf.write(globals.logfile.name, compress_type=zipfile.ZIP_DEFLATED)
    print(zf.filename)

if globals.env != 'dev':
    # if not shutil.which('concAddToLocker.exe'):
    #     cprint.warning('Application concAddToLocker.exe not found')
    # else:
    print('Adding log and sql files to locker')
    # filepath_windows = sproc.check_output(['cygpath', '-w', zf.filename])
    # lockerfileid = sproc.check_output(['concAddToLocker.exe', filepath_windows.decode().strip(), 'application/zip', '220781', 'maxbot'])
    # lockerfileid = lockerfileid.decode().strip()

    lockerfileid = add_document(zf.filename)
    print('Logging it to table')
    connection_string = 'DRIVER={{SQL Server}};SERVER={s};DATABASE={db};UID={u};PWD={p}'.format(
        s=creds['dba_server'], db='dba2', u=creds['dba_user'], p=creds['dba_pass']
    )
    # print('Connection string:', connection_string)
    conn = pyodbc.connect(connection_string)
    c = conn.cursor()

    # print('exec sp_maxbot_log {0} {1} {2} {3}'.format(
    #     os.path.split(os.path.dirname(globals.args.config_file))[1], 
    #     lockerfileid,
    #     globals.whoami,
    #     globals.env))

    c.execute("exec sp_maxbot_log ?, ?, ?, ?",
              os.path.split(os.path.dirname(globals.args.config_file))[1], 
              lockerfileid,
              globals.whoami,
              globals.env)
    c.commit()
    # print(colors.RED + 'ENABLE MAXBOT WRITE' + colors.END)

