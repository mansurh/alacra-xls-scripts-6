declare @enddate smalldatetime

/* Move to beginning of the month */
select @startdate = DATEADD(day, -1 * (DATEPART(day, @startdate) -1), @startdate)
select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

/* Calculate the end of the monthly period */
select @enddate = DATEADD (month, 1, @startdate)

select "Month Starting", @startdate

PRINT "Usage by Account, User id"
PRINT "========================="
select 
	p.account, COALESCE(a.name,p.company) "Company", u.userid, p.login, p.last_name, u.description, u.price, u.list_price,  u.access_time, u.project, u.remote_address, i.name, u.usageid
from 
	usage u, users p, ip i, account a
where 
	u.access_time >= @startDate
and
	u.access_time < @endDate
and
	u.userid = p.id
and
	u.ip = i.id
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	p.account = a.id
and
	u.no_charge_flag is null
and
	a.id >= @accountstart
and
	a.id <= @accountend
order by 
	p.account, u.userid, u.access_time

