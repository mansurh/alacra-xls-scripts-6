select "UBS - UK Usage"
select 
	i.ipcode, p.account, p.login, u.project,u.project2,u.project3,u.project4, u.project5,u.access_time,u.description,u.list_price 
from 
	usage u, users p, ip i
where 
	userid in (select id from users where account in (3014,3184) and demo_flag is NULL)
	and 
	access_time >= "January 1, 2003" 
	and 
	access_time < "February 1, 2003"
	and 
	no_charge_flag is null
	and 
	u.userid=p.id
	and
	u.ip = i.id
	and
	i.ipcode in (
		"CARSON",
		"DGA",
		"EXTEL",
		"IBES",
		"ITEXT",
		"SDC",
		"TECH"	
	)

order by 
	i.ipcode, p.account, p.login, u.ip, u.access_time asc


