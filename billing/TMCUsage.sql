select  
u.ip 'Database ID',
i.name 'Database Name',
COALESCE (f.value,'C6UJ9A003M44') 'TMC API ID (Effective User ID)',
p.account 'Alacra Client Account',
a.name 'Name',
u.dataset 'TMC Contributor ID',

case
when CHARINDEX('|', s.ipinfo) > 0
then
   SUBSTRING(s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1)+1, 
	  CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1)+1) - CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1) - 1)
else
   ''
end 'TMC Contributor Name',

p.login 'Alacra Client User Name (login)',

case 
when CHARINDEX('|',s.ipinfo) > 0
then
	SUBSTRING(s.ipinfo,1,CHARINDEX('|',s.ipinfo)-1) 
else
	''
end 'TMC Document ID',

u.description 'Headline',

CASE
WHEN CHARINDEX('Release Date: ',u.description) > 1 THEN SUBSTRING(u.description,CHARINDEX('Release Date: ',u.description)+14,18) 
ELSE ' '
END
'Release Date/Time',

case 
when CHARINDEX('|',s.ipinfo) > 0
then
	SUBSTRING(s.ipinfo, CHARINDEX('|',s.ipinfo)+1,CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1) - CHARINDEX('|',s.ipinfo) - 1) 
else
	''
end 'Pages Purchased',

u.access_time 'Download Date/Time',
u.list_price 'List Price',
u.price 'Alacra Billed Price',

CASE
WHEN (u.no_charge_flag is NULL) THEN ''
ELSE 'Y'
END
'Non-Billable'

from 
usage u JOIN usagespecial s ON u.usageid = s.usage JOIN ip i ON u.ip = i.id
JOIN users p ON u.userid = p.id JOIN account a ON p.account = a.id
LEFT JOIN accounts_flag f ON p.account = f.accountid and f.name='EffectiveUser'
where
u.ip in (168,417,419)
and
u.access_time >= 'February 1, 2011' and u.access_time < 'March 1, 2011'

/* and COALESCE (f.value,'C6UJ9A003M44') in ('C6UJ9A003M42','C6UJ9A003M44') */

order by u.access_time asc

