if @account=2791 or @account=3246
begin
select 
	substring(s.name,1,50)"Vendor", s.name"Sevice", substring(replace(@period,' ',''),1,15)"Period", ' '"Office", convert(char(9),u.access_time,1)"Date",
	substring(u.Project,1,30)"Project", p.login"User ID", p.last_name"Last Name", p.first_name"First Name", substring(i.name,1,50)"File Name", ' '"Quantity",
	u.Description, u.list_price, ' '"Surcharge", u.price, case i.id when 83 then u.vendorprice*0.5 when 99 then u.vendorprice*0.5 when 102 then u.vendorprice*0.5 
	when 79 then u.vendorprice*0.85 when 68 then 0 else u.vendorprice end"displayprice", ' '"TAX",
	case i.id when 68 then 0 else u.vendorprice end"vendorprice"
from 
	usage u, users p, ip i, account a, service s
where 
	a.id = @account
and 
	u.access_time >= @period + ' 1,'+convert(char(4), @year) 
and	
	u.access_time < DATEADD(month,1, @period + ' 1,'+convert(char(4), @year)) 
and 
	u.userid = p.id 
and
	p.service = s.id
and 
	u.ip = i.id 
and 
	u.access_time >= p.commence 
and 
	p.demo_flag is null 
and 
	p.account = a.id 
--and 
--	(p.service is null or p.service = 0 or p.service = 28 or p.service = 33 or p.service = 34 or p.service = 35 or p.service = 36 or p.service = 37 or p.service = 40 or p.service = 41) 
and 
	u.no_charge_flag is null 
order by 
	u.userid, u.access_time
end
else if @account=3526
begin
select 
	substring(s.name,1,50)"Vendor", substring(s.name,1,20)"Sevice", substring(replace(@period,' ',''),1,20)"Period", ' '"Office", convert(char(9),u.access_time,1)"Date",
	substring(u.Project,1,30)"Project",substring(u.Project2,1,30)"Project2",substring(u.Project3,1,30)"Project3",substring(u.Project4,1,30)"Project4", p.login"User ID", p.last_name"Last Name", p.first_name"First Name", substring(i.name,1,50)"File Name", ' '"Quantity",
	u.Description, u.list_price, ' '"Surcharge", u.price, case i.id when 83 then u.vendorprice*0.5 when 99 then u.vendorprice*0.5 when 102 then u.vendorprice*0.5 
	when 79 then u.vendorprice*0.85 when 68 then 0 else u.vendorprice end"displayprice", ' '"TAX",
	case i.id when 68 then 0 else u.vendorprice end"vendorprice"
from 
	usage u, users p, ip i, account a, service s
where 
	a.id = @account
and 
	u.access_time >= @period + ' 1,'+convert(char(4), @year) 
and	
	u.access_time < DATEADD(month,1, @period + ' 1,'+convert(char(4), @year)) 
and 
	u.userid = p.id 
and
	p.service = s.id
and 
	u.ip = i.id 
and 
	u.access_time >= p.commence 
and 
	p.demo_flag is null 
and 
	p.account = a.id 
--and 
--	(p.service is null or p.service = 0 or p.service = 28 or p.service = 33 or p.service = 34 or p.service = 35 or p.service = 36 or p.service = 37 or p.service = 40 or p.service = 41) 
and 
	u.no_charge_flag is null 
order by 
	u.userid, u.access_time
end
else if @account=3548 or @account=3798
begin
select 
	substring(s.name,1,50)"Vendor", s.name"Sevice", replace(@period,' ','')"Period", ' '"Office", convert(char(9),u.access_time,1)"Date",
	substring(u.Project,1,30)"Project", p.login"User ID", p.last_name"Last Name", p.first_name"First Name", substring(i.name,1,50)"File Name", ' '"Quantity",
	u.Description, u.list_price, ' '"Surcharge", u.price,u.displayprice, ' '"TAX",
	u.vendorprice
from 
	usage u, users p, ip i, account a, service s
where 
	a.id = @account
and 
	u.access_time >= @period + ' 1,'+convert(char(4), @year) 
and	
	u.access_time < DATEADD(month,1, @period + ' 1,'+convert(char(4), @year)) 
and 
	u.userid = p.id 
and
	p.service = s.id
and 
	u.ip = i.id 
and 
	u.access_time >= p.commence 
and 
	p.demo_flag is null 
and 
	p.account = a.id 
--and 
--	(p.service is null or p.service = 0 or p.service = 28 or p.service = 33 or p.service = 34 or p.service = 35 or p.service = 36 or p.service = 37 or p.service = 40 or p.service = 41) 
and 
	u.no_charge_flag is null 
order by 
	u.userid, u.access_time
end
else if @account=5039 or @account=5040 or @account=5041 or @account=5042
begin
select 
	substring(s.name,1,50)"Vendor", s.name"Sevice", substring(replace(@period,' ',''),1,15)"Period",  convert(char(9),u.access_time,1)"Date",
	p.login"User ID", p.last_name"Last Name", p.first_name"First Name", 
	p.company"Legal Entity",p.honorific"BP",p.department"CC SRZ",p.address2"CC LE",p.address1"Billing Address",p.title"PNR",
	substring(u.Project,1,30)"Override Name", substring(u.Project2,1,30)"Override BP", substring(u.Project3,1,30)"Override PNR", substring(u.Project4,1,30)"Override CC SRZ", substring(u.Project5,1,30)"Override CC LE",
	substring(i.name,1,50)"File Name", ' '"Quantity",
	u.Description, u.list_price, ' '"Surcharge", u.price,u.displayprice, ' '"TAX",
	u.vendorprice, c.prepbookid"Alacra Book ID"
from 
	usage u join users p on u.userid = p.id
	join ip i on u.ip = i.id
	join account a on p.account = a.id
	join service s on p.service = s.id
	full join shoppingcart c on c.shoppingcartid = u.shoppingcartid
where 
	a.id = @account
and 
	p.demo_flag is null 
and 
	u.no_charge_flag is null 
order by 
	u.userid, u.access_time
end
else
begin
select 
	substring(s.name,1,50)"Vendor", s.name"Sevice", substring(replace(@period,' ',''),1,15)"Period", ' '"Office", convert(char(9),u.access_time,1)"Date",
	substring(u.Project,1,30)"Project", substring(u.Project2,1,30)"Project2", substring(u.Project3,1,30)"Project3",substring(u.Project4,1,30)"Project4", 
	p.login"User ID", p.last_name"Last Name", p.first_name"First Name", substring(i.name,1,50)"File Name", ' '"Quantity",
	u.Description, u.list_price, ' '"Surcharge", u.price,u.displayprice, ' '"TAX",
	u.vendorprice
from 
	usage u, users p, ip i, account a, service s
where 
	a.id = @account
and 
	u.access_time >= @period + ' 1,'+convert(char(4), @year) 
and	
	u.access_time < DATEADD(month,1, @period + ' 1,'+convert(char(4), @year)) 
and 
	u.userid = p.id 
and
	p.service = s.id
and 
	u.ip = i.id 
and 
	u.access_time >= p.commence 
and 
	p.demo_flag is null 
and 
	p.account = a.id 
--and 
--	(p.service is null or p.service = 0 or p.service = 28 or p.service = 33 or p.service = 34 or p.service = 35 or p.service = 36 or p.service = 37 or p.service = 40 or p.service = 41) 
and 
	u.no_charge_flag is null 
order by 
	u.userid, u.access_time
end

