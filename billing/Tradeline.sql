select 
	i.id 'Database Id', 
	i.name 'Database Name', 
	a.id 'Account Id', 
	a.name 'Account Name', 
	p.login 'Login',
	u.project 'Project',
	u.project2 'Project 2',
	u.project3 'Project 3',
	u.project4 'Project 4',
	u.project5 'Project 5',
	u.access_time 'Access Time',
	u.description 'Content Description',
	u.list_price 'List Price',
	u.price 'Alacra Billed Price',
	s.ipinfo 'Opt 1|Opt 2|Opt 3|Opt 4'
from 
	usage u JOIN users p ON u.userid = p.id JOIN ip i ON u.ip = i.id
	JOIN account a ON p.account = a.id LEFT JOIN usagespecial s
	ON u.usageid = s.usage
where 
	u.ip in 	(
		select 
			id 
		from 
			ip 
		where 
			ipcode in (
				'TLINE'
				)
		)
	and 
	u.no_charge_flag is null
	and 
	u.access_time >= 'August 1, 2007' and u.access_time < 'September 1, 2007'
	and
	p.demo_flag is NULL
	and

	(
		/* JP Morgan */
		u.userid in (select id from users where account in (2542,2984))
	)
	
order by 
	i.id, a.id, p.login, u.access_time asc
