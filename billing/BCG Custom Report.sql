DECLARE @year varchar(4)
DECLARE @month varchar(3)
DECLARE @startdate datetime
DECLARE @enddate datetime
DECLARE @surcharge money

select @year='2002'
select @month='November'

select @startdate='November 1, 2002'
select @enddate='December 1, 2002'
select @surcharge = 0.20

select
	'Alacra' 'Vendor',
	'Alacra' 'Product',
	@month+'-'+@year 'Period',
	NULL 'Office',
	u.access_time 'Date',
	u.project 'Project',
	p.last_name+p.first_name 'User ID',
	p.last_name 'Last Name',
	p.first_name 'First Name',
	i.name 'File Name',

	CASE ip
		WHEN 68 THEN
			substring (
			substring (s.ipinfo, CHARINDEX ('|', s.ipinfo)+1,100),1,
			CHARINDEX ('|',substring (s.ipinfo, CHARINDEX ('|', s.ipinfo)+1,100))-1)
		WHEN 79 THEN
			substring (
			substring (s.ipinfo, CHARINDEX ('|', s.ipinfo)+1,100),1,
			CHARINDEX ('|',substring (s.ipinfo, CHARINDEX ('|', s.ipinfo)+1,100))-1)
		WHEN 89 THEN
			substring (
			substring (s.ipinfo, CHARINDEX ('|', s.ipinfo)+1,100),1,
			CHARINDEX ('|',substring (s.ipinfo, CHARINDEX ('|', s.ipinfo)+1,100))-1)
		ELSE 
			NULL
	END "Quantity",

	u.description 'Description',
	u.price * @surcharge 'Access Fee',
	u.list_price 'Retail Price (List Price)',
	u.price 'Discount Price (Alacra Price)',

	coalesce (t.rate,0.0) * u.price * (1.0 + @surcharge) 'Tax',

	u.price * (1.0 + @surcharge) * (1.0 + coalesce (t.rate,0.0)) 'Invoice Price',

	u.displayprice 'Client Charge (Display Price)'
	
from
	usage u JOIN users p ON u.userid = p.id JOIN ip i ON u.ip = i.id JOIN
	account a ON p.account = a.id LEFT JOIN taxrates t ON
	substring (coalesce (a.billing_postal_code, a.postal_code),1,5) = t.postal_code
	LEFT JOIN usagespecial s ON u.usageid = s.usage
where
	a.name like 'Boston Consulting Group%'
	and
	u.access_time >= @startdate and u.access_time < @enddate
