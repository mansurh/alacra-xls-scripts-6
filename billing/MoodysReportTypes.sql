select 
	a.name 'Platform',
	u.access_time 'Date',
	u.description 'Report',
	CASE 
		WHEN a.id=7126 THEN
			p.first_name
		ELSE
			''
		END 'First Name',
	CASE 
		WHEN a.id=7126 THEN
			p.last_name
		ELSE
			''
		END 'Last Name',
		COALESCE(o.company,COALESCE(p.company,'')) 'Company',
	CASE
		WHEN a.id = 7126 THEN
			COALESCE(o.address,'')
		ELSE
			''
		END 'Address',
	COALESCE(o.city,COALESCE(p.city,'')) 'City',
	CASE WHEN o.state='none' THEN '' ELSE o.state END 'State-Prov',
	COALESCE(c.name,'') 'Country',
	CASE 
		WHEN a.id=7126 THEN
			COALESCE(p.email,'')
		ELSE
			''
		END 'E-Mail',
	CASE
		WHEN a.id=7126 THEN
			COALESCE(o.phone,'') 
		ELSE
			''
		END 'Phone',	
	u.price,
	t.report_type
	
from
	usage u JOIN users p ON u.userid = p.id JOIN usagespecial us ON
	u.usageid = us.usage JOIN account a ON p.account = a.id JOIN cc_order o ON
	p.id = o.userid LEFT JOIN country c ON o.countrycode = c.iso3166_N3
	LEFT JOIN mikeatemp t ON us.ipinfo = t.doc_id
where
	u.ip = 172
	and
	a.id in (4163,7126)
	and
	u.access_time >= 'June 1, 2012' and u.access_time < 'July 1, 2012'
	and
	o.orderid = (select MAX(orderid) from cc_order where userid = p.id)
	and
	p.demo_flag is NULL
	and 
	u.no_charge_flag is NULL
order by a.name desc, u.access_time asc