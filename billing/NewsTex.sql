select 
	i.name 'Database Name', 
	a.id 'Account Id', 
	a.name 'Account Name', 
	p.login 'Login',
	u.access_time 'Access Time',
	u.description 'Content Description',
	s.ipinfo 'Story ID'
from 
	usage u JOIN users p ON u.userid=p.id LEFT JOIN usagespecial s ON
	u.usageid = s.usage JOIN ip i ON u.ip = i.id JOIN account a ON
	p.account = a.id
where 
	ip in 	(294)
	and 
	no_charge_flag is null
	and 
	access_time >= 'May 1, 2006' and access_time < 'June 1, 2006'
	and
	p.demo_flag is NULL
order by 
	i.id, a.id, p.login, u.access_time asc
