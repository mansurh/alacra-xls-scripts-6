XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: billcheck database"
	exit 1
fi

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`

#set output file names
CHECKLIST='./checklist.txt'

rm -f $CHECKLIST

isql /U${logon} /P${password} /S${dataserver} -n /w 500 /Q "exec generate_billcheck_data" > $CHECKLIST 2>&1

# Mail results to Colin and Mike Angle
blat $CHECKLIST -t "billing@alacra.com" -s "BILLCHECK"
