database="bamp"
TITLEBAR="${database} Index Update"

SCRIPTDIR=$XLS/src/scripts/indexing/${database}
LOGDIR=$XLSDATA/${database}
mkdir -p ${LOGDIR}

logfile=${LOGDIR}/fullindex.log

${SCRIPTDIR}/rebuildindex.sh > ${logfile} 2>&1
if [ $? != 0 ]
then
	blat ${logfile}  -t "administrators@alacra.com" -s "${database} index Error"
	exit 1
fi
