# Global Variables
set nextDocument 0
set nextWord 0
set hDictionary -1
set hDocuments -1
set hXref -1

# Word delimiter characters
set delimiters "(\[-\(\)&\ \	_.,:;%/\]+)"
set dingbats "(\['0-9\]+)"

# Stopwords
set stopwords(inc) 1
set stopwords(ltd) 1
set stopwords(corporation) 1
set stopwords(company) 1
set stopwords(plc) 1
set stopwords(group) 1
set stopwords(the) 1
set stopwords(industries) 1
set stopwords(international) 1
set stopwords(corp) 1
set stopwords(holdings) 1
set stopwords(bhd) 1
set stopwords(holding) 1
set stopwords(division) 1
set stopwords(int'l) 1
set stopwords(div) 1
set stopwords(ind) 1
set stopwords(mfg) 1
set stopwords(cie) 1

# Determine if a word is a stop word
proc stopWord {word} {
	global stopwords

	# Don't index anything under 2 characters
	if {[string length $word] < 2} {
		return 1
	}

	# See if it is in the stopword list
	if {[info exists stopwords($word)]} {
		return 1
	}

	return 0
}

proc addWords {docid keywords} {
	global wordset
	global delimiters
	global dingbats
	global nextWord
	global hXref

	# Convert the keywords to lower case
	set keywords [string tolower $keywords]

	# Convert all special characters to spaces and get rid of junk
	regsub -all $delimiters "$keywords" " " delimited
	regsub -all $dingbats "$delimited" "" subwords
	set keywords [string trim $subwords]

	# Loop through all the keywords
	set wordlist [split $keywords]
	for {set i 0} {$i < [llength $wordlist]} {incr i} {

		set word [lindex $wordlist $i]
		# puts [format "_____>%s<" $word]

		# See if the word is in the stop list
		if {[stopWord $word] == 1} {
			continue
		}

		# Stem the word
		set word [stem $word]

		# See if the word is in the dictionary
		if {![info exists wordset($word)]} {
			set wordset($word) [incr nextWord]
		}

		# Get the word id
		set wid $wordset($word)

		# Put it in our local word set with a count
		if {[info exists localset($wid)]} {
			incr localset($wid)
		} else {
			set localset($wid) 1
		}
	}

	# If we have any words in our document
	if {[info exists localset]} {

		# Post all the words in the document
		set sid [array startsearch localset]
		set elements [array size localset]
		for {set i 0} {$i < $elements} {incr i} {
			set name [array nextelement localset $sid]
			puts $hXref [format "%s	%s	%s" $name $docid $localset($name)]
		}
		array donesearch localset $sid
	}
}

proc addDocument {url title keywords} {
	global nextDocument
	global hDocuments

	# assign the document id
	set docid [incr nextDocument]

	# Add all the words in the document
	addWords $docid $keywords

	# Log a message
	if {[expr $docid % 500] == 0} {
		puts $docid
	}

	# Write out the document entry 
	puts $hDocuments [format "%s	%s	%s" $docid "$url" "$title"]
}

# Open the index flat files
set hDictionary [open dictionary.dat w]
set hDocuments [open documents.dat w]
set hXref [open xref.dat w]

# Open the connection to SQL Server
set handle [sybconnect xls xls data1]

# Process the List of Industries
sybsql $handle "select id,description from sic"
sybnext $handle {
		addDocument [format "/cgi-bin/industrydetail.exe?id=%s" @1] [format "SIC %4.4d: %s" @1 @2] @2
}

# Process the list of companies
# sybsql $handle "select id,name from company"
# set a 0
# sybnext $handle {
		# addDocument [format "/cgi-bin/companydetail.exe?id=%s" @1] @2 @2
		# incr a
		# if {$a > 100000} {
			# break
		# }
# }

# Process the List of Spreadsheets
sybsql $handle "select id,title from ss where master_index=1"
sybnext $handle {
		addDocument [format "/cgi-bin/spreaddetail.exe?id=%s" @1] @2 @2
}

# Close the connection to SQL Server
sybclose $handle

# Post all the words in the dictionary
set sid [array startsearch wordset]
set elements [array size wordset]
for {set i 0} {$i < $elements} {incr i} {
	set name [array nextelement wordset $sid]
	puts $hDictionary [format "%s	%s" $name $wordset($name)]
}
array donesearch wordset $sid

# Close the index flat files
close $hDictionary
close $hDocuments
close $hXref
