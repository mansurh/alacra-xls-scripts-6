# abasupdate.sh
#	1 = source db name
#	2 = source server
#	3 = source user
#	4 = source psw
#	5 = POP3 server
#	6 = email user name
#	7 = email password
#
shname=abasupdate.sh
if [ $# -lt 7 ]
then
	echo "Usage: ${shname} dbname dbserver dbuser dbpsw emailserver emailuser emailpsw"
	exit 1
fi

# Save the runstring parameters in local variables
dbname=${1}
dbserver=${2}
dbuser=${3}
dbpsw=${4}
emailserver=${5}
emailuser=${6}
emailpsw=${7}

msgname=abaswhatsnew
acctcode=3039
#acctcode=NULL

# set up our work directory.  if it does not exist, create it
workdir=$XLSDATA/abasupdate
# if work directory doesn't exist, create it
if [ ! -d ${workdir} ]
then
	mkdir -p ${workdir}
fi
# if it still doesn't exist, give up
if [ ! -d ${workdir} ]
then
	echo "${shname}: could not create work directory ${workdir}"
	exit 1
fi

logfilename=${workdir}/abasupdate.log

$XLS/src/scripts/emailupdates/emailupdate.sh "${dbname}" "${dbserver}" "${dbuser}" "${dbpsw}" "${emailserver}" "${emailuser}" "${emailpsw}" "${workdir}" "${msgname}" "${acctcode}" "^([a-zA-Z0-9_\-])([a-zA-Z0-9_\.\-])+\@uk.pwcglobal.com$|^([a-zA-Z0-9_\-])([a-zA-Z0-9_\.\-])+\@alacra.com$|^([a-zA-Z0-9_\-])([a-zA-Z0-9_\.\-])+\@uk.alacra.com$" > ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	echo "Bad return code from update.sh - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "administrators@alacra.com" -s "PWC ABAS Update Failed"
#	blat ${logfilename} -t "anthony.bruni@alacra.com" -s "PWC ABAS Update Failed"
fi

exit $returncode
