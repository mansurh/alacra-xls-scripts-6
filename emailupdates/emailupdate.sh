# emailupdate.sh
#	1 = source db name
#	2 = source server
#	3 = source user
#	4 = source psw
#	5 = POP3 server
#	6 = email user name
#	7 = email password
#	8 = work dir
#	9 = name for the message (for inserted db data)
#	10 = account code for the message (for inserted db data)
#	11 = pattern to match against From: in header before loading
#
shname=emailupdate.sh
if [ $# -lt 9 ]
then
	echo "Usage: ${shname} dbname dbserver dbuser dbpsw emailserver emailuser emailpsw workdir msgname acctcode"
	exit 1
fi

XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Save the runstring parameters in local variables
dbname=${1}
#dbserver=${2}
#dbuser=${3}
#dbpsw=${4}
dbserver=`get_xls_registry_value ${dbname} Server`
dbuser=`get_xls_registry_value ${dbname} User`
dbpsw=`get_xls_registry_value ${dbname} Password`

#emailserver=${5}
emailserver=`get_mail_registry_value "SMTP Server"`
emailuser=${6}
#emailpsw=${7}
emailpsw=`get_xls_registry_value ${emailuser} Password 6`
workdir=${8}
msgname=${9}
if [ $# -gt 9 ]
then
	acctcode=${10}
else
	acctcode="NULL"
fi
if [ $# -gt 10 ]
then
	frompattern=${11}
else
	frompattern=""
fi

#echo "dbname=${dbname}"
#echo "dbserver=${dbserver}"
#echo "dbuser=${dbuser}"
#echo "dbpsw=${dbpsw}"
#echo "emailserver=${emailserver}"
#echo "emailuser=${emailuser}"
#echo "emailpsw=${emailpsw}"
#echo "workdir=${workdir}"
#echo "msgname=${msgname}"
#echo "acctcode=${acctcode}"
#echo "frompattern=${frompattern}"


# change to the our work directory
cd ${workdir}
if [ $? != 0 ]
then
	echo "${shname}: Error changing to work directory, exiting"
	exit 1
fi

# wipe out any existing mail files
alldeletes="MSG*.TXT* extracted.htm* my.sql"
for onedel in $alldeletes
do
	rm -f ${onedel}
	if [ $? != 0 ]
	then
		echo "${shname}: could not remove ${onedel}, exiting"
		exit 1
	fi
done

# retrieve mail via getmail
echo "${shname}: Retrieving Mail"
getmail.exe -S ${emailserver} -U ${emailuser} -P ${emailpsw}
mailret=$?
if [ mailret -gt 1 ]
then
	echo "${shname}: Error in getmail, Exiting"
	echo ""
	exit 1
elif [ mailret -eq 1 ]
then
	echo "${shname}: getmail reports no mail today, exiting"
	echo ""
	exit 0
fi


# any mail files that emerged will be named MSGnnnn.TXT
for mailfilename in `ls MSG*.TXT`
do
	echo "${shname}: Processing ${mailfilename}"

	if [ "${frompattern}" != "" ]
	then
		# take the last token from the line starting with From: and remove angle brackets
		fromaddr=$(grep -i "^From:" ${mailfilename} | awk '{print $NF}')
		nol=${fromaddr%\>}
		fromaddr=${nol#\<}
		# validate the address matches the pattern
		echo "${shname}: email was sent from ${fromaddr}"
		egrep -q -e "${frompattern}" << HERE
${fromaddr}
HERE
		if [ $? != 0 ]
		then
			echo "${shname}: email is from an unrecognized address, rejecting"
			continue
		fi
	fi

	grep -qi "^Content-Type: multipart/alternative" ${mailfilename}
	if [ $? != 0 ]
	then
		echo "${shname}: ${mailfilename} does not appear to be a multipart/alternative e-mail message, skipping"
	else
		# split the parts into separate files
		echo "${shname}: Processing ${mailfilename}, run multipartchop to split up the parts"
		multipartchop.exe ${mailfilename}
		if [ $? != 0 ]
		then
			echo "${shname}: error in multipartchop, exiting"
			exit 1
		fi
		# multipartchop appends letters, like a, b, c, etc. to the filename, leaving extension unchanged, e.g. MSG0001a.TXT
		# find which part is the text/html part
		noext=${mailfilename%.TXT}
		htmlpart=""
		need2decode=0
		for partfilename in `ls ${noext}?.TXT`
		do
			echo "${shname}: examining ${partfilename}"
			grep -qi "^Content-Type: text/html" ${partfilename}
			if [ $? != 0 ]
			then
				thistype=$(grep -i "^Content-Type:" ${partfilename} | cut -d " " -f 2)
				thistype2=${thistype%\;}
				echo "${shname}: ${partfilename} is ${thistype2}, skipping"
			else
				# this is the HTML part
				htmlpart=${partfilename}
				echo "${shname}: found text/html part in ${partfilename}!"
				# must we decode it?
				grep -qi "^Content-Transfer-Encoding:" ${partfilename}
				if [ $? = 0 ]
				then
					thisenc=$(grep -i "^Content-Transfer-Encoding:" ${partfilename} | cut -d " " -f 2)
					thisenc2=${thisenc%\;}
					echo "${shname}: ${partfilename} is encoded as ${thisenc2}"
					if [ ${thisenc2} != "7bit" ]
					then
						need2decode=1
					fi
				else
				fi
			fi
		done
		# at least one of the parts should have been HTML
		if [ "${htmlpart}" = "" ]
		then
			echo "${shname}: none of the parts has a content type of text/html, exiting"
			exit 1
		fi

		# now we have the right part file, next strip the message header
		stripheader=${htmlpart}.nohdr
		doneheader="0"
		while read a
		do
			if [ "${a}" = "" ]
			then
				doneheader="1"
			else
				if [ "${doneheader}" = "1" ]
				then
					echo "${a}" >> ${stripheader}
				fi
			fi
		done < ${htmlpart}

#		rm ${htmlpart}
		echo "${shname}: Successfully stripped message header, remaining content in ${stripheader}"

		# now header is stripped out, so decode it if we must
		if [ "${need2decode}" = "1" ]
		then
			decodedfile=${htmlpart}.htm
			qprint.exe -d ${stripheader} ${decodedfile}
			if [ $? != 0 ]
			then
				echo "${shname}: error decoding quoted-printable content with qprint, exiting"
				exit 1
			fi
#			rm ${stripheader}
			echo "${shname}: Successfully decoded quoted-printable content to ${decodedfile}"
		else
			decodedfile=${stripheader}
		fi

		# now we have a decoded HTML file.  We need to use our special utility to extract just the part we need
		extractedfile=extracted.htm
#		rm -f ${extractedfile}
		htmlemailutil.exe -i ${decodedfile} -o ${extractedfile} -b "BEGINMESSAGE" -e "ENDMESSAGE"
		if [ $? != 0 ]
		then
			echo "${shname}: error extracting HTML snippet via htmlemailutil, exiting"
			exit 1
		fi

		# we finally have just the piece of HTML we want to embed in the page
		echo "${shname}: Successfully extracted HTML content between BEGINMESSAGE and ENDMESSAGE to ${extractedfile}"

		# make a SQL-escaped version by doubling all single-quotes
		escapedfile=${extractedfile}.sql
		sed -e "s/'/''/g" -e "s/$%7bsk%7d/$\{sk\}/g" ${extractedfile} > ${escapedfile}
		if [ $? != 0 ]
		then
			echo "${shname}: error escaping HTML snippet for SQL via sed, exiting"
			exit 1
		fi
		echo "${shname}: Successfully escaped HTML snippet to ${escapedfile}"

		# create a SQL command file
		sqlcommand=my.sql
#		rm -f ${sqlcommand}

		echo "USE ${dbname}" >> ${sqlcommand}
		echo "GO" >> ${sqlcommand}
		echo "DECLARE @c int" >> ${sqlcommand}
		echo "SELECT @c = COUNT(*) FROM stdmessages WHERE msgname = '${msgname}'" >> ${sqlcommand}
		if [ "${acctcode}" != "NULL" ]
		then
			echo " AND account = ${acctcode} " >> ${sqlcommand}
		else
			echo " AND account is NULL " >> ${sqlcommand}			
		fi
		echo "IF @c = 0" >> ${sqlcommand}
		echo "    INSERT stdmessages (msgname,account,textmessage) VALUES ('${msgname}',${acctcode},'" >> ${sqlcommand}
		cat ${escapedfile} >> ${sqlcommand}
		echo "') " >> ${sqlcommand}
		echo "ELSE " >> ${sqlcommand}
		echo "    UPDATE stdmessages SET textmessage = '" >> ${sqlcommand}
		cat ${escapedfile} >> ${sqlcommand}
		echo "' WHERE msgname = '${msgname}' " >> ${sqlcommand}
		if [ "${acctcode}" != "NULL" ]
		then
			echo " AND account = ${acctcode} " >> ${sqlcommand}
		else
			echo " AND account is NULL " >> ${sqlcommand}
		fi
		echo "GO" >> ${sqlcommand}

		echo "${shname}: Here is final SQL (in ${sqlcommand}):"
		cat ${sqlcommand}

		echo ""

		# run isql to update the database
		isql -S ${dbserver} -U ${dbuser} -P ${dbpsw} < ${sqlcommand}
		if [ $? != 0 ]
		then
			echo "${shname}: error from isql, exiting"
			exit 1
		fi

		echo "${shname}: Successfully updated database."
	fi

	# delete the mail text file we just processed
#	echo "${shname}: Deleting ${mailfilename}"
#	rm -f ${mailfilename}

	echo ""
	echo ""
done
