#Arguments: server, sourcesafe-user, sourcesafe-password, ip, ASN, [version number]

#if (number of arguments !=7 or != 8) {
#   print usage and exit
#}

myname=loadIndustryMap.sh

if [ $# -ne 7 -a $# -ne 8 ]
then
	echo "Usage: ${myname} xls-server xls-user xls-passwd sourcesafe-user ss-password ip alacrasearchname [version number]"
	exit 1
fi

ssserver="//DATA11/SS_DB"
xlsserver=$1
xlslogon=$2
xlspsw=$3
sslogon=$4
sspsw=$5
ip=$6
alacrasearchname=$7

#if ( version number provided ) {
#   get file for ip,ASN from visual source safe, with version number
#   if ( ss get fails ) {
#      exit with error
#   }
#} else {
#  get latest version of file for ip,ANS from visual source safe
#  if ( ss get fails ) {
#    exit with error
#  }
#  get version number of latest file
#}

ssproject="\$/concordance/${alacrasearchname}"
ssfile="${ssproject}/${ip}.csv"
destinationdir="c:/concordance/${alacrasearchname}"
localfile="${destinationdir}/${ip}.csv"
scriptdir="${XLS}/src/scripts/concordance"

# See if the checkout level was specified
if [ $# -gt 7 ]
then
	ssversion=$8
	vlabel=$ssversion
	checkoutlevelflag="-V${vlabel}"
	echo "Checkout Level specified.  Will use ${checkoutlevelflag}"
else
	vlabel=""
	checkoutlevelflag=""
	echo "Checkout Level not specified.  Will use tip."
	# ADD CODE HERE FOR VERSION DETECTION
	ssversion=`ss Properties ${ssfile} | grep "Version:" | awk 'BEGIN{FS=" "} {print $2}'`
fi



#echo "Getting file: ${ssfile} version: $ssversion"

# set the 'ignore prompts' flag (useful to remove for debugging)
iflag=-I-N
#iflag=


# Check that the directory exists
if [ ! -d ${destinationdir} ]
then
	# echo "${myname}: ${destinationdir} is not a directory."
	# exit 1
 	mkdir -p ${destinationdir}
fi

# Get the entire tree, recursively
echo "${myname}: Performing a Get"
echo "          of file: ${ssfile}"
echo "   into directory: ${destinationdir}"
echo "    version label: ${vlabel}"
echo "         ss logon: ${sslogon}"
echo "           ss psw: ${sspsw}"
echo ""

# read zzz

ss Get ${ssfile} -GL${destinationdir} -GWR -GTM ${iflag} ${checkoutlevelflag} -Y${sslogon},${sspsw}
if [ $? != 0 ]
then
	echo "${myname}: Error in Source Safe Get - docs, exiting"
	exit 1
fi

# check the checked out file
#exit with error:
#  if ( file not found )
#  if ( file size is 0 )

if [ ! -f ${localfile} ]
then
  echo "Checked out file not found"
  exit 1
fi

if [ ! -s ${localfile} ]
then
  echo "Checked out file is 0 length"
  exit 1
fi

# process the file to load
#cat file to qcd2pipe, save to temp file
# tab delimited because found pipes in some fields
tempfile=${destinationdir}/${ip}.tmp
cat $localfile | tail +2 | qcd2pipe -s "\t" > $tempfile

if [ $? -ne 0 ]
then
  echo "Unable to create temporary file for BCP"
  exit 1
fi

#exit with error:
#  if ( temp file is empty )
#  if ( temp file has different IP, ASN than expected in it)

if [ ! -f ${tempfile} ]
then
  echo "Temporary work file not found"
  exit 1
fi

if [ ! -s ${tempfile} ]
then
  echo "Temporary work file is 0 length"
  exit 1
fi

awk -f ${scriptdir}/fixExcelCsv.awk $tempfile > ${tempfile}2

if [ ! -f ${tempfile}2 ]
then
  echo "Temporary work file not found"
  exit 1
fi

mv -f ${tempfile}2 ${tempfile}

temptablename=industry_map_temp

#create temp loading table in xls database
echo "Truncating temporary table for loading..."

isql /S${xlsserver} /U${xlslogon} /P${xlspsw} <<EOF
delete from ${temptablename}
EOF

#isql /S${xlsserver} /U${xlslogon} /P${xlspsw} <<EOF
#IF EXISTS (select * from sysobjects where id=object_id('dbo.industry_map_temp')) 
#BEGIN
#DROP TABLE ${temptablename}
#END

#CREATE TABLE ${temptablename} (
#alacrasearchname char (50) NULL,
#alacrasearchvalue  char(50) NULL,
#ip int NULL,
#sourceformname varchar (50) NULL,
#sourcekey varchar (255) NULL,
#account int NULL,
#nomultiple int NULL,
#joinoperator char (10) NULL,
#pairformat varchar (64) NULL,
#escapestyle tinyint NULL,
#invIndexField varchar (255) NULL
#)
#EOF

if [ $? -ne 0 ]
then
  echo "Error creating temporary table"
  exit 1
fi

#bcp temp file into database
formatfile="$XLS/src/scripts/concordance/industry_map_temp.fmt"
echo "\n\nLoading data into temp table"
bcp ${temptablename} in ${tempfile} /S${xlsserver} /U${xlslogon} /P${xlspsw} /f${formatfile}

if [ $? -ne 0 ]
then
  echo "BCP loading failed"
  exit 1
fi


sqltempfile="${destinationdir}/temp.rpt"

isql /S${xlsserver} /P${xlspsw} /U${xlslogon} -n /o${sqltempfile} <<EOF
set nocount on
select count(*) from ${temptablename}
go
EOF

tablelinecount=`cat $sqltempfile | sed '1,2d;s/^[ ]*//;s/[ ]*$//;/^$/d'`

# do tests on temp table
#exit with error:
#  if ( table is empty ) 
if [ $tablelinecount -eq 0 ]
then
  echo "Error table is empty"
  exit 1
fi

filelinecount=`wc -l ${tempfile} | cut -c1-7`

#  if (table has different rowcount than temp data file)
if [ $tablelinecount -ne $filelinecount ]
then
  echo "Error table has different rowcount than temp file"
  exit 1
fi

#  if ( table has wrong ASN or IP in a row [loading error])
#if [ $? -ne 0 ]
#then
#  echo "Error table has wrong ASN or IP in a row [loading error]"
#  exit 1
#fi

#warn with message but continue:
#  if (table has unknown ASN in DB) 
#  if (table has more than X% (or N rows?) than for previous values for
#      ASN,IP in DB )

isql /S${xlsserver} /P${xlspsw} /U${xlslogon} /o${sqltempfile} <<EOF
set nocount on
select count(*)
from industry_map_temp
where ip=$ip and alacrasearchname='$alacrasearchname'
EOF

count=`cat $sqltempfile | sed '1,2d;s/^[ ]*//;s/[ ]*$//;/^$/d'`

if [ $count -ne $filelinecount ]
then
  echo "Error table contains different IPs or Alacrasearchname than specified (may also have blank rows)"
  exit 1
fi

#delete rows from industry_map with asn, ip
echo "Deleting old values for IP ${ip}, ASN ${alacrasearchname} from industry_map table"
isql /S${xlsserver} /P${xlspsw} /U${xlslogon} <<EOF
delete from industry_map where ip=$ip and alacrasearchname='$alacrasearchname'
EOF

if [ $? -ne 0 ]
then
  echo "Error deleting old values from industry_map table"
  exit 1
fi

#copy rows from temp_table into industry_map
echo "Copying rows from ${temptablename} into industry_map"

isql /S${xlsserver} /P${xlspsw} /U${xlslogon} <<EOF
INSERT INTO industry_map select * from ${temptablename}
EOF

if [ $? -ne 0 ]
then
  echo "Error copying values from ${temptablename} table"
  exit 1
fi

#exit with urgent error:
#  if (count of ASN, IP rows in industry_map table is different from temp table)

#if (ASN,IP pair in industry_map_version table)
#  update loaddate, version number in table
#else
#  insert row with asn, ip, version, loaddate into table

isql /S${xlsserver} /P${xlspsw} /U${xlslogon} <<EOF
IF NOT EXISTS(select * from industry_map_version where alacrasearchname='${alacrasearchname}' and mapSourceId=${ip})
BEGIN
INSERT INTO industry_map_version (alacrasearchname, mapSourceId, sourceVersion, dateLoaded, whoLoadedBy)
values('${alacrasearchname}', ${ip}, '${ssversion}', GETDATE(), '${sslogon}')
END
ELSE
BEGIN
UPDATE industry_map_version
set sourceVersion='${ssversion}', dateLoaded=GETDATE(), whoLoadedBy='${sslogon}'
where alacrasearchname='${alacrasearchname}' and mapSourceId=${ip}
END
EOF

#comparing rows from the copied file

isql /S${xlsserver} /P${xlspsw} /U${xlslogon} /o${sqltempfile} <<EOF
set nocount on
select count(*)
from industry_map
where ip=$ip and alacrasearchname='$alacrasearchname'
EOF

count=`cat $sqltempfile | sed '1,2d;s/^[ ]*//;s/[ ]*$//;/^$/d'`

if [ $count -ne $filelinecount ]
then
  echo "Number of rows inserted into industry_map
table does not equal number in the file"
  exit 1
fi




#delete temp_table
#isql /S${xlsserver} /P${xlspsw} /U${xlslogon} <<EOF
#DROP TABLE ${temptablename}
#EOF

if [ $? -ne 0 ]
then
  echo "Error dropping temporary table"
exit 1
fi

echo ""

exit 0
