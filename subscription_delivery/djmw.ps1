
# DJ Marketwatch alert item delivery

[Reflection.Assembly]::LoadWithPartialName("System.Data") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("System.ServiceModel.Syndication") | Out-Null

[Reflection.Assembly]::LoadWithPartialName("Alacra.Alerts") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("Alacra.Util") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("Alacra.Database") | Out-Null


$ftpserver = [Alacra.Util.General]::GetXLSRegistryValue("SOFTWARE\XLS\File Servers\storeaffiliatedjmarketwatch","Server","ftp.marketwatch.com")
$ftpuser = [Alacra.Util.General]::GetXLSRegistryValue("SOFTWARE\XLS\File Servers\storeaffiliatedjmarketwatch","User","")
$ftppass = [Alacra.Util.General]::GetXLSRegistryValue("SOFTWARE\XLS\File Servers\storeaffiliatedjmarketwatch","Password","")

foreach ($kv in $rss.GetEnumerator()) {
	$item = $kv.Value

	$feeduri = new-object System.Uri("http://www.alacrastore.com")
    $feed = new-object Alacra.Alerts.AlertSyndicationFeed("Alacra Dow Jones Feed Update", "", $feeduri)
	$alacraapins = "http://www.alacra.com/api"
	$xmlnsdecl = new-object System.Xml.XmlQualifiedName("api", "http://www.w3.org/2000/xmlns/")
	$item.AttributeExtensions.Add($xmlnsdecl, $alacraapins)

	$ext = new-object System.ServiceModel.Syndication.SyndicationElementExtension("logo", $alacraapins, [string]::Format("/alacrastore/images/logos/{0}.gif", $item.Ip))
	$item.ElementExtensions.Add($ext)
	$ext2 = new-object System.ServiceModel.Syndication.SyndicationElementExtension("teeny_logo", $alacraapins, [string]::Format("/alacrastore/images/logos/teeny/{0}.gif", $item.Ip))
	$item.ElementExtensions.Add($ext2)
	$link = ""
	if ($item.Links.Count -gt 0) {
		$link = $item.Links[0].Uri.PathAndQuery
	}
	$ext3 = new-object System.ServiceModel.Syndication.SyndicationElementExtension("html_url", $alacraapins, [string]::Format("http://pcan.alacrastore.com/pcan/ClickProc.aspx?landingurl={0}&afid=196960&pid=", $link))
	$item.ElementExtensions.Add($ext3)

	$listbase = [System.Collections.Generic.List``1]
	$param = new-object System.Data.SqlClient.SqlParameter("@ip", [System.Convert]::ToInt32($item.Ip))
	$sqlparamlisttype = [Type] $param.GetType()
	$gensqlparamtype = $listbase.MakeGenericType(@($sqlparamlisttype))
	$params = [Activator]::CreateInstance($gensqlparamtype)
	$params.Add($param)
	$ds = [Alacra.Database.SQLHelper]::GetDataByStoredProcedure("xls","get_ip_info",$params,"djmw_subscriptiondelivery")
	$ipname = $ds.Tables[0].Rows[0]['name'].ToString()
	$ee1 = new-object System.ServiceModel.Syndication.SyndicationElementExtension("publisher", $alacraapins, $ipname)
	$item.ElementExtensions.Add($ee1)
	$ee2 = new-object System.ServiceModel.Syndication.SyndicationElementExtension("collection", $alacraapins, $ipname)
	$item.ElementExtensions.Add($ee2)
	$sp = new-object System.ServiceModel.Syndication.SyndicationPerson($ipname)
	$item.Authors.Add($sp)
	$sc = new-object System.ServiceModel.Syndication.SyndicationCategory($ipname)
	$item.Categories.Add($sc)


	$elemexttype = [System.ServiceModel.Syndication.SyndicationElementExtension]
	[System.Type[]]$types = @()
	$method = $elemexttype.GetMethod('GetObject', $types)
	$gentype = [System.Xml.Linq.XElement]
	$genmethod = $method.MakeGenericMethod(@($gentype))

	$separatorvarstr = "/_"
	$separatorvars = "/_".ToCharArray()
	$mappingids = @()
	$xlsids = @()
	if ($item.Ip -eq "232") {
		foreach ($nonrsselem in $item.ElementExtensions) {
			if ($nonrsselem.OuterName -eq "index_entry_primissuerid") {
				$nonrsselemobj = $genmethod.Invoke($nonrsselem, @())
				$mappingids = $mappingids + $nonrsselemobj.Value
			}
		}
	} elseif ($item.Ip -eq "229") {
		foreach ($nonrsselem in $item.ElementExtensions) {
			if ($nonrsselem.OuterName -eq "companyid") {
				$nonrsselemobj = $genmethod.Invoke($nonrsselem, @())
				$mappingids = $mappingids + $nonrsselemobj.Value
			}
		}
	} elseif ($item.Ip -eq "349") {
		foreach ($nonrsselem in $item.ElementExtensions) {
			if ($nonrsselem.OuterName -eq "index_entry_company_id") {
				$nonrsselemobj = $genmethod.Invoke($nonrsselem, @())
				$mappingids = $mappingids + $nonrsselemobj.Value
			}
		}
	} elseif ($item.Ip -eq "129") {
		foreach ($nonrsselem in $item.ElementExtensions) {
			if ($nonrsselem.OuterName -eq "cusip") {
				$nonrsselemobj = $genmethod.Invoke($nonrsselem, @())
				$mappingids = $mappingids + $nonrsselemobj.Value
			}
		}
	} elseif ($item.Ip -eq "181") {
		foreach ($nonrsselem in $item.ElementExtensions) {
			if ($nonrsselem.OuterName -eq "company") {
				$nonrsselemobj = $genmethod.Invoke($nonrsselem, @())
				$mappingids = $mappingids + $nonrsselemobj.Value
			}
		}
	} elseif ([int]$item.Ip -gt 2000) {
		foreach ($nonrsselem in $item.ElementExtensions) {
			if ($nonrsselem.OuterName.StartsWith("index_entry_xlsid")) {
				$nonrsselemobj = $genmethod.Invoke($nonrsselem, @())
				$xlsids = $xlsids + $nonrsselemobj.Value
			}
		}
	} else {
		$mappingids = $mappingids + $item.Id.Substring($item.Id.LastIndexOfAny($separatorvars) + 1)
	}
	foreach ($mappingid in $mappingids) { 
		$p21 = new-object System.Data.SqlClient.SqlParameter("@foreignip", [system.Convert]::ToInt32($item.Ip))
		$params2 = [Activator]::CreateInstance($gensqlparamtype)
		$params2.Add($p21)
		$p22 = new-object System.Data.SqlClient.SqlParameter("@foreignid", $mappingid)
		$params2.Add($p22)
		$dsxlsid = [Alacra.Database.SQLHelper]::GetDataByStoredProcedure("xls","getxlsidfromforeignip",$params2,"djmw_subscriptiondelivery")
		if ($dsxlsid.Tables.Count -gt 0 -and $dsxlsid.Tables[0].Rows.Count -gt 0) {
			$xlsidrow = $dsxlsid.Tables[0].Rows[0]
			$xlsids = $xlsids + $xlsidrow['xlsid'].ToString()
		}

	}

	if ($xlsids.Count -gt 0) {

		$xmlstr = New-Object System.Text.StringBuilder
		$xmlstr.AppendFormat('<api:symbol_info xmlns:api="{0}">', $alacraapins)
		$foundtickers = $false
		foreach ($xlsid in $xlsids) { 
			$coparams = [Activator]::CreateInstance($gensqlparamtype)
			$p31 = new-object System.Data.SqlClient.SqlParameter("@xlsid", [System.Convert]::ToInt32($xlsid))
			$coparams.Add($p31)
			$dsco = [Alacra.Database.SQLHelper]::GetDataByStoredProcedure("xls","getSnapshot1Record",$coparams,"djmw_subscriptiondelivery")
			if ($dsco.Tables.Count -gt 0 -and $dsco.Tables[0].Rows.Count -gt 0) {
				$intlticker = $dsco.Tables[0].Rows[0]['intl_ticker'].ToString()
				if (-not [string]::IsNullOrEmpty($intlticker)) {
					$tickercountry = $intlticker.Substring($intlticker.IndexOf('=') + 1)
					if ($tickercountry -eq "US") {
						$foundtickers = $true
						$xmlstr.AppendFormat('<api:ticker country="{0}">{1}</api:ticker>', $tickercountry, $intlticker.Substring(0, $intlticker.IndexOf('=')))
					}
				}
			}
		}
		if (-not $foundtickers) {
			continue
		}
		$xmlstr.Append("</api:symbol_info>")
		$strr = new-object System.IO.StringReader($xmlstr.ToString())
		$xr = [System.Xml.XmlReader]::Create($strr)
		$see = new-object System.ServiceModel.Syndication.SyndicationElementExtension($xr)
		$item.ElementExtensions.Add($see)

		$fakeitem = New-Object System.ServiceModel.Syndication.SyndicationItem
		$syndicationitemlisttype = [Type] $fakeitem.GetType()
		$gensyndicationitemparamtype = $listbase.MakeGenericType(@($syndicationitemlisttype))
		$items = [Activator]::CreateInstance($gensyndicationitemparamtype)
		$items.Add([System.ServiceModel.Syndication.SyndicationItem]$item)
		$feed.Items = $items

		$filename = [string]::Format("{0}", $item.Id.Replace('/', '_'))
		$ftp = [System.Net.FtpWebRequest]::Create([string]::Format("ftp://{0}/{1}.tmp", $ftpserver, $filename))
		$ftp = [System.Net.FtpWebRequest]$ftp
		$ftp.Method = [System.Net.WebRequestMethods+Ftp]::UploadFile
		$ftp.Credentials = new-object System.Net.NetworkCredential($ftpuser,$ftppass)
		$ftp.UseBinary = $true
		$ftp.UsePassive = $true
		$ftp.Timeout = 120000

		$feedstr = $feed.ToString()
		$feedstr = $feedstr.Replace('encoding="utf-16"', 'encoding="utf-8"')

		$encobj = [System.Text.Encoding]::UTF8
		$buf = $encobj.GetBytes($feedstr)
		$ftp.ContentLength = $buf.Length
		$ftprs = $ftp.GetRequestStream()
		$ftprs.Write($buf, 0, $buf.Length)
		$ftprs.Close()
		$ftpwr = $ftp.GetResponse()
		$ftpwr.Close()

		$ftprn = [System.Net.FtpWebRequest]::Create([string]::Format("ftp://{0}/{1}.tmp", $ftpserver, $filename))
		$ftprn.Method = [System.Net.WebRequestMethods+Ftp]::Rename
		$ftprn.Credentials = new-object System.Net.NetworkCredential($ftpuser,$ftppass)
		$ftprn.RenameTo = [string]::Format("{0}.xml", $filename)
		$ftprnwr = $ftp.GetResponse()
		$ftprnwr.Close()

	}
}
