# Script file to generate the IP matching reports for finex
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_ni_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=ni

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in sdc(New Issues)"
select i.cusip ,i.name  from ipid_ni i
where "'"+i.cusip+"'" not in (select sourcekey from company_map where source=45)


/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from sdc(New Issues) "

HERE
