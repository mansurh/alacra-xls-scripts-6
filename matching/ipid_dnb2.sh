# Script file to build the IP matching table for wscope
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = dnb2 Server
# 6 = dnb2 Login
# 7 = dnb2 Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_wscope.sh ipid_server ipid_database ipid_login ipid_password wscope_server wscope_login wscope_password"
	exit 1
fi


IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
server=$5
logon=$6
password=$7

SCRIPTDIR=$XLS/src/scripts/matching
DATADIR=$XLSDATA/matching/dnb2
mkdir -p ${DATADIR}

echo
echo creating ipid_dnbTemp
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ipid_dnbTemp
go
create table ipid_dnbTemp (
	strDunsno char(10) NOT NULL,
	name varchar(120) NOT NULL,
	tradestyle varchar(90) NULL,
	dba tinyint NULL,
	addr1 varchar(64) NULL,
	addr2 varchar(64) NULL,
	city  varchar(60) NULL,
	stateAbbr char(5) NULL,
	country varchar(32) NULL,
	postalCode varchar(12) NULL,
	telephone varchar(16) NULL,
	fax varchar(16) NULL,
	busDesc varchar(120) NULL,
	SIC1 int NULL,	
	SIC2 int NULL,	
	SIC3 int NULL,	
	SIC4 int NULL,	
	SIC5 int NULL,	
	SIC6 int NULL,	
	url1 varchar(120) NULL,
	TypeOfEstablishment varchar(30) NOT NULL,
	salesAnnUSD numeric(20) NULL,
	employessTotal int NULL,
	dunsNoPrev int NULL,
	domesticUltimateDunsNo int NULL,
	domesticUltimateName varchar(120) NULL,
	globalUltimateDunsNo int NULL,
	globalUltimateName varchar(120) NULL,
	taxid int NULL
)                  
HERE
              
if [ "$?" != "0" ]
then
	echo "Error creating table, exiting"
	exit 1
fi

echo    
date
echo "load ipid_dnbTemp"
bcp ipid_dnbTemp in $DATADIR/ipid_dnb.out /b100000 /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /ein.err /f${SCRIPTDIR}/ipid_dnb2.fmt
if [ $? != 0 ]
then
    echo "Error loading ipid_dnbTemp"
    exit 1
fi


isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
CREATE UNIQUE INDEX ipid_dnb01   ON ipid_dnbTemp
    ( strDunsNo ) 

CREATE INDEX ipid_dnb02  ON ipid_dnbTemp
    ( name ) 

CREATE INDEX ipid_dnb03  ON ipid_dnbTemp
    ( tradestyle ) 

CREATE  INDEX ipid_dnb04  ON ipid_dnbTemp
	(country)

CREATE  INDEX ipid_dnb05  ON ipid_dnbTemp
	(TypeOfEstablishment)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating indices, exiting"
	exit 1
fi

echo
echo renaming ipid_dnbTemp
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ipid_dnb
EXEC sp_rename 'ipid_dnbTemp', 'ipid_dnb'
HERE
if [ $? -ne 0 ]
then
	echo "Error renaming ipid_dnb ipid_dnbTemp, exiting"
	exit 1
fi

