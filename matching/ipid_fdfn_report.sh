# Script file to generate the IP matching reports for fdfn
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_fdfn_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

LOADDIR=${XLS}/src/scripts/loading/fdfn
TEMPDIR=${XLSDATA}/matching

# Now build an email to send to company matching
blatfile="${TEMPDIR}/ipid_fdfn.txt"
blatfiletmp1=${blatfile}.tmp1
blatfiletmp2=${blatfile}.tmp2

finalblatfile=${REPORTFILE}

echo "*** ipid_fdfn update info: *** " > ${finalblatfile}
echo "" >> ${finalblatfile}
echo "*** Companies added: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}
isql -U ${xlslogin} -P ${xlspassword} -S ${xlsserver} -r -Q " set nocount on select distinct matchkey + '|' + name + '|??' from ipid_fdfn where matchkey not in (select distinct sourcekey from company_map where source=9971) order by 1 " > ${blatfiletmp1}
tail +9 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_fdfn.sed > ${blatfiletmp2}

while read -r line
do

    line=${line%% }

    if [ "${line}" != '' ]
    then

        print "${line}"

    fi

done < ${blatfiletmp2} >> ${finalblatfile}

echo "" >> ${finalblatfile}
echo "*** Companies deleted: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}
isql -U ${xlslogin} -P ${xlspassword} -S ${xlsserver} -s"|" -r -x 255 -w 255 -Q " set nocount on select distinct rtrim(convert(varchar(255), m.sourcekey)) + '|' + convert(varchar(255), m.xlsid) + '|' + c.name + '|' + cou.name from company_map m, company c, country cou where m.source=9971 and c.id=m.xlsid and cou.id=c.country and sourcekey not in (select distinct matchkey from ipid_fdfn) and ''<>isnull(m.sourcekey, '') order by 1 " > ${blatfiletmp1}
tail +5 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_fdfn.sed > ${blatfiletmp2}

while read -r line
do

    line=${line%% }

    if [ "${line}" != '' ]
    then

        print "${line}"

    fi

done < ${blatfiletmp2} >> ${finalblatfile}

exit 0
