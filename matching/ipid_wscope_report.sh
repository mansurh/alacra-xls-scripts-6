# Script file to generate the IP matching reports for wscope
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_wscope.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=wscope

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in wscope"
select * from ipid_wscope
where id not in (select sourcekey from security_map where source=187)


/* Required Report - Securities which have disappeared from database */
PRINT "Matched securities that have disappeared from wscope"
select x.id securityId, c.name, x.name, c.id xlsid  from company c, security x, security_map m
where
x.id = m.id and x.issuer=c.id and m.source=187
and
m.sourcekey not in (select id from ipid_wscope)

/* Optional report - new information available in database */

HERE
