# Script file to build the IP matching table for softbase
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = softbase Server
# 6 = softbase Login
# 7 = softbase Password
ARGS=7
if [ $# -ne $ARGS ]
then
    echo "Usage: ipid_softbase.sh IPID_server IPID_database IPID_login IPID_password softbase_server softbase_login softbase_password"
    exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogon=$3
IPIDpassword=$4
srcdataserver=$5
srclogon=$6
srcpassword=$7


LOADDIR=${XLS}/src/scripts/loading/softbase
TEMPDIR=${XLSDATA}/matching/softbase
mkdir -p ${TEMPDIR}


#
# Put new data in a batch file.
#

echo ""
echo "Building bulk load file from companies table from ${srcdataserver}"
echo ""

isql -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} -r -Q "set nocount on select d1.doc_id + '^' + d1.prop_value + '^' + d2.prop_value + '^' + d3.prop_value + '^' + isnull(d4.prop_value, '') + '^' + replace(d5.prop_value, '/', '-') from doc_props d1, doc_props d2, doc_props d3, doc_props d4, doc_props d5 where d1.prop_name='company' and d2.prop_name='status' and d3.prop_name='cou' and d4.prop_name='url' and d5.prop_name='date' and d1.doc_id=d2.doc_Id and d2.doc_id=d3.doc_Id and d3.doc_id=d4.doc_Id and d4.doc_id=d5.doc_Id and d1.doc_id in (select distinct doc_id from doc_props where prop_name='type' and prop_value='Company')" -w 255 -x 255 > ${TEMPDIR}/ipid_softbase.dat.temp1

tail +9 < ${TEMPDIR}/ipid_softbase.dat.temp1 | sed -f ${LOADDIR}/ipid_softbase.sed > ${TEMPDIR}/ipid_softbase.dat.temp2

while read -r line
do

    line=${line%% }

    if [ "${line}" != '' ]
    then

        echo ${line}

    fi

done < ${TEMPDIR}/ipid_softbase.dat.temp2 > ${TEMPDIR}/ipid_softbase.dat

#
# Drop old data from xls table ipid_softbase
#

echo ""
echo "Dropping old ipid_softbase data on ${IPIDserver}"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/create_ipid_softbase.sql


# drop the indices for the table
echo ""
echo "Dropping indices for ipid_softbase for faster loading"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/dropindex_ipid_softbase.sql


#
# Load all the new data...
#

DATFILE="${TEMPDIR}/ipid_softbase.dat"

# Load the ipid_softbase table
echo "Loading the ipid_softbase table" 

bcp concordance.dbo.ipid_softbase in ${DATFILE} /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} /f${LOADDIR}/ipid_softbase.fmt /e${TEMPDIR}/ipid_softbase.log
if [ $? != 0 ]
then

    echo "Error loading ipid_softbase table"
    exit 1

fi

# Rebuild the indices for the table
echo ""
echo "Rebuilding indices for ipid_softbase for faster loading"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/createindex_ipid_softbase.sql

echo ""
echo "Updated table ipid_softbase for softbase OK."
echo ""

exit 0
