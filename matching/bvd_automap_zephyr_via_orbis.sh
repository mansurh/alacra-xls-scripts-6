database=concordance
#----------------------------------------------
XLSUTILS=$XLS/src/scripts/loading/dba/
#----------------------------------------------
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn
#----------------------------------------------
dataserver=`get_xls_registry_value ${database} Server`
user=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
#----------------------------------------------
DATESTRING=`date +%Y-%m-%d-%H%M%S`
LOGDIR=${XLSDATA}/bvd
LOGFILE=${LOGDIR}/automap_zephyr_via_orbis_${DATESTRING}.log
#----------------------------------------------

mkdir -p ${LOGDIR}

echo LOGFILE ${LOGFILE}

run()
{
	echo "bvd zephyr via orbis automapping has started " `date`
	isql -b -S ${dataserver} -U ${user} -P ${password} -Q " exec spAutoMapZephyrViaOrbis "
    if [ $? != 0 ]
    then
        echo "Automapping has failed" `date`
        return 1
    fi 	
	echo "bvd zephyr via orbis automapping succeeded " `date`
	return 0
}

#----------main routines-----------------------------------------
(run) >> $LOGFILE 2>&1
rc=$?
if [ $rc != 0 ]
then
    echo "Sending out log file: ${LOGFILE}"
	#blat $LOGFILE -t administrators@alacra.com,simon.vileshin@alacra.com -s "Bvd Zephyr via Orbis automapping on $COMPUTERNAME has failed."
	blat $LOGFILE -t simon.vileshin@alacra.com -s "Bvd Zephyr via Orbis automapping on $COMPUTERNAME has failed."
fi

exit $rc
