# Script file to build the IP matching table for Vickers
# Revised by John Willcox 30 March 2005 to include column security_type

WD=C:/users/xls/src/scripts/matching
#WD=C:/SqlServer/Vickers

# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = Vickers Server
# 6 = Vickers Login
# 7 = Vickers Password
ARGS=7
if [ $# -lt $ARGS ]
then
	echo "Usage: ipid_Vickers.sh ipid_server ipid_database ipid_login ipid_password Vickers_server Vickers_login Vickers_password" >> $LOG
	exit 1
fi

ipname=Vickers

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
ipserver=$5
iplogin=$6
ippassword=$7

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

SED_FILE=${WD}/match.sed

# Name of the format file to use
FORMAT_FILE=${WD}/ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
echo "Selecting ip data into temporary file"
isql -S${ipserver} -U${iplogin} -P${ippassword} -w500 -s"|" -n -h-1 >${TMPFILE1} << ENDOfIsql
SET NOCOUNT ON
select cusip_number, cusip_number, security_name, ticker_symbol, security_type , stock_exchange
from sec s
where security_name is not null
go
ENDOfIsql

# Step 3 - post-process the temp file
echo "Post processing temporary file"
sed -f ${SED_FILE} < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
echo "Dropping the old table" 
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
echo "Creating new table"
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
set ansi_padding off
GO
create table ${TABLENAME} (
	matchkey varchar(12) NOT NULL,
	cusip_number varchar(12) NOT NULL,
	security_name varchar(60) NULL,
	ticker_symbol varchar(10) NULL,
	security_type varchar(2) NULL,
	stock_exchange varchar(3) NULL
)
GO
HERE

if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
echo "BCPing data into ipid_Vickers"
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /t /b1000
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - index the table
echo "Indexing the table"
isql -S${IPIDserver} -U${IPIDlogin} -P${IPIDpassword} << HERE2
create index ${TABLENAME}_01 on ${TABLENAME}
	(cusip_number)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(security_name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(ticker_symbol)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(stock_exchange)
GO
HERE2
if [ $? -ne 0 ]
then
	echo "Error indexing table, exiting"
	exit 1
fi


# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}