XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Script file to build the IP matching table for Investext
# 1 = Concordance Server
# 2 = Concordance Login
# 3 = Concordance Password
# 4 = BVD Server
# 5 = BVD Login
# 6 = BVD Password
#if [ $# -lt 6 ]
#then
#	echo "Usage: update_ipid_bvd.sh Concordance_server Concordance_login Concordance_password bvd_server bvd_login bvd_password"
#	exit 1
#fi

#cdserver=$1
#cdlogin=$2
#cdpassword=$3
#ipserver=$4
#iplogin=$5
#ippassword=$6

cdserver=`get_xls_registry_value concordance Server`
cdlogin=`get_xls_registry_value concordance User`
cdpassword=`get_xls_registry_value concordance Password`
ipserver=`get_xls_registry_value bvd Server`
iplogin=`get_xls_registry_value bvd User`
ippassword=`get_xls_registry_value bvd Password`

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo " ********* ${timestamp} Start update ***********  "

workdir=${XLSDATA}/bvd

# Name of the temp file to use
TMPZEPHYR=${workdir}/ipid_zephyr.tmp

# Format File for bcp
FORMAT_FILE=${XLS}/src/scripts/matching/ipid_bvd.fmt

echo " remove any old temp files"
rm -f ${TMPZEPHYR}

echo " bcp data from company table from bvd database to a temporary file"
bcp bvd.dbo.company out ${TMPZEPHYR} -S ${ipserver} -U ${iplogin} -P ${ippassword}  -c -eipid_bvd.err -m1000
if [ $? -ne 0 ]
then
	echo "Error in bcp company out, exiting"
	exit 1
fi

if [ ! -s ${TMPZEPHYR} ]
then
	echo "*** ${TMPZEPHYR} data file is empty. Exiting ***"
	exit 1
fi


echo " truncate ipid_bvd table "
isql /S${cdserver} /U${cdlogin} /P${cdpassword} << HERE
truncate table dbo.ipid_bvd
GO
HERE

echo " bcp data from file ${TMPZEPHYR} into ipid_bvd "
bcp ipid_bvd in ${TMPZEPHYR} -S ${cdserver} -U ${cdlogin} -P ${cdpassword}  /f${FORMAT_FILE} /eipid_bvd.err /m1000
if [ $? -ne 0 ]
then
	echo "Error in bcp ${TMPZEPHYR} into ipid_bvd table, exiting"
	exit 1
fi


##############################################

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo " ********* ${timestamp} update of ipid table for bvd has finished. ***********  "

exit 0

