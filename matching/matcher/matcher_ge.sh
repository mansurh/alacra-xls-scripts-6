if [ $# -lt 5 ]
then
  echo "usage: $0 server user pass jobid tablename"
  exit 1;
fi
server=$1
user=$2
pass=$3
jobid=$4 # test values 542, 1061
tablename=$5
emails="albert.tamayev@opus.com,Sybille.Polynice@Opus.Com,arizona@mobiusservices.com,Vasyl.Zayachkivsky@opus.com"

echo $server $user $jobid $tablename
reportdir=reports
batchname=match$jobid_`date +%Y%m%d%H%M`
#batchname=match`date +%Y%m%d%H%M`

create_config_file() {
  configfile=config_${jobid}.txt
  rm -f $configfile

  echo "// The meaning of the parameters below:" >> $configfile
  echo "// BatchJobName - name given to the batch job" >> $configfile
  echo "// FileNameSU - file name location for SearchUniverse" >> $configfile
  echo "// FileNameMG - file name location for MatchGroup" >> $configfile
  echo "MatcherURL=http://localhost:8080" >> $configfile
  echo "BatchJobName=${batchname}" >> $configfile
  #echo "FileNameSU=me.txt" >> $configfile
  echo "FileNameMG=vendor${jobid}.txt" >> $configfile
  echo "FileNameSU=alacra${jobid}.txt" >> $configfile
  #echo "FileNameMG=d:/users/default/matcher/20180910/vendor542.txt" >> $configfile
  #echo "FileNameSU=d:/users/default/matcher/20180910/alacra1062.txt" >> $configfile
  echo "BatchJobWorkerType=" >> $configfile
  echo "MatcherWorkerType=" >> $configfile
  #echo "IgnoreCountry=false" >> $configfile
  echo "IgnoreState=true" >> $configfile
  echo "// WorkerCount=16" >> $configfile
  echo "UseSemanticMatch=false" >> $configfile
  echo "ReportsDir=${reportdir}" >> $configfile
  cat $configfile
}

run_matcher() {
  rm -rf $XLSDATA/matcher/batchjobs/${batchname}
  mkdir -p $XLSDATA/matcher/batchjobs/
  echo "started: " `date`
  c:/Alacra/app/GE/MatchGE.Client.CL/MatchGE.Client.CL.exe $configfile&                        #> me.log 2>&1 
  pid=$! #Get the PID of the last background job
  echo "waiting for process to complete: $pid"

  #jobs -p #get a list of all background jobs
  #wait $pid #wait for specific job

  wait $pid     #wait for all background jobs
  rc=$?
  echo "return code: $rc"
  echo "end: " `date`
  return $rc
}

create_table_new(){
  osql /U${user} /P${pass} /S${server} /Q "drop table [${tablename}_ge_new] "
  osql /U${user} /P${pass} /S${server} /Q "drop table [${tablename}_ge_top3_new] "
  osql /U${user} /P${pass} /S${server} /b /Q "select top 0 * into [${tablename}_ge_new] from match_GE_template_table_output"
  if [ $? -ne 0 ]; then echo "error creating ${tablename}_ge_new table, exiting "; return 1; fi;
  osql /U${user} /P${pass} /S${server} /b /Q "select top 0 * into [${tablename}_ge_top3_new] from match_GE_template_table_output"
  if [ $? -ne 0 ]; then echo "error creating ${tablename}_ge_top3_new table, exiting "; return 1; fi;
}

load_data() {
  cd ${reportdir}/${batchname}
  ls -l batchjobreports_${batchname}.zip
  blat -attach batchjobreports_${batchname}.zip -s "${tablename}:${batchname}" -t $emails
  unzip -o batchjobreports_${batchname}.zip
  if [ $? -ne 0 ]; then echo "error unzipping batchjobreports_${batchname}.zip, exiting "; return 1; fi;

  ls -l batchjob_match_report_best_matches.txt batchjob_match_report_best_3.txt
  bcp ${tablename}_ge_new in batchjob_match_report_best_matches.txt /U${user} /P${pass} /S${server} /b1000 /e batchjob_match_report_best_matches.err /c /CRAW /F2
  if [ $? -ne 0 ]; then echo "error bcp ${tablename}_ge_new table, exiting "; return 1; fi;

  bcp ${tablename}_ge_top3_new in batchjob_match_report_best_3.txt /U${user} /P${pass} /S${server} /b1000 /e batchjob_match_report_best_matches.err /c /CRAW /F2
  if [ $? -ne 0 ]; then echo "error bcp ${tablename}_ge_new table, exiting "; return 1; fi;
  rm *.txt

  osql /U${user} /P${pass} /S${server} /Q "drop table [${tablename}_ge_bak] "
  osql /U${user} /P${pass} /S${server} /Q "drop table [${tablename}_ge_top3_bak] "

  osql /U${user} /P${pass} /S${server} /b /Q "exec sp_rename [${tablename}_ge], [${tablename}_ge_bak]"
  osql /U${user} /P${pass} /S${server} /b /Q "exec sp_rename [${tablename}_ge_top3], [${tablename}_ge_top3_bak]"

  osql /U${user} /P${pass} /S${server} /b /Q "exec sp_rename [${tablename}_ge_new], [${tablename}_ge]"
  if [ $? -ne 0 ]; then echo "error renaming ${tablename}_ge_new table, exiting "; return 1; fi;

  osql /U${user} /P${pass} /S${server} /b /Q "exec sp_rename [${tablename}_ge_top3_new], [${tablename}_ge_top3]"
  if [ $? -ne 0 ]; then echo "error renaming ${tablename}_ge_top3_new table, exiting "; return 1; fi;
}


main() {
  create_table_new
  if [ $? -ne 0 ]; then echo "error creating new table, exiting ..."; exit 1; fi;

  create_config_file

  run_matcher
  if [ $? -ne 0 ]; then echo "error in matcher, exiting ..."; exit 1; fi;

  load_data
  if [ $? -ne 0 ]; then echo "error in matcher, exiting ..."; exit 1; fi;
}

main