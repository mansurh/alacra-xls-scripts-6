if [ $# != 2 ]
then
    echo "Usage: score_daily_main.sh id error_emaillist"
    echo "Example: score_daily_main.sh 1 administrators@alacra.com"
    exit 1
fi

id=$1
error_emaillist=$2

LOADDIR=${XLSDATA}/matcher
SCRIPTDIR=$XLS/src/scripts/matching/matcher
joblog=${LOADDIR}/s_jobs.log

TODAY=`date +%d%m%Y`

XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

db=concordance
user=`get_xls_registry_value ${db} user`
password=`get_xls_registry_value ${db} password`
server=`get_xls_registry_value ${db} server`
backup=`get_xls_registry_value ${db} backup`

#----------------------------------------------------------------

get_next_job()
{
start=`date`
echo "${start}: Getting next job"

cd ${LOADDIR}
check_return_code $? "Error changing to load directory ${LOADDIR} - Aborting" 1

sql="UPDATE score_jobs SET jobStarted = GETDATE(), jobStatus = 10 WHERE id = '${id}'"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error updating log table for job ${id} - aborting" 1

# select 1 job
sql="SET NOCOUNT ON SELECT TOP 1 j.sourceId, j.description, j.emaillist, j.selectClause, j.fromClause, j.whereClause, j.thresholdScore, j.fieldType, t.stopwordTableName, t.abbreviationTableName, j.soundexModifier, j.regionPenalty, j.stopwordPenalty, j.regionQuery, j.id FROM score_jobs j, match_field_type t WHERE j.fieldType = t.type AND j.id = '${id}'"
isql -U${user} -P${password} -S${server} -Q"${sql}" -o"${joblog}" -h-1 -n -s":" -b -w1000
check_return_code $? "Error getting next job - aborting" 1

# remove all new line characters
tr -d '\n' < ${joblog} > ${joblog}.fmt
read line < ${joblog}.fmt

# parse for each parameter
sourceid=$(echo "$line" | cut -d":" -f1)
# trimming all preceding and trailing whitespace
sourceid=$(echo "$sourceid" | sed 's/^[ \t]*//;s/[ \t]*$//')
description=$(echo "$line" | cut -d":" -f2)
description=$(echo "$description" | sed 's/^[ \t]*//;s/[ \t]*$//')
description=$(echo "$description" | sed 's/ /_/g')
emaillist=$(echo "$line" | cut -d":" -f3)
emaillist=$(echo "$emaillist" | sed 's/^[ \t]*//;s/[ \t]*$//')
selectClause=$(echo "$line" | cut -d":" -f4)
selectClause=$(echo "$selectClause" | sed 's/^[ \t]*//;s/[ \t]*$//')
fromClause=$(echo "$line" | cut -d":" -f5)
fromClause=$(echo "$fromClause" | sed 's/^[ \t]*//;s/[ \t]*$//')
whereClause=$(echo "$line" | cut -d":" -f6)
whereClause=$(echo "$whereClause" | sed 's/^[ \t]*//;s/[ \t]*$//')
alacrasource="${selectClause} ${fromClause} ${whereClause}"
thresholdscore=$(echo "$line" | cut -d":" -f7)
thresholdscore=$(echo "$thresholdscore" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldtype=$(echo "$line" | cut -d":" -f8)
fieldtype=$(echo "$fieldtype" | sed 's/^[ \t]*//;s/[ \t]*$//')
stopwordtablename=$(echo "$line" | cut -d":" -f9)
stopwordtablename=$(echo "$stopwordtablename" | sed 's/^[ \t]*//;s/[ \t]*$//')
abbreviationtablename=$(echo "$line" | cut -d":" -f10)
abbreviationtablename=$(echo "$abbreviationtablename" | sed 's/^[ \t]*//;s/[ \t]*$//')
soundexmodifier=$(echo "$line" | cut -d":" -f11)
soundexmodifier=$(echo "$soundexmodifier" | sed 's/^[ \t]*//;s/[ \t]*$//')
regionpenalty=$(echo "$line" | cut -d":" -f12)
regionpenalty=$(echo "$regionpenalty" | sed 's/^[ \t]*//;s/[ \t]*$//')
stopwordpenalty=$(echo "$line" | cut -d":" -f13)
stopwordpenalty=$(echo "$stopwordpenalty" | sed 's/^[ \t]*//;s/[ \t]*$//')
regionquery=$(echo "$line" | cut -d":" -f14)
regionquery=$(echo "$regionquery" | sed 's/^[ \t]*//;s/[ \t]*$//')
#id=$(echo "$line" | cut -d":" -f15)
#id=$(echo "$id" | sed 's/^[ \t]*//;s/[ \t]*$//')

rm ${joblog}
rm ${joblog}.fmt

# if we managed to pull down a job
if [[ ${id} != '' ]]
then
	alacrafile=${LOADDIR}/s_alacra${id}.txt
	configfile=${LOADDIR}/s_config${id}.txt
	regionfile=${LOADDIR}/s_region${id}.txt
	stopwordfile=${LOADDIR}/s_stopwordfile${id}.txt
	abbreviationfile=${LOADDIR}/s_abbreviationfile${id}.txt
	outputfile=${LOADDIR}/s_output${id}.txt
	tempoutputtable=scorer_${description}_output_temp
	outputtable=scorer_${description}_output
	# this is user id for a/c 6619
	userid=118087	

	# display job parameters
	echo "Job ${id} parameters"
	echo "IPID Table: '${ipidtable}'"
	echo "Source Name: '${sourceid}'"
	echo "Description: '${description}'"
	echo "Alacra Source Query: '${alacrasource}'"
	echo "Threshold score: '${thresholdscore}'"
	echo "Field Type: '${fieldtype}'"
	echo "Stopword table: '${stopwordtablename}'"
	echo "Abbreviation table '${abbreviationtablename}'"
	echo "Soundex modifier: '${soundexmodifier}'"
	echo "Region penalty: '${regionpenalty}'"
	echo "Stopword penalty '${stopwordpenalty}'"
	echo "Region query: '${regionquery}'"
	echo "Output file: '${outputfile}'"
	echo "Alacra file: '${alacrafile}'"
else
	echo "No more jobs"
fi

end=`date`
echo "${end}: Getting next job - completed" 

}

#----------------------------------------------------------------

create_alacra_file()
{
start=`date`
echo "${start}: Creating Alacra file ${alacrafile} for job ${id}"

alacrafile2=${alacrafile}2
rm $alacrafile2

# retrieve data for Alacra source file
alacrasource="SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ${alacrasource}"
bcp "${alacrasource}" queryout ${alacrafile} -b10000 -c -t"|" -S${server} -U${user} -P${password}
check_return_code $? "Error creating Alacra file ${alacrafile} for job ${id} - aborting" 1

sed 's/\x0//g' $alacrafile > $alacrafile2
mv $alacrafile2 $alacrafile

end=`date`
echo "${end}: Creating Alacra file ${alacrafile} for job ${id} - completed" 

}

#----------------------------------------------------------------

create_stopword_files()
{
start=`date`
echo "${start}: Creating stopword files for job ${id}"

# retrieve data for stopword file to be used for first matching field
# this is delimited with a space
bcp "SELECT * FROM ${stopwordtablename}" queryout ${stopwordfile} -b10000 -c -t" " -S${server} -U${user} -P${password}
check_return_code $? "Error bcping out from ${stopwordtablename} for job ${id} - aborting" 1

# retrieve data for abbreviations file to be used for first matching field
# this is pipe-delimited
bcp "SELECT abbreviation, word FROM ${abbreviationtablename}" queryout ${abbreviationfile} -b10000 -c -t"|" -S${server} -U${user} -P${password}
check_return_code $? "Error bcping out from ${abbreviationtablename} for job ${id} - aborting" 1

end=`date`
echo "${end}: Creating stopword files for job ${id} - completed" 

}

#----------------------------------------------------------------

create_region_file()
{
start=`date`
echo "${start}: Creating region file ${regionfile} for job ${id}"

# retrieve data for region file
# delimited with space character
bcp "${regionquery}" queryout ${regionfile} -b10000 -c -t" " -S${server} -U${user} -P${password}
check_return_code $? "Error creating region file ${regionfile} for job ${id} - aborting" 1

end=`date`
echo "${end}: Creating region file ${regionfile} for job ${id} - completed" 

}

#----------------------------------------------------------------

create_config_file()
{
start=`date`
echo "${start}: Creating config file for job ${id}"

echo "${alacrafile}" > ${configfile}
echo "${thresholdscore}" >> ${configfile}
echo "${fieldtype}" >> ${configfile}
echo "${soundexmodifier}" >> ${configfile}
echo "${regionpenalty}" >> ${configfile}
echo "${stopwordpenalty}" >> ${configfile}
echo "${regionfile}" >> ${configfile}
echo "${outputfile}" >> ${configfile}
echo "${stopwordfile}" >> ${configfile}
echo "${abbreviationfile}" >> ${configfile}

end=`date`
echo "${end}: Creating config file for job ${id} - completed" 

}

#----------------------------------------------------------------

run_matcher()
{
start=`date`
echo "${start}: Running scorer for job ${id}"

echo "matcher.exe ${configfile}"
matcher.exe "score" "${configfile}" "${id}"
check_return_code $? "Error running matcher.exe using ${configfile} for job ${id} - aborting" 1

end=`date`
echo "${end}: Running matcher for job ${id} - completed" 

}

#----------------------------------------------------------------

email_file()
{
start=`date`
echo "${start}: E-mailing output file ${outputfile} to ${emaillist} for job ${id}"

if [[ -e ${LOADDIR}/RegionError.txt ]]
then
	echo "Some regions were not in the regions table ${regiontable} allocated for this job ${id}"
	tmpfile=${LOADDIR}/emailregions${id}.txt
	echo "Some regions were not in the regions table ${regiontable} allocated for this job ${id}" > ${tmpfile}
	echo "Job description was ${description}" >> ${tmpfile}
	blat ${tmpfile} -t "${error_emaillist}" -s "Regions to be added to ${regiontable}" -attach "${LOADDIR}/RegionError.txt"
	check_return_code $? "Error - e-mailing message to ${error_emaillist} for job ${id} - aborting" 1
	rm ${tmpfile}
	rm ${LOADDIR}/RegionError.txt
fi	

if [[ ${emaillist} != 'NULL' ]]
then
	size=$(du -ks ${outputfile} | cut -f1)
	echo "Size of file is ${size} Kb"
	tmpfile=${LOADDIR}/email${id}.txt
	if [[ ${size} -lt 10000 ]]
	then
		echo "Output for job ${id}" > ${tmpfile}
		blat ${tmpfile} -t "${emaillist}" -s "Matching results ${day} ${description}" -attach "${outputfile}"
		check_return_code $? "Error - e-mailing output file ${outputfile} to ${emaillist} for job ${id} - aborting" 1
	else
		echo "File for job ${id} is too big, please access it from the DMO or in concordance database on ${server}" > ${tmpfile}
		blat ${tmpfile} -t "${emaillist}" -s "Matching results ${day} ${description}"
		check_return_code $? "Error - e-mailing message to ${emaillist} for job ${id} - aborting" 1
	fi
	rm ${tmpfile}
else
	echo "E-mail address list is blank, no need to e-mail output file"
fi

end=`date`
echo "${end}: E-mailing output file ${outputfile} to ${emaillist} for job ${id} - completed" 

}

#----------------------------------------------------------------

load_files_to_locker()
{
start=`date`
echo "${start}: Load log and output files to locker for job ${id}"

echo "concAddToLocker.exe \"${outputfile}\" \"text/plain\" \"${userid}\""
locker_file_id=`concAddToLocker.exe "${outputfile}" "text/plain" "${userid}"`
check_return_code $? "Failed to add ${outputfile} to locker server - Aborting" 1

end=`date`
echo "${end}: Load log and output files to locker for job ${id} - completed" 

}

#----------------------------------------------------------------

load_file_to_database()
{
start=`date`
echo "${start}: Load output file to database for job ${id}"

# first delete temporary table for output results if it exists
echo "Dropping ${tempoutputtable} if it exists on ${server}"
sql="IF OBJECT_ID('${tempoutputtable}') IS NOT NULL DROP TABLE ${tempoutputtable}"
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -l0
check_return_code $? "Error dropping ${tempoutputtable} - aborting" 1

echo "Dropping ${tempoutputtable} if it exists on ${backup}"
isql -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b -l0
check_return_code $? "Error dropping ${tempoutputtable} - aborting" 1

# also delete old results table
echo "Dropping ${outputtable}_old if it exists on ${server}"
sql="IF OBJECT_ID('${outputtable}_old') IS NOT NULL DROP TABLE ${outputtable}_old"
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -l0
check_return_code $? "Error dropping ${outputtable}_old - aborting" 1

echo "Dropping ${outputtable}_old if it exists on ${backup}"
isql -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b -l0
check_return_code $? "Error dropping ${outputtable}_old - aborting" 1

# now create temporary table for output results
echo "Creating ${tempoutputtable} on ${server}"
columncount=$((`echo $selectClause|sed 's/[^,]//g'|wc -m`))
sql="CREATE TABLE ${tempoutputtable} (col1 VARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL"
for ((i=2;i<=columncount;i+=1)); do
	sql+=",col${i} VARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL"
done
sql+=",score INT NULL)"
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -l0
check_return_code $? "Error creating ${tempoutputtable} - aborting" 1

bcp ${tempoutputtable} in ${outputfile} -b10000 -c -t"|" -r"\n" -S${server} -U${user} -P${password}
check_return_code $? "Error bcping output into ${tempoutputtable} for job ${id} - aborting" 1

echo "Creating ${tempoutputtable} on ${backup}"
isql -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b -l0
check_return_code $? "Error creating ${tempoutputtable} - aborting" 1

bcp ${tempoutputtable} in ${outputfile} -b10000 -c -t"|" -r"\n" -S${backup} -U${user} -P${password}
check_return_code $? "Error bcping output into ${tempoutputtable} for job ${id} - aborting" 1

end=`date`
echo "${end}: Load output file to database for job ${id} - completed" 

}

#----------------------------------------------------------------

update_tables()
{
start=`date`
echo "${start}: Update log table and replace output table for job ${id}"

echo "Rename tables on ${server}"
sql="sp_rename '${outputtable}', '${outputtable}_old'"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b

sql="sp_rename '${tempoutputtable}', '${outputtable}'"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error renaming new output table to current output table for job ${id} - aborting" 1

echo "Rename tables on ${backup}"
sql="sp_rename '${outputtable}', '${outputtable}_old'"
isql -n -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b

sql="sp_rename '${tempoutputtable}', '${outputtable}'"
isql -n -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error renaming new output table to current output table for job ${id} - aborting" 1

filesize=$(stat -c%s "${outputfile}")
rowcount=$((`wc -l "${outputfile}" | awk '{print $1'}`-1))

sql=" UPDATE score_jobs SET lastUpdated = GETDATE(), jobStatus = 100, filename='score_${description}_output.txt', filesize=${filesize}, matches=${rowcount}, lockerfileid='${locker_file_id}' WHERE id = ${id}"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error updating log table for job ${id} - aborting" 1

end=`date`
echo "${end}: Update log table and replace output table for job ${id} - completed" 

}

#----------------------------------------------------------------

generate_dmo_events()
{

start=`date`
echo "${start}: Generating dmo events for job ${id}"


isql -l0 -b -S ${server} -U ${user} -P ${password} -Q " exec up_process_auto_scoring_updates ${id}"
if [ $? -ne 0 ]
then
	echo "Error generating dmo events for ${id}"
	exit 1
else
	end=`date`
	echo "${end}: Generating dmo events for job ${id} completed." 
fi

}

#----------------------------------------------------------------

clean_up()
{
start=`date`
echo "${start}: Cleaning up job id: ${id}"

rm ${alacrafile}
rm ${configfile}
rm ${stopwordfile}
rm ${abbreviationfile}
rm ${regionfile}
mv ${outputfile} score_${description}_output.txt

end=`date`
echo "${end}: Cleaning up job id ${id} - completed" 

}




#----------main routines-----------------------------------------

get_next_job

create_alacra_file

create_stopword_files

create_region_file

create_config_file

run_matcher

email_file

load_files_to_locker

load_file_to_database

update_tables

generate_dmo_events

clean_up		

exit 0