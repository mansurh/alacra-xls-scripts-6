if [ $# -lt 2 ]
then
    echo "Usage: match_daily_main.sh id error_emaillist"
    echo "Example: match_daily_main.sh 1 administrators@alacra.com"
    exit 1
fi

id=$1
error_emaillist=$2
#test version of executable to run
TEST_EXE=$3

LOADDIR=${XLSDATA}/matcher
########LOADDIR="./"

SCRIPTDIR=$XLS/src/scripts/matching/matcher
joblog=${LOADDIR}/jobs.log

TODAY=`date +%Y%m%d`

XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

db=concordance
user=`get_xls_registry_value ${db} user`
password=`get_xls_registry_value ${db} password`
server=`get_xls_registry_value ${db} server`
backup=`get_xls_registry_value ${db} backup`

#----------------------------------------------------------------

get_next_job()
{
start=`date`
echo "${start}: Getting next job"

cd ${LOADDIR}
check_return_code $? "Error changing to load directory ${LOADDIR} - Aborting" 1

sql="UPDATE match_jobs SET jobStarted = GETDATE(), jobStatus = 10 WHERE id = '${id}'"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error updating log table for job ${id} - aborting" 1

# select 1 job
# status is in (0 = Pending, 10 = In Progress, 20 = Error)
sql="SET NOCOUNT ON SELECT TOP 1 j.ipidTable, j.sourceId, j.description, j.emaillist, j.alacraSource, j.alacraNonMatching, j.vendorSource, j.vendorNonMatching, j.maxMatchNum, j.minScore, j.matchFields, j.fieldOneType, j.fieldOneScore, t1.stopwordTableName, t1.abbreviationTableName, j.fieldTwoType, j.fieldTwoScore, t2.stopwordTableName, t2.abbreviationTableName, j.matchIncorrectRegion, j.regionPenalty, j.stopwordPenalty, j.regionQuery, j.id FROM match_jobs j, match_field_type t1, match_field_type t2 WHERE j.fieldOneType = t1.type AND (j.fieldTwoType = t2.type OR j.fieldTwoType IS NULL) AND j.id='${id}'"
isql -U${user} -P${password} -S${server} -Q"${sql}" -o"${joblog}" -h-1 -n -s":" -b -w"1000"
check_return_code $? "Error getting next job - aborting" 1

# remove all new line characters
tr -d '\n' < ${joblog} > ${joblog}.fmt
read line < ${joblog}.fmt

# parse for each parameter
ipidtable=$(echo "$line" | cut -d":" -f1)
# trimming all preceding and trailing whitespace
ipidtable=$(echo "$ipidtable" | sed 's/^[ \t]*//;s/[ \t]*$//')
sourceid=$(echo "$line" | cut -d":" -f2)
sourceid=$(echo "$sourceid" | sed 's/^[ \t]*//;s/[ \t]*$//')
description=$(echo "$line" | cut -d":" -f3)
description=$(echo "$description" | sed 's/^[ \t]*//;s/[ \t]*$//')
description=$(echo "$description" | sed 's/ /_/g')
emaillist=$(echo "$line" | cut -d":" -f4)
emaillist=$(echo "$emaillist" | sed 's/^[ \t]*//;s/[ \t]*$//')
alacrasource=$(echo "$line" | cut -d":" -f5)
alacrasource=$(echo "$alacrasource" | sed 's/^[ \t]*//;s/[ \t]*$//')
alacraNonMatching=$(echo "$line" | cut -d":" -f6)
alacraNonMatching=$(echo "$alacraNonMatching" | sed 's/^[ \t]*//;s/[ \t]*$//')
vendorsource=$(echo "$line" | cut -d":" -f7)
vendorsource=$(echo "$vendorsource" | sed 's/^[ \t]*//;s/[ \t]*$//')
vendorNonMatching=$(echo "$line" | cut -d":" -f8)
vendorNonMatching=$(echo "$vendorNonMatching" | sed 's/^[ \t]*//;s/[ \t]*$//')
maxmatchnum=$(echo "$line" | cut -d":" -f9)
maxmatchnum=$(echo "$maxmatchnum" | sed 's/^[ \t]*//;s/[ \t]*$//')
minscore=$(echo "$line" | cut -d":" -f10)
minscore=$(echo "$minscore" | sed 's/^[ \t]*//;s/[ \t]*$//')
matchfields=$(echo "$line" | cut -d":" -f11)
matchfields=$(echo "$matchfields" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldonetype=$(echo "$line" | cut -d":" -f12)
fieldonetype=$(echo "$fieldonetype" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldonescore=$(echo "$line" | cut -d":" -f13)
fieldonescore=$(echo "$fieldonescore" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldonestopwordtablename=$(echo "$line" | cut -d":" -f14)
fieldonestopwordtablename=$(echo "$fieldonestopwordtablename" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldoneabbreviationtablename=$(echo "$line" | cut -d":" -f15)
fieldoneabbreviationtablename=$(echo "$fieldoneabbreviationtablename" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldtwotype=$(echo "$line" | cut -d":" -f16)
fieldtwotype=$(echo "$fieldtwotype" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldtwoscore=$(echo "$line" | cut -d":" -f17)
fieldtwoscore=$(echo "$fieldtwoscore" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldtwostopwordtablename=$(echo "$line" | cut -d":" -f18)
fieldtwostopwordtablename=$(echo "$fieldtwostopwordtablename" | sed 's/^[ \t]*//;s/[ \t]*$//')
fieldtwoabbreviationtablename=$(echo "$line" | cut -d":" -f19)
fieldtwoabbreviationtablename=$(echo "$fieldtwoabbreviationtablename" | sed 's/^[ \t]*//;s/[ \t]*$//')
matchincorrectregion=$(echo "$line" | cut -d":" -f20)
matchincorrectregion=$(echo "$matchincorrectregion" | sed 's/^[ \t]*//;s/[ \t]*$//')
regionpenalty=$(echo "$line" | cut -d":" -f21)
regionpenalty=$(echo "$regionpenalty" | sed 's/^[ \t]*//;s/[ \t]*$//')
stopwordpenalty=$(echo "$line" | cut -d":" -f22)
stopwordpenalty=$(echo "$stopwordpenalty" | sed 's/^[ \t]*//;s/[ \t]*$//')
regionquery=$(echo "$line" | cut -d":" -f23)
regionquery=$(echo "$regionquery" | sed 's/^[ \t]*//;s/[ \t]*$//')
#id=$(echo "$line" | cut -d":" -f24)
#id=$(echo "$id" | sed 's/^[ \t]*//;s/[ \t]*$//')

rm -f ${joblog}
rm -f ${joblog}.fmt

# if we managed to pull down a job
if [[ ${id} != '' ]]
then
	alacrafile=${LOADDIR}/alacra${id}.txt
	vendorfile=${LOADDIR}/vendor${id}.txt
	configfile=${LOADDIR}/config${id}.txt
	regionfile=${LOADDIR}/region${id}.txt
	stopword1file=${LOADDIR}/stopword1file${id}.txt
	abbreviation1file=${LOADDIR}/abbreviation1file${id}.txt
	# if we are matching on two fields
	if [[ ${fieldtwotype} != 'NULL' ]]	
	then
		stopword2file=${LOADDIR}/stopword2file${id}.txt
		abbreviation2file=${LOADDIR}/abbreviation2file${id}.txt
	fi
	ignorewordfile=${LOADDIR}/ignorewordfile${id}.txt
	outputfile=${LOADDIR}/output${id}.txt
	outputfilename=output${id}.txt
	tempoutputtable=match_${description}_output_temp
	outputtable=match_${description}_output
	# this is user id for a/c 6619
	userid=118087

	numAlacraNonMatchingFields=${alacraNonMatching}
	numVendorNonMatchingFields=${vendorNonMatching}

	# display job parameters
	echo "Job ${id} parameters"
	echo "IPID Table: '${ipidtable}'"
	echo "Source Name: '${sourceid}'"
	echo "Description: '${description}'"
	echo "Alacra Source Query: '${alacrasource}'"
	echo "Vendor Source Query: '${vendorsource}'"
	echo "Max match number: '${maxmatchnum}'"
	echo "Minimum score: '${minscore}'"
	echo "Number matching fields: '${matchfields}'"
	echo "Field 1 Type: '${fieldonetype}'"
	echo "Field 1 Score '${fieldonescore}'"
	echo "Field 1 stopword table: '${fieldonestopwordtablename}'"
	echo "Field 1 abbreviation table '${fieldoneabbreviationtablename}'"
	echo "Field 2 Type: '${fieldtwotype}'"
	echo "Field 2 Score '${fieldtwoscore}'"
	echo "Field 2 stopword table: '${fieldtwostopwordtablename}'"
	echo "Field 2 abbreviation table '${fieldtwoabbreviationtablename}'"
	echo "Number of Alacra fields not used for matching '${numAlacraNonMatchingFields}'"
	echo "Number of vendor fields not used for matching '${numVendorNonMatchingFields}'"
	echo "Match incorrect regions: '${matchincorrectregion}'"
	echo "Region penalty: '${regionpenalty}'"
	echo "Stopword penalty '${stopwordpenalty}'"
	echo "Region query: '${regionquery}'"
	echo "Output file: '${outputfile}'"
	echo "Alacra file: '${alacrafile}'"
	echo "Vendor file: '${vendorfile}'"
else
	echo "No more jobs"
fi

end=`date`
echo "${end}: Getting next job - completed" 

}

#----------------------------------------------------------------

create_alacra_file()
{
start=`date`
echo "${start}: Creating Alacra file ${alacrafile} for job ${id}"

alacrafile2=${alacrafile}2
rm -f $alacrafile2

# retrieve data for Alacra source file
alacrasource="SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ${alacrasource}"
bcp "${alacrasource}" queryout ${alacrafile} -b10000 -c -t"|" -S${server} -U${user} -P${password} -CRAW
check_return_code $? "Error creating Alacra file ${alacrafile} for job ${id} - aborting" 1

sed 's/\x0//g' $alacrafile > $alacrafile2
mv $alacrafile2 $alacrafile

if [ ! -s $alacrafile ]
then
	check_return_code -1 "Alacra file ${alacrafile} for job ${id} has a zero length - aborting" 1
fi

end=`date`
echo "${end}: Creating Alacra file ${alacrafile} for job ${id} - completed" 

}

#----------------------------------------------------------------

create_vendor_file()
{
start=`date`
echo "${start}: Creating vendor file ${vendorfile} for job ${id}"

vendorfile2=${vendorfile}2
rm -f $vendorfile2

# retrieve data for vendor source file
vendorsource="SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ${vendorsource}"
bcp "${vendorsource}" queryout ${vendorfile} -b10000 -c -t"|" -S${server} -U${user} -P${password} -CRAW
check_return_code $? "Error creating vendor file ${vendorfile} for job ${id} - aborting" 1

sed 's/\x0//g' $vendorfile > $vendorfile2
mv $vendorfile2 $vendorfile

if [ ! -s $vendorfile ]
then
	check_return_code -1 "Vendor file ${vendorfile} for job ${id} has a zero length - aborting" 1
fi

end=`date`
echo "${end}: Creating vendor file ${vendorfile} for job ${id} - completed" 

}

#----------------------------------------------------------------

create_stopword_files()
{
start=`date`
echo "${start}: Creating stopword files for job ${id}"

# retrieve data for stopword file to be used for first matching field
# this is delimited with a space
bcp "SELECT * FROM ${fieldonestopwordtablename}" queryout ${stopword1file} -b10000 -c -t" " -S${server} -U${user} -P${password}
check_return_code $? "Error bcping out from ${fieldonestopwordtablename} for job ${id} - aborting" 1

# retrieve data for abbreviations file to be used for first matching field
# this is pipe-delimited
#use case in select due to wierdness in bcp out - null data comes out as 'nothing' but zero length fields come out as NULLs (00) which messes up matcher code
bcp "SELECT abbreviation, case len(word) when 0 then null else word end FROM ${fieldoneabbreviationtablename}" queryout ${abbreviation1file} -b10000 -c -t"|" -S${server} -U${user} -P${password}
check_return_code $? "Error bcping out from ${fieldoneabbreviationtablename} for job ${id} - aborting" 1

# if we're matching on two fields
# retrieve data for stopword and abbreviations file
if [[ ${fieldtwotype} != 'NULL' ]]
then
	bcp "SELECT * FROM ${fieldtwostopwordtablename}" queryout ${stopword2file} -b10000 -c -t" " -S${server} -U${user} -P${password}
	check_return_code $? "Error bcping out from ${fieldtwostopwordtablename} for job ${id} - aborting" 1

	bcp "SELECT abbreviation, word FROM ${fieldtwoabbreviationtablename}" queryout ${abbreviation2file} -b10000 -c -t"|" -S${server} -U${user} -P${password}
	check_return_code $? "Error bcping out from ${fieldtwoabbreviationtablename} for job ${id} - aborting" 1
fi

end=`date`
echo "${end}: Creating stopword files for job ${id} - completed" 

}

#----------------------------------------------------------------

create_ignore_word_file()
{
start=`date`
echo "${start}: Creating ignore word file ${ignorewordfile} for job ${id}"

# retrieve data for ignore word file
bcp "SELECT word FROM match_ignore_words" queryout ${ignorewordfile} -b10000 -c -t" " -S${server} -U${user} -P${password}
check_return_code $? "Error creating ignore word file ${ignorewordfile} for job ${id} - aborting" 1

end=`date`
echo "${end}: Creating ignore word file ${ignorewordfile} for job ${id} - completed" 

}

#----------------------------------------------------------------

create_region_file()
{
start=`date`
echo "${start}: Creating region file ${regionfile} for job ${id}"

# retrieve data for region file
# delimited with space character
bcp "${regionquery}" queryout ${regionfile} -b10000 -c -t" " -S${server} -U${user} -P${password}
check_return_code $? "Error creating region file ${regionfile} for job ${id} - aborting" 1

end=`date`
echo "${end}: Creating region file ${regionfile} for job ${id} - completed" 

}

#----------------------------------------------------------------

create_config_file()
{
start=`date`
echo "${start}: Creating config file for job ${id}"

echo "${alacrafile}" > ${configfile}
echo "${vendorfile}" >> ${configfile}
echo "${maxmatchnum}" >> ${configfile}
echo "${minscore}" >> ${configfile}
echo "${matchfields}" >> ${configfile}
echo "${fieldonetype} ${fieldonescore}" >> ${configfile}
if [[ ${fieldtwotype} != 'NULL' ]]
then
	echo "${fieldtwotype} ${fieldtwoscore}" >> ${configfile}
fi
echo "${matchincorrectregion}" >> ${configfile}
echo "${regionpenalty}" >> ${configfile}
echo "${stopwordpenalty}" >> ${configfile}
echo "${regionfile}" >> ${configfile}
echo "${outputfile}" >> ${configfile}
echo "${ignorewordfile}" >> ${configfile}
echo "${stopword1file}" >> ${configfile}
echo "${abbreviation1file}" >> ${configfile}
if [[ ${fieldtwotype} != 'NULL' ]]
then
	echo "${stopword2file}" >> ${configfile}
	echo "${abbreviation2file}" >> ${configfile}
fi

end=`date`
echo "${end}: Creating config file for job ${id} - completed" 

}

#----------------------------------------------------------------

run_matcher()
{
start=`date`
echo "${start}: Running matcher for job ${id}"

#executable="matcher.exe"
executable="matcher64td.exe"
if [ "$numAlacraNonMatchingFields" == "5" ]
then
	#testing extra diagnostics
	executable="matcher64tdeHQ.exe"
fi

echo "$executable ${configfile}"
#last param turns off sound matching
#soundOff="1"
$executable "match" "${configfile}" "${id}" $soundOff
check_return_code $? "Error running $executable using ${configfile} for job ${id} - aborting" 1

end=`date`
echo "${end}: Running $executable for job ${id} - completed" 

}

#----------------------------------------------------------------

email_file()
{
start=`date`
echo "${start}: E-mailing output file ${outputfile} to ${emaillist} for job ${id}"

if [[ -e ${LOADDIR}/RegionError.txt ]]
then
	echo "Some regions were not in the regions table ${regiontable} allocated for this job ${id}"
	tmpfile=${LOADDIR}/emailregions${id}.txt
	echo "Some regions were not in the regions table ${regiontable} allocated for this job ${id}" > ${tmpfile}
	echo "Job description was ${description}" >> ${tmpfile}
	blat ${tmpfile} -t "${error_emaillist}" -s "Regions to be added to ${regiontable}" -attach "${LOADDIR}/RegionError.txt"
	check_return_code $? "Error - e-mailing message to ${error_emaillist} for job ${id} - aborting" 1
	rm -f ${tmpfile}
	rm -f ${LOADDIR}/RegionError.txt
fi	

if [[ ${emaillist} != 'NULL' ]]
then
	size=$(du -ks ${outputfile} | cut -f1)
	echo "Size of file is ${size} Kb"
	tmpfile=${LOADDIR}/email${id}.txt
	if [[ ${size} -lt 10000 ]]
	then
		echo "Output for job ${id}" > ${tmpfile}
		blat ${tmpfile} -t "${emaillist}" -s "Matching results ${day} ${description}" -attach "${outputfile}"
		check_return_code $? "Error - e-mailing output file ${outputfile} to ${emaillist} for job ${id} - aborting" 1
	else
		echo "File for job ${id} is too big, please access it from the DMO or in concordance database on ${server}" > ${tmpfile}
		blat ${tmpfile} -t "${emaillist}" -s "Matching results ${day} ${description}"
		check_return_code $? "Error - e-mailing message to ${emaillist} for job ${id} - aborting" 1
	fi
	rm -f ${tmpfile}
else
	echo "E-mail address list is blank, no need to e-mail output file"
fi

end=`date`
echo "${end}: E-mailing output file ${outputfile} to ${emaillist} for job ${id} - completed" 

}

#----------------------------------------------------------------

load_files_to_locker()
{
start=`date`
echo "${start}: Load log and output files to locker for job ${id}"

echo "concAddToLocker.exe \"${outputfile}\" \"text/plain\" \"${userid}\""
locker_file_id=`concAddToLocker.exe "${outputfile}" "text/plain" "${userid}"`
check_return_code $? "Failed to add ${outputfile} to locker server - Aborting" 1

end=`date`
echo "${end}: Load log and output files to locker for job ${id} - completed" 

}

#----------------------------------------------------------------

load_file_to_database()
{
start=`date`
echo "${start}: Load output file to database for job ${id}"

# first delete temporary table for output results if it exists
echo "Dropping ${tempoutputtable} if it exists on ${server}"
sql="IF OBJECT_ID('${tempoutputtable}') IS NOT NULL DROP TABLE ${tempoutputtable}"
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error dropping ${tempoutputtable} - aborting" 1

# also delete old results table
echo "Dropping ${outputtable}_old if it exists on ${server}"
sql="IF OBJECT_ID('${outputtable}_old') IS NOT NULL DROP TABLE ${outputtable}_old"
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error dropping ${outputtable}_old - aborting" 1

if [ ! -z ${backup} ]
then
  echo "Dropping ${tempoutputtable} if it exists on ${backup}"
  isql -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b
  check_return_code $? "Error dropping ${tempoutputtable} - aborting" 1

  # also delete old results table
  echo "Dropping ${outputtable}_old if it exists on ${backup}"
  isql -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b
  check_return_code $? "Error dropping ${outputtable}_old - aborting" 1
fi

# now create temporary table for output results
echo "Creating ${tempoutputtable}"
sql="CREATE TABLE ${tempoutputtable} (AlacraID VARCHAR(255) NOT NULL,AlacraRegion VARCHAR(255) NULL,AlacraState VARCHAR(255),AlacraField1 VARCHAR(255) NULL"
if [[ ${fieldtwotype} != 'NULL' ]]
then
	sql+=",AlacraField2 VARCHAR(255) NULL"
fi
for i in `seq 1 $numAlacraNonMatchingFields`
do
	sql+=",AlacraNonMatchingField$i VARCHAR(255) NULL"
done
sql+=",VendorID VARCHAR(255) NOT NULL,VendorRegion VARCHAR(255) NULL,VendorState VARCHAR(255),VendorField1 VARCHAR(255) NULL"
if [[ ${fieldtwotype} != 'NULL' ]]
then
	sql+=",VendorField2 VARCHAR(255) NULL"
fi
for j in `seq 1 $numVendorNonMatchingFields`
do
	sql+=",VendorNonMatchingField$j VARCHAR(255) NULL"
done
sql+=",Score INT NOT NULL"
sql+=",MatchType VARCHAR(50) NULL"
sql+=",VendorIDDupe INT NOT NULL"
sql+=",DupeFlag char(1) NULL"
sql+=",ComparedWords varchar(500) NULL"
sql+=",MissingLeft INT NULL"
sql+=",MissingRight INT NULL"
sql+=",MissingStopWordLeft INT NULL"
sql+=",MissingStopWordRight INT NULL"
sql+=",TrailingS INT NULL"
sql+=",OutOfOrder char(1) NULL"
sql+=",parenthesis int NULL"
#new code, has new flags and extra#mismatch flags
if [ "$numAlacraNonMatchingFields" == "5" ]
then
	sql+=",wordCountLeft int NULL"
	sql+=",wordCountRight int NULL"
	sql+=",wordScoreLeft int NULL"
	sql+=",wordScoreRight int NULL"
	sql+=",regionMismatch char(1) NULL"
	sql+=",stateMismatch char(1) NULL"
	extraFields=$(echo $(($numAlacraNonMatchingFields < $numVendorNonMatchingFields?$numAlacraNonMatchingFields:$numVendorNonMatchingFields)))
	for i in `seq 1 $extraFields`
	do
		sql+=",extra${i}Mismatch CHAR(1) NULL"
	done
fi
sql+=")"

sqlFile=createTable$id.sql
echo $sql > $sqlFile

echo "Load to primary server ${server}"
isql -U${user} -P${password} -S${server} -h-1 -n -b -l0 < $sqlFile
check_return_code $? "Error creating ${tempoutputtable} - aborting" 1

bcp ${tempoutputtable} in ${outputfile} -b10000 -c -t"|" -r"\n" -S${server} -U${user} -P${password} -e ${outputfile}_${server}.err
check_return_code $? "Error bcping output into ${tempoutputtable} for job ${id} - aborting" 1

if [ ! -z ${backup} ]
then
  echo "Load to secondary server ${backup}"
  isql -U${user} -P${password} -S${backup} -h-1 -n -b -l0  < $sqlFile
  check_return_code $? "Error creating ${tempoutputtable} - aborting" 1

  bcp ${tempoutputtable} in ${outputfile} -b10000 -c -t"|" -r"\n" -S${backup} -U${user} -P${password} -e ${outputfile}_${backup}.err 
  check_return_code $? "Error bcping output into ${tempoutputtable} for job ${id} - aborting" 1  
fi

end=`date`
echo "${end}: Load output file to database for job ${id} - completed" 

rm $sqlFile

}

#----------------------------------------------------------------

update_tables()
{
start=`date`
echo "${start}: Update log table and replace output table for job ${id}"

echo "Rename tables on primary server ${server}"
sql="exec sp_rename '${outputtable}', '${outputtable}_old'"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -l0

sql="exec sp_rename '${tempoutputtable}', '${outputtable}'"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -l0
check_return_code $? "Error renaming new output table to current output table for job ${id} on ${server} - aborting" 1

if [ ! -z ${backup} ]
then
  echo "Rename tables on secondary server ${backup}"  
  sql="exec sp_rename '${outputtable}', '${outputtable}_old'"
  isql -n -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b -l0

  sql="exec sp_rename '${tempoutputtable}', '${outputtable}'"
  isql -n -U${user} -P${password} -S${backup} -Q"${sql}" -h-1 -n -b -l0
  check_return_code $? "Error renaming new output table to current output table for job ${id} on ${backup} - aborting" 1
fi

filesize=$(stat -c%s "${outputfile}")
rowcount=$((`wc -l "${outputfile}" | awk '{print $1'}`-1))

sql=" UPDATE match_jobs SET lastUpdated = GETDATE(), jobStatus = 100, filename='match_${description}_output.txt', filesize=${filesize}, matches=${rowcount}, lockerfileid='${locker_file_id}' WHERE id = ${id}"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error updating log table for job ${id} - aborting" 1

end=`date`
echo "${end}: Update log table and replace output table for job ${id} - completed" 

}

#----------------------------------------------------------------

generate_dmo_events()
{

start=`date`
echo "${start}: Generating dmo events for job ${id}"

#echo "Rename tables on primary server ${server}"
#sql="exec sp_rename '${outputtable}', '${outputtable}_old'"
#isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -l0


isql -S ${server} -U ${user} -P ${password} -Q " exec up_process_auto_matching_updates ${id}"
if [ $? -ne 0 ]
then
	echo "Error generating dmo events for ${id}"
	exit 1
else
	end=`date`
	echo "${end}: Generating dmo events for job ${id} completed." 
fi

}

#----------------------------------------------------------------

clean_up()
{
start=`date`
echo "${start}: Cleaning up job id: ${id}"

rm -f ${alacrafile}
rm -f ${vendorfile}
rm -f ${configfile}
rm -f ${stopword1file}
rm -f ${abbreviation1file}
if [[ ${fieldtwotype} != 'NULL' ]]
then
	rm -f ${stopword2file}
	rm -f ${abbreviation2file}
fi
rm -f ${regionfile}
rm -f ${ignorewordfile}
rm -f ${outputfile}*

end=`date`
echo "${end}: Cleaning up job id ${id} - completed" 

}
run_matcher_ge() {
  if [ $id = "1061" -o $id = "1062" -o $id = "542" -o $id = "10475049" -o $id = "10475050" -o $id = "10475051" -o $id = "517" -o $id = "833" -o $id = "892" -o $id = "10475165" ]
  then
    echo "calling ge matcher:" `date`
    nohup $XLS/src/scripts/matching/matcher/matcher_ge.sh ${server} ${user} ${password} $id ${outputtable} > log/match_ge_${TODAY}_${id}.log 2>&1 &
    echo "done with ge matcher:" `date`
  fi
}

#----------main routines-----------------------------------------

get_next_job

clean_up		

create_alacra_file

create_vendor_file

create_stopword_files

create_ignore_word_file

create_region_file

create_config_file

run_matcher_ge

run_matcher

email_file

load_file_to_database

load_files_to_locker

update_tables

generate_dmo_events

#mkdir -p ${TODAY}
#mv ${alacrafile} ${TODAY}/
#mv ${vendorfile} ${TODAY}/
#mv ${outputfile} ${TODAY}/match_${description}_output.txt

exit 0