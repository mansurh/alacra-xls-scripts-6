TITLEBAR="Scoring job"

if [ $# != 2 ]
then
    echo "Usage: match_daily_main.sh id error_emaillist"
    echo "Example: match_daily_main.sh 1 administrators@alacra.com"
    exit 1
fi

id=$1
error_emaillist=$2

LOADDIR=${XLSDATA}/matcher
SCRIPTDIR=$XLS/src/scripts/matching/matcher

XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

db=concordance
user=`get_xls_registry_value ${db} user`
password=`get_xls_registry_value ${db} password`
server=`get_xls_registry_value ${db} server`

# Remove the log file
mkdir -p ${LOADDIR}/log
updatelog=${LOADDIR}/log/score_update`date +%Y%m%d`_${id}.log

now=`date`
echo "Start Time: ${now}"

echo "Scoring job ${id}" > ${updatelog}

# Perform the update
${SCRIPTDIR}/score_daily_main.sh ${id} ${error_emaillist} >> ${updatelog}
returncode=$?

if [ $returncode != 0 ]
then
     # first mark the job in error
     sql="update score_jobs set jobstatus=200,lastupdated =getdate() where id='${id}'"
     isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -s":" -b -w"1000" >> ${updatelog}

     # Mail the results to the administrators
     blat ${updatelog} -t "${error_emaillist}" -s "Scoring job ${id} - FAILURE"
     echo "END Scoring job ${id} - FAILURE" >> ${updatelog}
     now1=`date`
     echo "End Time: ${now1}"
     exit $returncode
fi

echo "END Scoring job ${id} - SUCCESS" >> ${updatelog}
now1=`date`
echo "End Time: ${now1}"

exit $returncode