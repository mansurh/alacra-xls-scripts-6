# Parse the command line arguments.
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = CS Server
# 6 = CS Login
# 7 = CS Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_cs.sh IPID_server IPID_database IPID_login IPID_password cs_server cs_login cs_password"
	exit 1
fi


# Save the runstring parameters in local variables
IPID_server=$1
IPID_database=$2
IPID_logon=$3
IPID_password=$4
cs_server=$5
cs_logon=$6
cs_password=$7


# ipid_cs table has been populated by the loading script: see hemscott/update_local.sh

exit 0
 
