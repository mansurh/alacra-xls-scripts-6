# Script file to build the IP matching table for Investext
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
if [ $# -lt 3 ]
then
	echo "Usage: ipid_disclosure.sh xls_server xls_login xls_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3

ipidname=ipid_disclosure
SCRIPTDIR=$XLS/src/scripts/matching

# Name of the temp file to use
TMPFILE=${ipidname}.dat

# Name of the table to use
TABLENAME=${ipidname}

# Name of the format file to use
FORMAT_FILE=${SCRIPTDIR}/${ipidname}.fmt

# Step 1 - remove an old temp file
rm -f ${TMPFILE}

# Step 2 - run shredding program to pull out Disclosure
# ipid information through DIW API.
startime="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "${startime} : Start shredding..."
echo ""
ipidDisclosure.exe
if [ $? -ne 0 ]
then
	echo "Error in shredding, Exiting"
	exit 1
fi
endtime="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "${endtime} : End shredding..."
echo ""

# Step 3 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 4 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} < ${SCRIPTDIR}/${ipidname}_create.sql
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 5 - load the temporary data file.
bcp ${TABLENAME} in ${TMPFILE} /S${xlsserver} /U${xlslogin} /P${xlspassword} /f${FORMAT_FILE} 
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi
rm -f ${TMPFILE}

# Step 6 - create indexes
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} < ${SCRIPTDIR}/${ipidname}_createindex.sql
if [ $? -ne 0 ]
then
	echo "Error creating indexes, exiting"
	exit 1
fi

# Step 7 - map the country codes
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} < ${SCRIPTDIR}/${ipidname}_country.sql
if [ $? -ne 0 ]
then
	echo "Error mapping country codes, exiting"
	exit 1
fi

exit 0
