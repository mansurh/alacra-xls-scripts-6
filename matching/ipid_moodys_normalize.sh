# Script file to add normalized name column to ipid tables
# Created 26 April 2005 by John Willcox
# Revised 22 June 2005

# 1 = Working directory
# 2 = Log
# 3 = IPID server
# 4 = IPID database
# 5 = IPID login
# 6 = IPID password
# 7 = IPID table to be altered
# 8 = name column
# 9 = name column length

ARGS=9
if [ $# -ne $ARGS ]
then
	echo "Usage: normalize.sh WD Log IPIDserver IPIDdatabase IPIDlogin IPIDpassword IPIDtable nameColumn nameColumnLength" >> $LOG
	exit 1
fi

WD=$1
LOG=$2
IPIDserver=$3
IPIDdatabase=$4
IPIDlogin=$5
IPIDpassword=$6
IPIDtable=$7
nameColumn=$8
nameColumnLength=$9

normalizedNameColumn=normalized_${nameColumn}
SQLfile=${WD}/ipid_moodys_stopwords.sql
stopwordsFilename=${WD}/ipid_moodys_stopwords.txt

# Drop and add normalized name column

echo "Dropping and adding normalized name column" >> $LOG

isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL >> $LOG
DROP INDEX ${IPIDtable}.${IPIDtable}_norm
GO
ALTER TABLE ${IPIDtable}
DROP COLUMN ${normalizedNameColumn}
GO
ALTER TABLE ${IPIDtable}
ADD ${normalizedNameColumn} varchar(${nameColumnLength})
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = UPPER(${nameColumn})
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Encountered problem dropping and adding normalized name column" >> $LOG
	exit 1
fi

# Removing punctuation

echo "Removing punctuation" >> $LOG

isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL >> $LOG
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, ' ', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, ',', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, "'", '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '.', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '&', 'AND')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '-', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '(', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, ')', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '!', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '*', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '@', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '/', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '\', '')
GO
UPDATE ${IPIDtable}
SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, ':', '')
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Could not remove punctuation" >> $LOG
	exit 1
fi

# Normalizing words

echo "Normalizing words" >> $LOG

file_length=175
file_length=`expr $file_length + 0`
count=2

> $SQLfile

while [ $count -lt $file_length ]
do

short_name=`head -n$count $stopwordsFilename | tail -1`
count=`expr $count + 1`
long_name=`head -n$count $stopwordsFilename | tail -1`

echo "UPDATE ${IPIDtable}" >> $SQLfile
echo "SET ${normalizedNameColumn} = REPLACE(${normalizedNameColumn}, '${short_name}', '${long_name}')" >> $SQLfile

count=`expr $count + 1`

done

echo "GO" >> $SQLfile

isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile} >> $LOG
if [ $? != 0 ]
then
	echo "Problem substituting stopwords - exiting" >> $LOG
	exit 1
fi

# Create index

echo "Creating index" >> $LOG
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL >> $LOG
CREATE INDEX ${IPIDtable}_norm on ${IPIDtable}
	(${normalizedNameColumn})
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Problem creating index - exiting" >> $LOG
	exit 1
fi

echo "Normalizing done" >> $LOG
