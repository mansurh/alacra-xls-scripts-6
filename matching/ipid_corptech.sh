# Script file to build the IP matching table for CorpTech
# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = CorpTech Server
# 6 = CorpTech Login
# 7 = CorpTech Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: $0 IPID_server IPID_database IPID_login IPID_password corptech_server corptech_login corptech_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
corptechserver=$5
corptechlogin=$6
corptechpassword=$7

ipname=corptech

SCRIPTDIR=$XLS/src/scripts/matching

# Name of the temp file to use
TMPFILE1=ipid_corptech1.tmp
TMPFILE2=ipid_corptech.tmp

# Name of the table to use
TABLENAME=ipid_corptech

# Step 1 - remove any old temp files
#rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the corptech data into a temporary file
# Get the most recent version of the company name for the matching
# process

isql -S${corptechserver} -U${corptechlogin} -P${corptechpassword} -s"|" -n -w4000 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select 'matchkey'=a.CorpTechID, 'name'=a.CompanyName, 'address'=a.Address,
'city'=a.city, 'state'=a.state, 'country'=a.Country, 'url'=a.HomePage, 'type'=b.description, 
'ticker'=a.tickersymbol, 'parent'=a.parentURI, a.salesactual
from etable a, ownership_type b
where a.ownership=b.code and a.ownership in ('PRV','PUB')
go
HERE

# Step 3 - post-process the temp file
sed -f ${SCRIPTDIR}/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	matchkey varchar(15) NULL,
	name varchar(255) NULL,
	address varchar(255) NULL,
	city varchar(60) NULL,
	state varchar(30) NULL,
	country varchar(30) NULL,
	url varchar(120) NULL,
	type varchar(15) NULL,
	ticker varchar(25) NULL,
	parent varchar(15) NULL,
	salesactual float NULL

)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(state)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(parent)
GO

HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c /t"|" /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} < ${SCRIPTDIR}/ipid_corptech.sql

# Step 8 - clean up
rm -f ${TMPFILE1} ${TMPFILE2}
