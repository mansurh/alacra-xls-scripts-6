
XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 1 ]
then
	echo "Usage: RunDirectMapping.sh dbname [SkipCountCheckFlag 1/0] [component]"
	exit 1
fi

if [ $# -eq 1 ]
then
	skipCountCheck=0
else
	skipCountCheck=$2  #1 - skip check
fi

dbname=$1
comp=$3
srcloc=${XLS}/src/scripts/matching
out_dir=$XLSDATA/DirectMapping/$dbname$comp

run() {
mkdir -p $out_dir
cd $out_dir
cserver=`get_xls_registry_value "concordance" "Server" "0"`
cuser=`get_xls_registry_value "concordance" "User" "0"`
cpass=`get_xls_registry_value "concordance" "Password" "0"`

server=`get_xls_registry_value ${dbname} "Server" "0"`
user=`get_xls_registry_value ${dbname} "User" "0"`
pass=`get_xls_registry_value ${dbname} "Password" "0"`

echo "Database:           $dbname"
echo "Concordance Server: $cserver"
dbname=${dbname}${comp}

echo "bcp sp_CreateDirectMappingTable ${comp} ..."
bcp "exec sp_CreateDirectMappingTable ${comp}" queryout direct_mapping.dat -S$server -U$user -P$pass -c -t"|"
if [ $? -ne 0 ]
then
	echo "Error exec sp_CreateDirectMappingTable ${comp}, exiting ..."
	return 1
fi

echo "create new table direct_mapping_${dbname}_new ..."
isql /S${cserver} /U${cuser} /P${cpass} /Q "drop table direct_mapping_${dbname}_new"

isql /S${cserver} /U${cuser} /P${cpass} /b /Q "select top 0 * into direct_mapping_${dbname}_new from direct_mapping_${dbname} "
if [ $? -ne 0 ]
then
	echo "Error creating new table direct_mapping_${dbname}_new, exiting ..."
	return 1
fi

bcp direct_mapping_${dbname}_new in direct_mapping.dat -S$cserver -U$cuser -P$cpass -c -t"|"
if [ $? -ne 0 ]
then
	echo "Error in bcp concordance direct_mapping_${dbname}_new, exiting ..."
	return 1
fi

echo "skip row count check flag: ${skipCountCheck}"
if  [ ! ${skipCountCheck} == '1' ]
then
	typeset -i filecount=`wc -l direct_mapping.dat |cut -f1 -d" "`
	echo "Row count check"
	$XLS/src/scripts/matching/check_ipid_row_counts.sh ${cserver} ${cuser} ${cpass} direct_mapping_${dbname} ${filecount}
	if [ $? -ne 0 ]
	then
		echo "ERROR: Direct Mapping ${dbname} count failed, exiting ..."
		return 1
	fi
else
	echo "Skipping the row count check"
fi

echo "create index direct_mapping01"
isql /S${cserver} /U${cuser} /P${cpass} /Q "CREATE NONCLUSTERED INDEX [direct_mapping01] ON [dbo].[direct_mapping_${dbname}_new] ([direct_mapping_value] ASC) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] " /b
if [ $? -ne 0 ]
then
	echo "Error creating index direct_mapping01 on direct_mapping_${dbname}_new, exiting ..."
	return 1
fi

#echo "create index direct_mapping02"
#isql /S${cserver} /U${cuser} /P${cpass} /Q "CREATE INDEX direct_mapping02 ON direct_mapping_${dbname}_new (sourcekey, direct_mapping_value)" /b
#if [ $? -ne 0 ]
#then
#	echo "Error creating index direct_mapping02 on direct_mapping_${dbname}_new, exiting ..."
#	return 1
#fi
isql /b /S${cserver} /U${cuser} /P${cpass} /Q "drop table direct_mapping_${dbname}_backup"

isql /S${cserver} /U${cuser} /P${cpass} /b << HERE
exec sp_rename 'direct_mapping_${dbname}', 'direct_mapping_${dbname}_backup'
go
exec sp_rename 'direct_mapping_${dbname}_new', 'direct_mapping_${dbname}'
go
HERE
if [ $? -ne 0 ]
then
	echo "Error renaming tables, exiting ..."
	return 1
fi

echo "DONE"
}

filename=$XLSDATA/DirectMapping/${dbname}$comp`date +%y%m%d`.log
run > ${filename} 2>&1

if [ $? -ne 0 ]
then
	blat ${filename} -t administrators@alacra.com -s "${dbname} Direct Mapping update failed."
	exit 1
else
	#blat ${filename} -t administrators@alacra.com -s "${dbname} Direct Mapping update."
	#rm ${filename}
	exit 0
fi

exit 0


