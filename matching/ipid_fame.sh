# Script file to build the IP matching table for Investext
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
if [ $# -lt 3 ]
then
	echo "Usage: ipid_ibes.sh xls_server xls_login xls_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3

ipname=fame

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 3 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} (
	ticker varchar(12) NULL,
	sedol varchar (12) NULL,
	name varchar(60) NULL,
	country varchar(60) NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(sedol)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(country)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 4 - preprocess and load all the international files
for famefile in fame_gpeu.txt fame_gpfe.txt fame_gpuks.txt fame_gpwh.txt
do
	sed -f fameintl.sed < ${famefile} > ${TMPFILE1}
	bcp ${TABLENAME} in ${TMPFILE1} /S${xlsserver} /U${xlslogin} /P${xlspassword} /f${FORMAT_FILE} 
	if [ $? -ne 0 ]
	then
		echo "Error in BCP, exiting"
		exit 1
	fi
	rm -f ${TMPFILE1}
done

# Step 5 - preprocess and load the north american file
for famefile in fame_idce.csv
do
	sed -e "/^0/d;/0$/d" < ${famefile} > ${TMPFILE1}
	sed -f famena.sed < ${TMPFILE1} > ${TMPFILE2}
	bcp ${TABLENAME} in ${TMPFILE2} /S${xlsserver} /U${xlslogin} /P${xlspassword} /f${FORMAT_FILE} 
	if [ $? -ne 0 ]
	then
		echo "Error in BCP, exiting"
		exit 1
	fi
	rm -f ${TMPFILE1} ${TMPFILE2}
done

# Step 6 - map the country codes
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} < $XLS/src/scripts/matching/ipid_${ipname}_country.sql
