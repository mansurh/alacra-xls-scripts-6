XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Script file to build the IP matching table for Firstcall Estimates
# Created 23 June 2005 by John Willcox
# Amended 7 March 2006 JW

WD=$XLS/src/scripts/matching
DATADIR=$XLSDATA/matching/firstcall
mkdir -p ${DATADIR}
cd ${DATADIR}


# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password

ARGS=4
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_firstcallestimates.sh ipid_server ipid_database ipid_login ipid_password" 
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
#FTPserver=$5
#FTPlogin=$6
#FTPpassword=$7
FTPserver=`get_xls_registry_value firstcallconcord Server 6`
FTPlogin=`get_xls_registry_value firstcallconcord User 6`
FTPpassword=`get_xls_registry_value firstcallconcord Password 6`

# Function to retrieve data from Firstcall FTP server

retrieve_files() {

echo "Retrieving data file from Firstcall FTP server" 
ftp -i -n ${FTPserver} << END_FTP 
user ${FTPlogin} ${FTPpassword}
binary
get ${getUSFilename}
get ${getJPFilename}
get ${getROWFilename}
quit
END_FTP
if [ $? != 0 ]
then
	echo "FTP process failed - exiting" 
	exit 1
fi
echo "Retrieved data file" 

# Ensure output file exists

if [ ! -e $getUSFilename ]
then
	echo "Data file not found - exiting" 
	exit 1
fi
if [ ! -e $getJPFilename ]
then
	echo "Data file not found - exiting" 
	exit 1
fi
if [ ! -e $getROWFilename ]
then
	echo "Data file not found - exiting" 
	exit 1
fi
}

# Function to drop existing ipid table

drop_ipid_table() {
table=$1
echo "Dropping ${table}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
drop table ${table}
go
ENDSQL
if [ $? != 0 ]
then
	echo "Connecting to SQL server failed - exiting" 
	exit 1
fi
}

# Function to create new ipid table

create_new_table() {
table=$1
echo "Creating ${table}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
create table ${table} (
	matchkey varchar(15) not NULL,
	name varchar(31) NULL,
	cusip varchar(10) NULL,
	sedol varchar(6) NULL,
	isin varchar(11) NULL,
	ric varchar(8) NULL,
	ticker varchar(12) NULL,
	country varchar(40) NULL
)
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Could not create table - exiting" 
	exit 1
fi
}

# Function to bulk copy file into table

bulk_copy() {
filename=$1
table=$2
echo "Bulk copying ${filename} into ${table}" 
bcp ${table} in ${filename} /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /f${formatFilename} /t /b 5000 -m255
if [ $? != 0 ]
then
	echo "BCP not completed - exiting" 
	exit 1
fi
}

# Function to remove first character of each line

remove_first_character() {
filename=$1
echo "Removing first character from ${filename} inputting from ${filename} outputting to EDIT${filename}" 
cut -c2- ${filename} > EDIT${filename}
if [ $? != 0 ]
then
	echo "Problem removing first character - exiting" 
	exit 1
fi
}

# Function to remove headers from IPID table

delete_headers() {
table=$1
echo "Removing headers from ${table}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
delete from ${table} where matchkey='tfn' and name='companyname' and cusip='cusip'
ENDSQL
if [ $? != 0 ]
then
	echo "Problem removing headers - exiting" 
	exit 1
fi
}

# Function to add regional identifier to matchkey

format_matchkey() {
table=$1
identifier=$2
echo "Adding regional identifier ${identifier} to ${table}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
update ${table}
set matchkey = '${identifier}:' + ltrim(matchkey)
go
ENDSQL
if [ $? != 0 ]
then
	echo "Problem adding regional identifier to matchkey - exiting" 
	exit 1
fi
}

# Function to transfer data from one table to another

transfer() {
sourceTable=$1
destinationTable=$2
echo "Copying data from ${sourceTable} to ${destinationTable}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
insert into ${destinationTable} select * from ${sourceTable}
go
ENDSQL
if [ $? != 0 ]
then
	echo "Problem copying - exiting" 
	exit 1
fi
}

# Function to empty table completely

empty_table() {
table=$1
echo "Deleting all data from ${table}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
delete from ${table}
go
ENDSQL
if [ $? != 0 ]
then
	echo "Problem clearing table - exiting" 
	exit 1
fi
}

# Formatting countries to match Alacra countries

format_countries() {
table=$1
echo "Formatting countries to match Alacra countries" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
UPDATE ${table}
SET country = REPLACE(country, 'SOU', 'South Africa')
GO
UPDATE ${table}
SET country = REPLACE(country, 'RIA', 'Romania')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CAR', 'Argentina')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CAS', 'Austria')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CAU', 'Australia')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CBE', 'Belgium')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CBM', 'Bermuda')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CBZ', 'Brazil')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CCA', 'Canada')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CCH', 'China')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CDE', 'Denmark')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CES', 'Estonia')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CFI', 'Finland')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CFR', 'France')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CGR', 'Greece')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CHK', 'Hong Kong')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CHL', 'Chile')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CID', 'India')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CIN', 'Indonesia')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CIR', 'Republic of Ireland')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CIS', 'Israel')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CIT', 'Italy')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CJP', 'Japan')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CKO', 'South Korea')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CML', 'Malaysia')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CMO', 'Morocco')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CMX', 'Mexico')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CNE', 'Netherlands')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CNO', 'Norway')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CNZ', 'New Zealand')
GO
UPDATE ${table}
SET country = REPLACE(country, 'COL', 'Colombia')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CPH', 'Philippines')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CPK', 'Pakistan')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CPO', 'Portugal')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CRC', 'Croatia')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CRU', 'Russia')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CSL', 'Sri Lanka')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CSM', 'Singapore')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CSP', 'Spain')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CSV', 'Slovakia')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CSW', 'Sweden')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CSZ', 'Switzerland')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CTA', 'Taiwan')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CTH', 'Thailand')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CTU', 'Turkey')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CUK', 'United Kingdom')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CUS', 'United States')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CWG', 'Germany')
GO
UPDATE ${table}
SET country = REPLACE(country, 'CZE', 'Czech Republic')
GO
UPDATE ${table}
SET country = REPLACE(country, 'EGT', 'Egypt')
GO
UPDATE ${table}
SET country = REPLACE(country, 'HUN', 'Hungary')
GO
UPDATE ${table}
SET country = REPLACE(country, 'ICL', 'Iceland')
GO
UPDATE ${table}
SET country = REPLACE(country, 'PER', 'Peru')
GO
UPDATE ${table}
SET country = REPLACE(country, 'POL', 'Poland')
GO
UPDATE ${table}
SET country = REPLACE(country, 'UKR', 'Ukraine')
GO
UPDATE ${table}
SET country = REPLACE(country, 'VEN', 'Venezuela')
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Problem formatting countries - exiting" 
	exit 1
fi
}

# Function to delete rows with country 'ZZZ'

delete_ZZZ() {
table=$1
echo "Deleting rows with country 'ZZZ'" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
DELETE FROM ${table}
WHERE country = 'ZZZ'
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Problem deleting rows with country 'ZZZ' - exiting" 
	exit 1
fi
}

# Function to trim ticker column

trim_ticker() {
table=$1
echo "Trimming ticker column"
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
UPDATE ipid_firstcallestimates
SET ticker = RIGHT(ticker,len(ticker)-2)
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Problem trimming ticker column - exiting" 
	exit 1
fi
}

# Function to create indices

create_indices() {
echo "Creating indices" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
create index ${IPIDtable}_01 on ${IPIDtable}
	(matchkey)
GO
create index ${IPIDtable}_02 on ${IPIDtable}
	(name)
GO
create index ${IPIDtable}_03 on ${IPIDtable}
	(country)
GO
create index ${IPIDtable}_04 on ${IPIDtable}
	(cusip)
GO
create index ${IPIDtable}_05 on ${IPIDtable}
	(sedol)
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Indices not created - exiting" 
	exit 1
fi
}

# Function to clean up

clean_up() {
echo "Cleaning up" 
rm ${getUSFilename}
rm ${getJPFilename}
rm ${getROWFilename}
rm EDIT${getUSFilename}
rm EDIT${getJPFilename}
rm EDIT${getROWFilename}

echo "Done" 
}



getUSFilename="alacra.csv"
getJPFilename="alacra_j.csv"
getROWFilename="alacra_p.csv"

IPIDtable="ipid_firstcallestimates"
tempIPIDtable="ipid_firstcallestimatestemp"

formatFilename=${WD}/firstcallestimatesbcp.fmt

if [ $? != 0 ]
then
	echo "could not find formatFilename"
	exit 1
fi

retrieve_files
remove_first_character ${getUSFilename}
remove_first_character ${getJPFilename}
remove_first_character ${getROWFilename}
drop_ipid_table ${IPIDtable}
drop_ipid_table ${tempIPIDtable}
create_new_table ${IPIDtable}
create_new_table ${tempIPIDtable}
bulk_copy EDIT${getUSFilename} ${tempIPIDtable}
delete_headers ${tempIPIDtable}
format_matchkey ${tempIPIDtable} C
transfer ${tempIPIDtable} ${IPIDtable}
empty_table ${tempIPIDtable}
bulk_copy EDIT${getJPFilename} ${tempIPIDtable}
delete_headers ${tempIPIDtable}
format_matchkey ${tempIPIDtable} J
transfer ${tempIPIDtable} ${IPIDtable}
empty_table ${tempIPIDtable}
bulk_copy EDIT${getROWFilename} ${tempIPIDtable}
delete_headers ${tempIPIDtable}
format_matchkey ${tempIPIDtable} S
transfer ${tempIPIDtable} ${IPIDtable}
drop_ipid_table ${tempIPIDtable}
format_countries ${IPIDtable}
delete_ZZZ ${IPIDtable}
trim_ticker ${IPIDtable}
create_indices
clean_up
