XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Script to perform the first two steps in matching a third-party
# database to the Alacra Universe:
#
#   1) create and populate the IPID_XXX table in the xls database
#   2) run the cross-reference report to look for new and/or dead
#      links

# Arguments:
# 1 = IPID Database
# 5 = source Database
# 6 = source Login
# 7 = source Password
ARGS=2 # Number of Arguments expected by the script
if [ $# -ne $ARGS ]
then
    echo "Usage: xref_factset.sh IPID_Database source_database"
    exit 1
fi

IPIDdatabase=$1
srcdatabase=$2
IPIDserver=`get_xls_registry_value ${IPIDdatabase} Server`
IPIDlogin=`get_xls_registry_value ${IPIDdatabase} User`
IPIDpassword=`get_xls_registry_value ${IPIDdatabase} Password`
ipserver=`get_xls_registry_value ${srcdatabase} Server`
iplogin=`get_xls_registry_value ${srcdatabase} User`
ippassword=`get_xls_registry_value ${srcdatabase} Password`

mkdir -p $XLSDATA/matching/factset
cd $XLSDATA/matching/factset
if [ $? -ne 0 ]
then
	echo "error in cd to $XLSDATA/matching/factset, exiting ..."
	exit 1
fi

# Create a temporary logfile name to record whats going on
tmpfilename=`date "+%Y-%m-%d-%H-%M-%S"`.log

# Create a logfile name for the exception report
reportfilename=`date "+%Y-%m-%d-%H-%M-%S"`.rpt

# Step 1 - build and populate the IPID table
echo "Step 1 - build and populate the IPID table" >> ${tmpfilename}
${XLS}/src/scripts/matching/ipid_factset.sh ${IPIDserver} ${IPIDdatabase} ${IPIDlogin} ${IPIDpassword} ${ipserver} ${iplogin} ${ippassword} >> ${tmpfilename} 2>&1
if [ $? -ne 0 ]
then
    blat ${tmpfilename} -t "bofa_alacra_tech@alacra.com,bofa_alacra_concordance@alacra.com,bofa_notifications1@alacra.com" -s "Error in ipid_factset.sh"
    exit 1
fi

# Step 2 - run the exception reports
echo " " >> ${tmpfilename}
echo "Step 2 - run the exception reports" >> ${tmpfilename}
${XLS}/src/scripts/matching/ipid_factset_report.sh ${reportfilename} ${IPIDserver} ${IPIDlogin} ${IPIDpassword} >> ${tmpfilename} 2>&1
if [ $? -ne 0 ]
then
    blat ${tmpfilename} -t "bofa_alacra_tech@alacra.com,bofa_alacra_concordance@alacra.com,bofa_notifications1@alacra.com" -s "Error in ipid_factset_report.sh"
    exit 1
fi

# Email the report to the matching address
blat ${reportfilename} -t "bofa_alacra_concordance@alacra.com" -s "factset bofa concordance report" >> ${tmpfilename}

# attempt to report any problems with blat
if [ $? != 0 ]
then

    echo "error sending factset bofa report via blat" >> ${tmpfilename}
    exit 1

fi

# Everything worked, delete the log file and the report file
echo " " >> ${tmpfilename}
echo "Everything worked, delete the log file and the report file" >> ${tmpfilename}

exit 0
