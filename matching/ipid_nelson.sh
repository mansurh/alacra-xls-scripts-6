# Script file to build the IP matching table for Nelson Earnings Estimates.
# Created Colin duSaire Feb 11, 2002
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
# 4 = Nelson Server
# 5 = Nelson Login
# 6 = Nelson Password
if [ $# -lt 6 ]
then
	echo "Usage: ipid_nelson.sh xls_server xls_login xls_password nelson_server nelson_login nelson_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3
ipserver=$4
iplogin=$5
ippassword=$6

ipname=nelson

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp
#TMPFILE3=ipid_${ipname}3.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << ENDOfIsql
SET NOCOUNT ON

select a.ENTID, RTRIM(a.COMPANY), RTRIM(a.TICK2), 
RTRIM(a.EXCH), RTRIM(b.loccty), RTRIM(b.enturl)
from earn a, company b 
where a.ENTID*=b.pubid

ENDOfIsql

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}


# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} 
(
	matchkey	varchar(10),
	name		varchar(255),
	ticker		varchar(10),
	exchange	varchar(5),
	country		varchar(20),
	url		varchar(255)
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE3} -S ${xlsserver} -U ${xlslogin} -P ${xlspassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - index the table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE2
create index ${TABLENAME}_01 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(exchange)
GO
HERE2

if [ $? -ne 0 ]
then
	echo "Error indexing table, exiting"
	exit 1
fi


# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2} 
# ${TMPFILE3}
