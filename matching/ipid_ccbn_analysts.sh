XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/check_return_code.fn
. $XLSUTILS/get_registry_value.fn

# Script file to build the IP matching table for ccbn analyst ipid
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = ccbn Server
# 6 = ccbn Login
# 7 = ccbn Password
ARGS=4
check_arguments $# ${ARGS} "usage: ipid_ccbn_analysts ipid_server ipid_database ipid_login ipid_password" 1

IPIDserver=$1
IPIDdatabase=$2
IPIDlogon=$3
IPIDpassword=$4

srcdataserver=`get_xls_registry_value ccbn server`
srclogon=ccbn
srcpassword=ccbn

LOADDIR=${XLSDATA}/matching/ccbn/
mkdir -p ${LOADDIR}
cd ${LOADDIR}
check_return_code $? "Error switching to directory ${LOADDIR}, exiting" 1

query="select distinct id, company_id, first_name, middle_name, last_name, last_pub_date from analysts where last_pub_date is not null"
bcp "${query}" queryout ccbn_analysts.dat -U ccbn -P ccbn -S ${srcdataserver} -c
check_return_code $? "Error bcp-ing out ccbn analyst data, exiting" 1

isql -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -Q "select top 0 * into ipid_streetevents_analysts_tmp from ipid_streetevents_analysts"
check_return_code $? "Error creating temporary analyst ipid table, exiting" 1

bcp ipid_streetevents_analysts_tmp in ccbn_analysts.dat -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -c
check_return_code $? "Error bcp-ing in analyst ipid data, exiting" 1

isql -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -Q "delete i from ipid_streetevents_analysts i, ipid_streetevents_analysts_tmp t where i.analyst_id = t.analyst_id"
check_return_code $? "Error deleting old analyst ipid data, exiting" 1

isql -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -Q "insert into ipid_streetevents_analysts select distinct * from ipid_streetevents_analysts_tmp"
check_return_code $? "Error inserting new analyst ipid data, exiting" 1

isql -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -Q "drop table ipid_streetevents_analysts_tmp"
check_return_code $? "Error dropping temporary analyst ipid table, exiting" 1

query="select distinct company_id, company from analysts where company_id is not null and company is not null"
bcp "${query}" queryout ccbn_analysts_firms.dat -U ccbn -P ccbn -S ${srcdataserver} -c
check_return_code $? "Error bcp-ing out ccbn analyst firm data, exiting" 1

isql -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -Q "select top 0 * into ipid_streetevents_firms_tmp from ipid_streetevents_firms"
check_return_code $? "Error creating temporary analyst firm ipid table, exiting" 1

bcp ipid_streetevents_firms_tmp in ccbn_analysts_firms.dat -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -c
check_return_code $? "Error bcp-ing in analyst firm ipid data, exiting" 1

isql -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -Q "delete i from ipid_streetevents_firms i, ipid_streetevents_firms_tmp t where i.firm_id = t.firm_id"
check_return_code $? "Error deleting old analyst firm ipid data, exiting" 1

isql -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -Q "insert into ipid_streetevents_firms select * from ipid_streetevents_firms_tmp"
check_return_code $? "Error inserting new analyst firm ipid data, exiting" 1

isql -S ${IPIDserver} -U ${IPIDlogon} -P ${IPIDpassword} -Q "drop table ipid_streetevents_firms_tmp"
check_return_code $? "Error dropping temporary analyst firm ipid table, exiting" 1

exit 0
