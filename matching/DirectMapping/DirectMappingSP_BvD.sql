IF EXISTS (SELECT * FROM sysobjects WHERE name='sp_CreateDirectMappingTable' and type='P')
DROP procedure sp_CreateDirectMappingTable
go

create procedure sp_CreateDirectMappingTable 
	@Param           VARCHAR (50)
as
	if @Param = 'Fame'
	Begin
		select regd_no as sourcekey, 1 as type, regd_no as value 
		from company_fame order by regd_no
	End
	else if @Param = 'Zephyr'
	Begin
		select substring(bvd_id,3,8),1,bvd_id 
		from zephyr_bvdid 
			where  bvd_id like 'GB%' and bvd_id not like 'GB*%'
		union
		select 'E0' + substring(bvd_id,3,8),1,bvd_id 
		from zephyr_bvdid 
			where  bvd_id like 'IE%' and bvd_id not like 'IE*%'
	end
go