/****************************************************************************/
/* Create xls ipid_disclosure                                               */
/****************************************************************************/
USE xls
GO
/****************************************************************************/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id('dbo.ipid_disclosure'))
BEGIN
	PRINT ''
	PRINT 'Dropping old version of dbo.ipid_disclosure'
	PRINT ''
	DROP table dbo.ipid_disclosure
END
GO

/****************************************************************************/
PRINT ''
PRINT 'Creating Table:  dbo.ipid_disclosure'
PRINT ''
/****************************************************************************/
GO

CREATE TABLE dbo.ipid_disclosure (
	matchkey varchar(15) NULL,
	name varchar(50) NULL,
	country varchar(30) NULL,
	cusip varchar(9) NULL,
	ticker varchar(10) NULL,
	exchange varchar(10) NULL
)
GO
