# Script file to build the IP matching table for Investext
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
# 4 = MARKETG Server
# 5 = MARKETG Login
# 6 = MARKETG Password
if [ $# -lt 6 ]
then
	echo "Usage: ipid_marketg.sh xls_server xls_login xls_password marketg_server marketg_login marketg_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3
ipserver=$4
iplogin=$5
ippassword=$6

ipname=marketg

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select 
	ig.repno, ig.cusip, ig.ticker, ig.coname, ig.exch, ig.city, ig.state, ig.zip, ig.country, ig.costatus
from 
	im_gen ig
order by
	coname
HERE

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} (
	repno varchar(5) NULL,
	cusip varchar(9) NULL,
	ticker varchar(6) NULL,
	coname varchar(30) NULL,
	exchange varchar(4) NULL,
	city varchar(30) NULL,
	state varchar(2) NULL,
	zip varchar(10) NULL,
	country varchar(20) NULL,
	costatus integer NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(repno)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(cusip)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(coname)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(exchange)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(city)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(state)
GO
create index ${TABLENAME}_08 on ${TABLENAME}
	(zip)
GO
create index ${TABLENAME}_09 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_10 on ${TABLENAME}
	(costatus)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${xlsserver} -U ${xlslogin} -P ${xlspassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# step 7 - cross reference the exchange names
#isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
#update ipid_${ipname} set exchange=73 where exchange="NYSE"
#update ipid_${ipname} set exchange=3 where exchange="AMEX"
#update ipid_${ipname} set exchange=66 where exchange="NASD"
#update ipid_${ipname} set exchange=66 where exchange="OTC"
#update ipid_${ipname} set exchange=NULL where exchange not in (73,3,66)
#HERE

# step 8 - cross reference the country names
#isql /S${xlsserver} /U${xlslogin} /P${xlspassword} < ${XLS}/src/scripts/matching/ipid_${ipname}_country.sql

# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
