# Script file to build the IP matching table for Investext
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = ITEXT Server
# 6 = ITEXT Login
# 7 = ITEXT Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_itext.sh ipid_server ipid_database ipid_login ipid_password itext_server itext_login itext_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
itextserver=$5
itextlogin=$6
itextpassword=$7

# Name of the temp file to use
TMPFILE1=ipid_itext1.tmp
TMPFILE2=ipid_itext2.tmp

# Name of the table to use
TABLENAME=ipid_itext
DATE_TABLENAME=ipid_date_itext

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_itext.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the itext data into a temporary file
# We are only trying to match companies which have had research
# written on them in the past year.  The check of ticker against
# name is because investext sometimes mistakenly puts the ticker
# in the company name field of the metadata.

isql -S${itextserver} -U${itextlogin} -P${itextpassword} -s"|" -n -h-1 -w500 >${TMPFILE1} << HERE
SET NOCOUNT ON
select RTRIM(c.name), RTRIM(c.ticker), RTRIM(c.name), max(r.date_written) 
from company c, report r
where c.accession_number = r.accession_number
and r.source_code != 'CTI'
and c.ticker <> c.name
group by RTRIM(c.name),RTRIM(c.ticker),RTRIM(c.name)
HERE

# Step 3 - post-process the temp file
sed -e "s/  *|/|/g;s/  *$//g" < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	matchkey varchar(80),
	ticker varchar(12),
	name varchar(80),
	last_report datetime
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(matchkey)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - drop the old itext date table
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${DATE_TABLENAME}
go
HERE

# Step 8 - create the new mktcap table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${DATE_TABLENAME} (
	xlsid int NULL,
	last_report datetime NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 9 - Populate the table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
insert into ${DATE_TABLENAME} 
select c.id, MAX(i.last_report)
from company c, ipid_itext i, company_map m
where
m.source=30
and
m.sourcekey=i.matchkey
and
c.id = m.xlsid
group by c.id
GO
HERE

# Step 7 - clean up
# rm -f ${TMPFILE1} ${TMPFILE2}
