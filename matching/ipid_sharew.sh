# Script file to build the IP matching table for Sharew
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = Sharew Server
# 6 = Sharew Login
# 7 = Sharew Password
ARGS=7
if [ $# -lt 6 ]
then
	echo "Usage: ipid_Sharew.sh ipid_server ipid_database ipid_login ipid_password Sharew_server Sharew_login Sharew_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
sharewserver=$5
sharewlogin=$6
sharewpassword=$7

ipname=sharew

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
isql -S${sharewserver} -U${sharewlogin} -P${sharewpassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << ENDOfIsql
SET NOCOUNT ON
select s.cusip, s.name, s.class, c.name, s.cusip, 
s.ticker, c2.name 
from security s, country c, exchange c2 
where c.id =* s.issuing_country
and c2.id =* s.exchange
go
ENDOfIsql

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	cusipcins_id char(8) NOT NULL,
	name varchar(75) NOT NULL,
	class varchar(10) NOT NULL,
	country varchar(30) NULL,
	cusipcins char(8) NULL,
	ticker varchar(10) NULL,
	exchange varchar(60) NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - index the table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE2
create index ${TABLENAME}_01 on ${TABLENAME}
	(cusipcins_id)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(cusipcins)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(exchange)
GO
HERE2
if [ $? -ne 0 ]
then
	echo "Error indexing table, exiting"
	exit 1
fi


# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
