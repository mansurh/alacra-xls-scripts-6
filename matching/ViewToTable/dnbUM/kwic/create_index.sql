
IF EXISTS(SELECT * FROM sys.indexes WHERE name='ipid_dnbUM_kwic01' AND object_id = OBJECT_ID('ipid_dnbUM_kwic_new'))        
BEGIN 
	DROP INDEX [ipid_dnbUM_kwic01] on ipid_dnbUM_kwic_new 
END


CREATE CLUSTERED INDEX [ipid_dnbUM_kwic01] ON [dbo].[ipid_dnbUM_kwic_new] 
(
	[value] ASC,
	[dunsNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


