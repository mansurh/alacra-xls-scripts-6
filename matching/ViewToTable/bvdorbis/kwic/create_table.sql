IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
				 WHERE
                 TABLE_NAME = 'ipid_bvdorbis_kwic' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
	drop table ipid_bvdorbis_kwic
END
CREATE TABLE [dbo].[ipid_bvdorbis_kwic](
       [id] [varchar](40) NULL,
       [value] [varchar](255) NULL
 
) ON [PRIMARY]