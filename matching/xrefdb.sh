XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Script to perform the first two steps in matching a third-party
# database to the Alacra Universe:
#
#	1) create and populate the IPID_XXX table in the xls database
#	2) run the cross-reference report to look for new and/or dead
#	   links

# Arguments:
# 1 = ipid to match
# 2 = ipid database
# 3 = srcdatabase -- optional -- if present, assume there is a database specific ipid table (if set to "none", don't run ipid report)
# 4 = skipevents -- optional -- if present, don't run dmo_events script)
if [ $# -lt 2 ]
then
	echo "Usage: xrefdb.sh ipidname IPID_Database srcdatabase"
	exit 1
fi

ipname=$1
IPIDdatabase=$2
srcdatabase=$3
skipevents=$4

IPIDserver=`get_xls_registry_value ${IPIDdatabase} Server`
IPIDlogin=`get_xls_registry_value ${IPIDdatabase} User`
IPIDpassword=`get_xls_registry_value ${IPIDdatabase} Password`

workdir=$XLSDATA/matching/${ipname}/
mkdir -p ${workdir}
cd ${workdir}
datename=`date "+%Y-%m-%d-%H"`
# Create a temporary logfile name to record whats going on
tmpfilename=${workdir}/${datename}.log

# Step 1 - build and populate the IPID table
echo "Step 1 - build and populate the IPID table" > ${tmpfilename}
if [ -z ${srcdatabase} ] || [ ${srcdatabase} == "xrefdb" ]
then
	${XLS}/src/scripts/matching/xrefdb_ipid.sh ${IPIDserver} ${IPIDdatabase} ${IPIDlogin} ${IPIDpassword} ${ipname} >> ${tmpfilename} 2>&1
	if [ $? -ne 0 ]
	then
		blat ${tmpfilename} -t "administrators@alacra.com,ipidalerts@alacra.com" -s "Error in xrefdb_ipid.sh for ${ipname}"
		exit 1
	fi
elif [ ${srcdatabase} == "concordance" ]
then
	echo "error in usage: xrefdb.sh ipname database type" >> ${tmpfilename}
	blat ${tmpfilename} -t administrators@alacra.com -s "Error in ipid_${ipname}.sh"
	exit 1
elif [ ${srcdatabase} != "none" ]
then
	${XLS}/src/scripts/matching/ipid_${ipname}.sh ${IPIDserver} ${IPIDdatabase} ${IPIDlogin} ${IPIDpassword} >> ${tmpfilename} 2>&1
	if [ $? -ne 0 ]
	then
		blat ${tmpfilename} -t "administrators@alacra.com,ipidalerts@alacra.com" -s "Error in ipid_${ipname}.sh"
		exit 1
	fi
fi

#echo "Step 1a - update mappings with latest updatedate for ${ipname}" >> ${tmpfilename}

if [ -z ${skipevents} ]
then
	echo "Step 2 - generate ipid change events for ${ipname}" >> ${tmpfilename}
	$XLS/src/scripts/matching/dmo_events.sh ${ipname} ${IPIDdatabase} ${IPIDserver} ${IPIDlogin} ${IPIDpassword} >> ${tmpfilename} 2>&1
	if [ $? -ne 0 ]
	then
		echo "error generating ipid change events for ${ipname}, exiting ..." >> ${tmpfilename}
		blat ${tmpfilename} -t "administrators@alacra.com,ipidalerts" -s "Error in $0 for ${ipname}"
		exit 1
	fi
fi

#NO LONGER NEED TO SEND REPORTS FOR ADD/DELETES TO CONCORDANCE
# Create a logfile name for the exception report
#reportfilename=${workdir}/${datename}.rpt
#
#if [ -s ${XLS}/src/scripts/matching/ipid_${ipname}_report.sh ]
#then
#  # Step 2 - run the exception reports
#  echo " " >> ${tmpfilename}
#  echo "Step 2 - run the exception reports" >> ${tmpfilename}
#  ${XLS}/src/scripts/matching/ipid_${ipname}_report.sh ${reportfilename} ${IPIDserver} ${IPIDlogin} ${IPIDpassword} >> ${tmpfilename} 2>&1
#  if [ $? -ne 0 ]
#  then
#	blat ${tmpfilename} -t administrators@alacra.com -s "Error in ipid_${ipname}_report.sh"
#	exit 1
#  fi
#
#  # Email the report to the matching address
#  blat ${reportfilename} -t companymatch@alacra.com -s "Exceptions report for ${ipname} on ${IPIDserver}"
#else
#  echo "no ipid_${ipname}_report.sh" >> ${tmpfilename}
#fi

# Everything worked, delete the log file and the report file
echo " " >> ${tmpfilename}
echo "Everything worked, delete the log file and the report file" >> ${tmpfilename}
#rm -f ${tmpfilename} ${reportfilename}

exit 0
