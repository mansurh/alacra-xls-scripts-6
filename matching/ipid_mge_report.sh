# Script file to generate the IP matching reports for mge
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_mge_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=mge

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in mge"
select * from ipid_mge where matchkey not in (
  select sourcekey from security_map where source = 7
)


/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from mge"
select c.id, c.name from company c, security s, security_map m
where
c.id = s.issuer and s.id = m.id and m.source=7
and
m.sourcekey not in (select matchkey from ipid_mge)

/* Optional report - new information available in database */

HERE
