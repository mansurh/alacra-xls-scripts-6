# Script file to generate the IP matching reports for BNI
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_bni.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=bni

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in BNI database */
PRINT "Potentially new companies in BNI"
select * from ipid_bni
where matchkey not in (select sourcekey from company_map where source=10)


/* Required Report - Companies which have disappeared from BNI database */
PRINT "Matched companies that have disappeared from BNI"
select x.id, x.name, c.name from company x, company_map m, country c
where
x.id = m.xlsid and m.source=10 and x.country*=c.id
and
m.sourcekey not in (select matchkey from ipid_bni)

/* Optional report - Name changes, same itext ticker */
PRINT "Mapped Investext Companies that may have changed Name"
select "xlsID"=a.id, "xlsName"=a.name, "FM1"=a.former_name1,
"FM2"=a.former_name2,"Country"=x.name, 
"Old Mapping"=b.sourcekey, "Old Last Date"=convert(char(15),c.last_report,106), 
"Possible New Mapping"=d.matchkey, "New Last Date"=convert(char(15),d.last_report,106)
from company a, company_map b, ipid_itext c, ipid_itext d, country x
where a.id=b.xlsid and b.source=30 and b.sourcekey=c.matchkey
and a.country=x.id
and c.ticker=d.ticker
and not exists
(select * from company_map where d.matchkey=sourcekey and source=30)
order by x.name, d.last_report

HERE
