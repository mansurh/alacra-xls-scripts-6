Unique ID: ${_uuid}


Basic Information
First: ${first}
Last: ${last}
Company: ${company}
Email: ${email}
Job: ${job}
Otherjob: ${otherjob}


Alacra Experience
Alacra is easy to use: ${easy2use}
Alacra is logically organized: ${logicallyorganized}
Alacra is reliable and dependable: ${reliable}
Alacra search interfaces work well: ${searchscreens}
I usually find what I need in Alacra: ${ifindwhatineed}
Over time, the likelihood of Alacra having what I need has improved: ${hasimproved}
Alacra Customer Care is available and helpful: ${Customercare}
Alacra E-mail updates are helpful: ${updates}


Alacra Enhancements
What applications or functionality would you most like to see added to Alacra: ${enhancements}
If you could make one change in Alacra what would it be: ${OneChange}

Company Snapshot Page
In Alacra, I search primarily for: ${isearchfor}
Ranking
Employee Size: ${copageempsize}
Stock Charts: ${copagecharts}
Top Competitors: ${copagetopcomp}
News: ${copagenews}
Investor Relations link: ${copageir}
Other data: ${othercopgdata}


Dun's Market Identifiers or D&B Credit Reports
Do you currently use either D&B Market Identifiers or Credit Reports: ${useDBMIorCR}
>From where do you access D&B information: ${getDBfrom}
Other Provider: ${otherdbprovider}


Choices for Premium Databases
ABI: ${ABI}
BarraTR: ${BarraTR}
Cahners: ${Cahners}
Compustat: ${Compustat}
D&B Market Identifiers: ${DBMI}
D&B Credit Reports: ${DBCR}
D&B Business Information Reports: ${DBBIR}
Hoovers: ${Hoovers}
Lionshares: ${Lionshares}
Moody: ${Moody}
Sectorbase: ${Sectorbase}
Standard & Poor's Ratings Direct: ${SPRD}
ValueLine: ${ValueLine}
Worldscope: ${Worldscope}

Does your organization/department use FactSet: ${FactSet}


Does your organization/department use Piranha Web: ${Piranha}


Over the next 12 months, your firm will invest in: ${investin}


Do you need Alacra Model: ${AlacraModel}


Have you heard of Alacra Book: ${heardofAlacrabook}
Do you want a demo: ${wantdemo}
Are you interested in a product/industry Alacra Book: ${prod_ind}


Operating System: ${opersys}
MS Excel version: ${excelversion}
Web Browser: ${browser}
Otherbrowser: ${otherbrowser}
Browser Version: ${browserversion}

Comments: ${comments}

