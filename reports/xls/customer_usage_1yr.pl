use Win32::OLE;
use OLE;
use Date::Manip;

if (@ARGV < 1){
	die "Usage: perl customer_usage_1yr.pl <last date + 1>\n";
}

my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!"; 
my $connect="DRIVER={SQL SERVER};PWD=xls;UID=xls;SERVER=data3;DATABASE=xls"; 
$conn->open($connect);

my $conn2=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!"; 
my $connect2="DRIVER={SQL SERVER};PWD=xls;UID=xls;SERVER=data3;DATABASE=xls"; 
$conn2->open($connect2);

my $conn3=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!"; 
my $connect3="DRIVER={SQL SERVER};PWD=xls;UID=xls;SERVER=data3;DATABASE=xls"; 
$conn3->open($connect3);

$main::TZ = "EST5EDT";	#Need this to datemanip
$currentDate = &ParseDate($ARGV[0]);
for ($i=0; $i<=12; $i++){
	$monthsback = " -" . $i . " month";
	$cutoffDate  = &DateCalc($currentDate, $monthsback); 
	$daterange[$i] = &UnixDate($cutoffDate, "%m/%d/%Y");
	#print &UnixDate($cutoffDate, "%m/%d/%Y\n");
}

#Start by getting all accounts
my $sql="Select * from account order by name";
my $rs=$conn->Execute($sql) || die "Execute Error: $!";
my $fcnt = $rs->Fields->Count;
#Loop thru all accounts
while ( !$rs->EOF){
	#Clear each months sum total holder
	for ($i=1; $i<=12; $i++){
		$sumprice[$i] = 0;
	}
	$account = $rs->Fields('id')->value;
	print $account;
	print "|";
	print $rs->Fields('name')->value;

	#Get all the users for this account
	my $sql2 = " Select id from users where account = " . $account;
	my $rs2=$conn2->Execute($sql2) || die "Execute Error: $!";
	while ( !$rs2->EOF){
		$id = $rs2->Fields('id')->value;
		for ($i=12; $i>=1; $i--){
			#Get sum(price) for this user for each month
			my $sql3 = " Select sum(price) as price from usage where access_time <= '$daterange[$i - 1]' and access_time > '$daterange[$i]' and userid = " . $id;
			my $rs3=$conn3->Execute($sql3) || die "Execute Error(${sql3}): $!";
			while ( !$rs3->EOF ){
				$oneprice = $rs3->Fields('price')->value;
				#print "!${oneprice}!";
				$sumprice[$i] += $oneprice;	
				$rs3->MoveNext;
			}
			$rs3->Close;
		}
		$rs2->MoveNext;
	}
	#Print the sum for all users of this account for each month
	for ($i=12; $i>=1; $i--){
		print "|";
		print $sumprice[$i];
	}
	$rs2->Close;

	$rs->MoveNext;
	print "\n";
}
$rs->Close;

$conn->Close;
$conn2->Close;
$conn3->Close;




                    