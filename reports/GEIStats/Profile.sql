set nocount on
set transaction isolation level read uncommitted

select CASE WHEN PubCoClass is not NULL THEN 'PublicCorp' WHEN Fund_Type is NOT NULL THEN 'Fund' ELSE 'Company' END as profile,
			Entity_Status, COALESCE(Fund_Type, '(none)'), COUNT(*) as [Count]
FROM
	ipid_gei
GROUP BY CASE WHEN PubCoClass is not NULL THEN 'PublicCorp' WHEN Fund_Type is NOT NULL THEN 'Fund' ELSE 'Company' END, Entity_Status, COALESCE(Fund_Type, '(none)')
ORDER BY 1, 2, 3
