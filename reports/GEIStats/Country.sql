set nocount on
set transaction isolation level read uncommitted

/* Statistics by Country */
select i.Country, COUNT(*) as [Count]
FROM ipid_gei i
GROUP BY i.Country
ORDER BY COUNT(*) desc
