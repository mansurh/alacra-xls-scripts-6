set nocount on
set transaction isolation level read uncommitted

select COALESCE([Legal_Form], '(none)') as [LegalForm], COUNT(*) as [Count]
FROM
	ipid_gei
GROUP BY Legal_Form
ORDER BY COUNT(*) desc
