set nocount on
set transaction isolation level read uncommitted

/* Statistics by Country */
select s.countryname 'Country', count(distinct ep.property_value) 'Companies', count(*) 'Quotes'
from 
event e,
event_type et,
event_property ep,
property_spec ps,
snapshot1 s
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD(day,-30,GETDATE())
and
e.id = ep.event_id
and
ep.property_id = ps.property_id
and
ps.event_type = et.id
and
ps.xpath = 'CompanyId'
and
ep.property_value = s.xlsid
group by s.countryname
order by count(distinct ep.property_value) desc
