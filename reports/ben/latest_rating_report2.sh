shname=$0

XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

server=`get_xls_registry_value concordance server`
user=`get_xls_registry_value concordance user`
password=`get_xls_registry_value concordance password`

fitchserver=`get_xls_registry_value fitch server`
fitchuser=`get_xls_registry_value fitch user`
fitchpassword=`get_xls_registry_value fitch password`

moodysserver=`get_xls_registry_value moodys server`
moodysuser=`get_xls_registry_value moodys user`
moodyspassword=`get_xls_registry_value moodys password`

spcredserver=`get_xls_registry_value spcred server`
spcreduser=`get_xls_registry_value spcred user`
spcredpassword=`get_xls_registry_value spcred password`


SCRIPTDIR=$XLS/src/scripts/reports/ben



echo "${shname}: create the empty latest_rating_report table in concordance db"
isql -U ${user} -P ${password} -S ${server} -b < ${SCRIPTDIR}/create_latest_rating_report.sql
check_return_code $? "creating latest_rating_report failed." 2



echo "${shname}: INITIAL QUERY FOR STORE COMPANIES"
isql /U ${user} /P ${password} /S ${server} << HERE
	INSERT INTO latest_rating_report (xlsid)
		  select xlsid from snapshot1 where is_store_mapped = 1
HERE
check_return_code $? "latest_rating_report query failed" 2



echo "${shname}: CREATE LATEST_RATING_TEMP TABLE IN CONCORDANCE DB"
isql -U ${user} -P ${password} -S ${server} -b < ${SCRIPTDIR}/create_latest_rating_temp.sql
check_return_code $? "creating latest_rating_temp failed." 2


# ------------------------------ FITCH --------------------------------------

mapping_id=229


echo "${shname}: CREATE CONCORDANCE_MAPPED_IDS_TEMP TABLE IN FITCH DB"
isql -U ${fitchuser} -P ${fitchpassword} -S ${fitchserver} -b < ${SCRIPTDIR}/create_concordance_mapped_ids_temp.sql
check_return_code $? "creating concordance_mapped_ids_temp failed." 2



echo "${shname}: COPY MAPPED IDs TO FITCH TEMP TABLE"
query=" \
	select DISTINCT sourcekey, NULL, NULL, NULL FROM company_map m JOIN latest_rating_report r ON m.xlsid=r.xlsid WHERE m.source = ${mapping_id} \
"
desttable=concordance_mapped_ids_temp
$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" concordance ${server} ${user} ${password} ${desttable} fitch ${fitchserver} ${fitchuser} ${fitchpassword} temp.dat
check_return_code $? "populating concordance_mapped_ids_temp failed" 2
rm temp.dat


echo "${shname}: UPDATE DATA IN FITCH TEMP TABLE WITH ITS GDA TABLES"
isql /U ${fitchuser} /P ${fitchpassword} /S ${fitchserver} << HERE
	UPDATE concordance_mapped_ids_temp 
	SET latest_doc_id = z.latest_doc_id,
		latest_doc_date	= z.latest_doc_date
	FROM concordance_mapped_ids_temp t
	JOIN (
		SELECT p1.prop_value as company_id, SUBSTRING( max( RTRIM(p2.prop_value) + ':' + p2.doc_id ),  12, 255) as latest_doc_id, 
				CAST( SUBSTRING( max( RTRIM(p2.prop_value) + ':' + p2.doc_id ), 1, 10) as datetime) as latest_doc_date
		from doc_props p1 
		join doc_props p2 on p1.doc_id = p2.doc_id 
		where p1.prop_name = 'companyid' 
		and p2.prop_name = 'date' 
		GROUP BY p1.prop_value
	) z on z.company_id = t.local_id
	GO
	UPDATE concordance_mapped_ids_temp 
	SET url = 'http://www.alacrastore.com/research/fitch-ratings-' + master.dbo.removePunctAndSpaces(p1.prop_value) + '-' + latest_doc_id
	FROM concordance_mapped_ids_temp t
	join doc_props p1 on p1.doc_id = t.latest_doc_id and p1.prop_name = 'title'
HERE
check_return_code $? "latest_rating_report query failed" 2




echo "${shname}: COPY ALL MAPPED FITCH DATA INTO LATEST_RATING_TEMP TABLE IN CONCORDANCE"
query=" \
	SELECT 	local_id, latest_doc_id, latest_doc_date, url FROM concordance_mapped_ids_temp
"
desttable=latest_rating_temp
$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" fitch ${fitchserver} ${fitchuser} ${fitchpassword} ${desttable} concordance ${server} ${user} ${password} temp.dat
check_return_code $? "populating latest_rating_temp failed" 2
rm temp.dat



echo "${shname}: UPDATE OUR TABLE WITH THE FITCH DATA"
isql /U ${user} /P ${password} /S ${server} << HERE
	UPDATE latest_rating_report
	SET fitch_url = b.url, fitch_date = b.latest_doc_date
	FROM latest_rating_report r
	join company_map m on r.xlsid = m.xlsid and m.source = ${mapping_id}
	JOIN latest_rating_temp b on b.local_id = m.sourcekey
HERE
check_return_code $? "fitch update query failed" 2


echo "${shname}: CLEAR TEMP TABLE"
isql /U ${user} /P ${password} /S ${server} << HERE
	TRUNCATE TABLE latest_rating_temp
HERE
check_return_code $? "latest_rating_temp truncation failed" 2



# ------------------------------ S&P --------------------------------------

mapping_id=232


echo "${shname}: CREATE CONCORDANCE_MAPPED_IDS_TEMP TABLE IN SPCRED DB"
isql -U ${spcreduser} -P ${spcredpassword} -S ${spcredserver} -b < ${SCRIPTDIR}/create_concordance_mapped_ids_temp.sql
check_return_code $? "creating concordance_mapped_ids_temp failed." 2



echo "${shname}: COPY MAPPED IDs TO SPCRED TEMP TABLE"
query=" \
	select DISTINCT sourcekey, NULL, NULL, NULL FROM company_map m JOIN latest_rating_report r ON m.xlsid=r.xlsid WHERE m.source = ${mapping_id} \
"
desttable=concordance_mapped_ids_temp
$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" concordance ${server} ${user} ${password} ${desttable} spcred ${spcredserver} ${spcreduser} ${spcredpassword} temp.dat
check_return_code $? "populating concordance_mapped_ids_temp failed" 2
rm temp.dat



echo "${shname}: UPDATE DATA IN SPCRED TEMP TABLE WITH ITS GDA TABLES"
isql /U ${spcreduser} /P ${spcredpassword} /S ${spcredserver} << HERE
	UPDATE concordance_mapped_ids_temp 
	SET latest_doc_id = z.latest_doc_id,
		latest_doc_date	= z.latest_doc_date
	FROM concordance_mapped_ids_temp t
	JOIN (
		SELECT p1.entry_text as company_id, SUBSTRING( max( RTRIM(p2.prop_value) + ':' + p2.doc_id ),  12, 255) as latest_doc_id, 
				CAST( SUBSTRING( max( RTRIM(p2.prop_value) + ':' + p2.doc_id ), 1, 10) as datetime) as latest_doc_date
		from index_entries p1 
		join doc_props p2 on p1.doc_id = p2.doc_id 
		where p1.entry_name = 'primissuerid' 
		and p2.prop_name = 'date' 
		GROUP BY p1.entry_text
	) z on z.company_id = t.local_id
	GO
	UPDATE concordance_mapped_ids_temp 
	SET url = 'http://www.alacrastore.com/research/s-and-p-credit-research-' + master.dbo.removePunctAndSpaces(p1.prop_value) + '-' + latest_doc_id
	FROM concordance_mapped_ids_temp t
	join doc_props p1 on p1.doc_id = t.latest_doc_id and p1.prop_name = 'title'
HERE
check_return_code $? "latest_rating_report query failed" 2




echo "${shname}: COPY ALL MAPPED SPCRED DATA INTO LATEST_RATING_TEMP TABLE IN CONCORDANCE"

query=" \
	SELECT 	local_id, latest_doc_id, latest_doc_date, url FROM concordance_mapped_ids_temp
"
desttable=latest_rating_temp
$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" spcred ${spcredserver} ${spcreduser} ${spcredpassword} ${desttable} concordance ${server} ${user} ${password} temp.dat
check_return_code $? "populating latest_rating_temp failed" 2
rm temp.dat



echo "${shname}: UPDATE OUR TABLE WITH THE S&P DATA"
isql /U ${user} /P ${password} /S ${server} << HERE
	UPDATE latest_rating_report
	SET spcred_url = b.url, spcred_date = b.latest_doc_date
	FROM latest_rating_report r
	join company_map m on r.xlsid = m.xlsid and m.source = ${mapping_id}
	JOIN latest_rating_temp b on b.local_id = m.sourcekey
HERE
check_return_code $? "spcred update query failed" 2



echo "${shname}: CLEAR TEMP TABLE"
isql /U ${user} /P ${password} /S ${server} << HERE
	TRUNCATE TABLE latest_rating_temp
HERE
check_return_code $? "latest_rating_temp truncation failed" 2





# ------------------------------ MOODYS --------------------------------------

mapping_id=56


echo "${shname}: CREATE CONCORDANCE_MAPPED_IDS_TEMP TABLE IN MOODYS DB"
isql -U ${moodysuser} -P ${moodyspassword} -S ${moodysserver} -b < ${SCRIPTDIR}/create_concordance_mapped_ids_temp.sql
check_return_code $? "creating concordance_mapped_ids_temp failed." 2



echo "${shname}: COPY MAPPED IDs TO MOODYS TEMP TABLE"
query=" \
	select DISTINCT sourcekey, NULL, NULL, NULL FROM company_map m JOIN latest_rating_report r ON m.xlsid=r.xlsid WHERE m.source = ${mapping_id} \
"
desttable=concordance_mapped_ids_temp
$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" concordance ${server} ${user} ${password} ${desttable} moodys ${moodysserver} ${moodysuser} ${moodyspassword} temp.dat
check_return_code $? "populating concordance_mapped_ids_temp failed" 2
rm temp.dat



echo "${shname}: UPDATE DATA IN MOODYS TEMP TABLE WITH ITS GDA TABLES"
isql /U ${moodysuser} /P ${moodyspassword} /S ${moodysserver} << HERE
	UPDATE concordance_mapped_ids_temp 
	SET latest_doc_id = z.latest_doc_id,
		latest_doc_date	= z.latest_doc_date
	FROM concordance_mapped_ids_temp t
	JOIN (
		SELECT p1.prop_value as company_id, SUBSTRING( max( RTRIM(p2.prop_value) + ':' + p2.doc_id ),  12, 255) as latest_doc_id, 
				CAST( SUBSTRING( max( RTRIM(p2.prop_value) + ':' + p2.doc_id ), 1, 10) as datetime) as latest_doc_date
		from doc_props p1 
		join doc_props p2 on p1.doc_id = p2.doc_id 
		where p1.prop_name = 'org_id' 
		and p2.prop_name = 'date' 
		GROUP BY p1.prop_value
	) z on z.company_id = t.local_id
	GO
	UPDATE concordance_mapped_ids_temp 
	SET url = 'http://www.alacrastore.com/research/moodys-global-credit-research-' + master.dbo.removePunctAndSpaces(p1.prop_value) + '-' + latest_doc_id
	FROM concordance_mapped_ids_temp t
	join doc_props p1 on p1.doc_id = t.latest_doc_id and p1.prop_name = 'title'
HERE
check_return_code $? "latest_rating_report query failed" 2




echo "${shname}: COPY ALL MAPPED MOODYS DATA INTO LATEST_RATING_TEMP TABLE IN CONCORDANCE"

query=" \
	SELECT 	local_id, latest_doc_id, latest_doc_date, url FROM concordance_mapped_ids_temp
"
desttable=latest_rating_temp
$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" moodys ${moodysserver} ${moodysuser} ${moodyspassword} ${desttable} concordance ${server} ${user} ${password} temp.dat
check_return_code $? "populating latest_rating_temp failed" 2
rm temp.dat


echo "${shname}: UPDATE OUR TABLE WITH THE MOODYS DATA"
isql /U ${user} /P ${password} /S ${server} << HERE
	UPDATE latest_rating_report
	SET moodys_url = b.url, moodys_date = b.latest_doc_date
	FROM latest_rating_report r
	join company_map m on r.xlsid = m.xlsid and m.source = ${mapping_id}
	JOIN latest_rating_temp b on b.local_id = m.sourcekey
HERE
check_return_code $? "moodys update query failed" 2



echo "${shname}: CLEAR TEMP TABLE"
isql /U ${user} /P ${password} /S ${server} << HERE
	TRUNCATE TABLE latest_rating_temp
HERE
check_return_code $? "latest_rating_temp truncation failed" 2




echo "${shname}: latest_rating_report TABLE HAS BEEN CREATED SUCCESSFULLY"

exit 0