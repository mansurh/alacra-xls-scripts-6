use concordance
go

if exists (select * from dbo.sysobjects where id = object_id(N'[latest_rating_report]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [latest_rating_report]
go

CREATE TABLE latest_rating_report
(
	xlsid	int NULL,
	moodys_date datetime NULL,
	moodys_url varchar(512) NULL,
	fitch_date datetime NULL,
	fitch_url varchar(512) NULL,
	spcred_date datetime NULL,
	spcred_url varchar(512) NULL,
)
GO

CREATE INDEX latest_rating_report_01 ON latest_rating_report(xlsid)
GO
