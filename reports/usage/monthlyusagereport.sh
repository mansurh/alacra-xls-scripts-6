XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/get_specific_date.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database [startdate] [enddate]"
	exit 1
fi

accountlist=$XLS/src/scripts/reports/usage/account.lst
database=$1

if [ $# -gt 1 ]
then
  prevMonthDate=$2
  currentMonthDate=$3
else
  prevMonth=`get_specific_date -20 112 6`
  prevMonthDate="${prevMonth:0:4}-${prevMonth:4:2}-01"
  currentMonthDate=`date +%Y-%m-01`
fi

run() {
  logon=`get_xls_registry_value ${database} User`
  password=`get_xls_registry_value ${database} Password`
  dataserver=`get_xls_registry_value ${database} Server`


  if [ $prevMonthDate = $currentMonthDate ]
  then
	echo "Previous and current month are the same, exiting ..."
	return 1
  fi

  echo "Running usage for periods of $prevMonthDate to $currentMonthDate"
  if [ ! -s $accountlist ]
  then
	echo "Account list ($accountlist) not found, exiting ..."
	return 1
  fi

  rm -f *.xls
  IFS="|"
  while read accountid accountname emailgrp 
  do
	echo "Runing usage for account: $accountid, $accountname"
	usagefile=usage${accountid}.xls
	query="select ip.name, convert(varchar(20),access_time,100), u.login, description, remote_address, us.list_price from usage us, users u, ip  where us.userid=u.id and account=$accountid and us.access_time >= '$prevMonthDate' and us.access_time < '$currentMonthDate' and ip.id = us.ip"
	bcp "$query" queryout ${usagefile} /U${logon} /P${password} /S${dataserver} /c 
	if [ $? -ne 0 ]
	then
		echo "Error getting data for $accountid, exiting ..."
		return 1
	fi
	echo "Download summary for account $accountname" > messagebody
	blat messagebody -t "$emailgrp" -cc operations@alacra.com -f "customer_service@alacra.com" -attach "${usagefile}" -s "Download Summary for $accountname"
  done < $accountlist

}

mkdir -p $XLSDATA/usage/log
cd $XLSDATA/usage
logfile=$XLSDATA/usage/log/xlsusage`date +%y%m%d`.log
run > $logfile 2>&1
if [ $? -ne 0 ]
then
	blat ${logfile} -t administrators@alacra.com -s "Monthly XLS Usage failed"
	exit 1
fi

