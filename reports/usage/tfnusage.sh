# tfnusage.sh
XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database"
	exit 1
fi

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`

echo -e "logon=${logon}"
echo -e "password=${password}"
echo -e "dataserver=${dataserver}"

# IP constants
ip_itext=68
ip_markintel=79
ip_iinsider=89


if [ $# -gt 1 ]
then
	updatedate=$2

	# date was supplied on command line - use it
	imonth=${updatedate#[0-9][0-9]}
	iyear=${updatedate%[0-9][0-9]}

	# Convert year into 4 digits
	if [ $iyear -gt 80 ]
	then
		year4=19${iyear}
	else
		year4=20${iyear}
	fi

	# Translate month number to name
	case $imonth in
	01)		strmonth=January;;
	02)		strmonth=February;;
	03)		strmonth=March;;
	04)		strmonth=April;;
	05)		strmonth=May;;
	06)		strmonth=June;;
	07)		strmonth=July;;
	08)		strmonth=August;;
	09)		strmonth=September;;
	10)		strmonth=October;;
	11)		strmonth=November;;
	*)		strmonth=December;;
	esac

	lastmonth="${strmonth} 1, ${year4}"

	# Translate month number to name
	case $imonth in
	02)		strmonth=January;;
	03)		strmonth=February;;
	04)		strmonth=March;;
	05)		strmonth=April;;
	06)		strmonth=May;;
	07)		strmonth=June;;
	08)		strmonth=July;;
	09)		strmonth=August;;
	10)		strmonth=September;;
	11)		strmonth=October;;
	12)		strmonth=November;;
	*)		strmonth=December;;
	esac

	thismonth="${strmonth} 1, ${year4}"

else

	# date was not supplied on command line - ask sql server what last month was
	lastmonth=$(isql /S ${dataserver} /U${logon} /P${password} /n /h-1 /Q"SELECT DATENAME(month, DATEADD(month,-1,GETDATE())) + ' 1, ' + CONVERT(char(4), DATEPART(year,DATEADD(month,-1,GETDATE())))" | grep -v "(" )
	# remove trailing spaces
	space2star="  *"
	lastmonth=${lastmonth%%$space2star}
	# remove leading space
	aspace=" "
	lastmonth=${lastmonth#$aspace}

	thismonth=$(isql /S ${dataserver} /U${logon} /P${password} /n /h-1 /Q"SELECT DATENAME(month, GETDATE()) + ' 1, ' + CONVERT(char(4), DATEPART(year, GETDATE()))" | grep -v "(" )
	# remove trailing spaces
	thismonth=${thismonth%%$space2star}
	# remove leading space
	thismonth=${thismonth#$aspace}


	numberdate=$(isql /S ${dataserver} /U${logon} /P${password} /n /h-1 /Q"SELECT CONVERT(char(2), DATEPART(month,DATEADD(month,-1,GETDATE()))) + ',' + CONVERT(char, DATEPART(year,DATEADD(month,-1,GETDATE())))" | grep -v "(" )
	# remove any blanks
	numberdate=$( echo "${numberdate}" | sed "s/ //g" )

	year4=${numberdate##*,}
	iyear=${year4#??}

	imonth=${numberdate%%,*}
	# force leading zero if req.
	if [ $imonth -lt 10 ]
	then
		imonth=0${imonth}
	fi

fi


echo -e ""
echo -e "lastmonth=<${lastmonth}>"
echo -e "thismonth=<${thismonth}>"
echo -e "iyear=<${iyear}>"
echo -e "imonth=<${imonth}>"
echo -e "year4=<${year4}>"
echo -e ""


datestr=$lastmonth
echo -e "tfnusage.sh: Reporting usage for ${datestr}"


# Query the usage for all the databases into a usage file for each

allthree="itext markintel"

for db in $allthree
do
	eval thisip='$'ip_${db}
	echo "tfnusage.sh: Extracting ${db} data into ${db}.out  (ip=${thisip})"

	# Extract the data
	isql /S${dataserver} /U${logon} /P${password} /n /w1024 /h-1 /Q"EXEC extract_tfn_usage '${datestr}', ${thisip}" > ${db}.out
	if [ $? != 0 ]
	then
		echo "tfnusage.sh: Error returned from isql, Exiting"
		exit 1
	fi
done

#exit 0



# Build an FTP script file
rm -f temp.ftp
echo "user ddl mbdisg5" > temp.ftp
for db in $allthree
do
	# Look up the proper file name extension
	case $db in
	itext)		xfile=_INV;;
	markintel)	xfile=_MI;;
	*)			xfile=_II;;
	esac

	# create file name as Thomson wants it and rename our file
	xmitfilename=ddl${iyear}${imonth}${xfile}
	mv ${db}.out ${xmitfilename}
	if [ $? != 0 ]
	then
		echo "tfnusage.sh: Error renaming ${db}.out to ${xmitfilename}"
		exit 1
	fi

	# Add the command to script file
	echo "put ${xmitfilename}" >> temp.ftp
done

echo "bye" >> temp.ftp


# Invoke FTP to send the files
ftp -i -n -s:temp.ftp ftp.tfresearch.com
# Test the exit status of ftp
if [ $? != 0 ]
then
	echo "tfnusage.sh: Error in ftp, Exiting"
	exit 1
fi


# Clean up
rm -f temp.ftp


exit 0
