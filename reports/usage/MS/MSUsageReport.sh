XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Want a table that has: 
# ReportName|SheetName|Format|Database|Query

#emailDestination="Susan.Ryan@morganstanley.com,adam.krug@alacra.com,lauren.elliott@alacra.com"
#emailDestination="tun.he@alacra.com"
emailDestination="morganusage@alacra.com"


# Run this for previous month
month=`date +%m`
year=`date +%Y`
month=`expr $month - 1`

if [ $month = 0 ]
then
	year=`expr $year - 1 `
	month=12
fi
if [ $month = 1 ]
then
	monthName='January'
	alacralogTable="alacralog_archive_${year}01"
elif [ $month = 2 ]
then
	monthName='February'
	alacralogTable="alacralog_archive_${year}02"
elif [ $month = 3 ]
then
	monthName='March'
	alacralogTable="alacralog_archive_${year}03"
elif [ $month = 4 ]
then
	monthName='April'
	alacralogTable="alacralog_archive_${year}04"
elif [ $month = 5 ]
then
	monthName='May'
	alacralogTable="alacralog_archive_${year}05"
elif [ $month = 6 ]
then
	monthName='June'
	alacralogTable="alacralog_archive_${year}06"
elif [ $month = 7 ]
then
	monthName='July'
	alacralogTable="alacralog_archive_${year}07"
elif [ $month = 8 ]
then
	monthName='August'
	alacralogTable="alacralog_archive_${year}08"
elif [ $month = 9 ]
then
	monthName='September'
	alacralogTable="alacralog_archive_${year}09"
elif [ $month = 10 ]
then
	monthName='October'
	alacralogTable="alacralog_archive_${year}${month}"
elif [ $month = 11 ]
then
	monthName='November'
	alacralogTable="alacralog_archive_${year}${month}"
elif [ $month = 12 ]
then
	monthName='December'
	alacralogTable="alacralog_archive_${year}${month}"
fi

lastReport='X'

IFS='|'
while read report sheet database format query
do

	# Determine if this is the start of a new report
	if [ ${report} != ${lastReport} ] 
	then

		if [ ${lastReport} != 'X' ]
		then

			# Wrap up the last report
			echo "wrap up previous report"
			cat < postamble.xml >>${lastReport}.xml
			if [ $? != 0 ]
			then
				echo "Error wrapping up report.xml file, terminating" >&2
				exit 1
			fi

			# Zip the report file
			mv ${lastReport}.xml ${lastReport}_${year}${monthName}.xml
			if [ $? != 0 ] 
			then
				echo "Error renaming report.xml file, terminating" >&2
				exit 1
			fi

			zip report.zzz ${lastReport}_${year}${monthName}.xml
			if [ $? != 0 ]
			then
				echo "Error zipping ${lastReport}.xml file, terminating" >&2
				exit 1
			fi


			# Mail out the report.  Must be a binary attachment as UTF-8 uses the high bit
			blat -to ${emailDestination} -subject "Morgan Stanley Usage Statistics - ${lastReport}" -body "See Attached" -uuencode -attach report.zzz
			if [ $? != 0 ]
			then
				echo "Error sending report file via Blat, terminating" >&2
				exit 1
			fi

			# Clean up the file
			rm -f ${lastReport}_${year}${monthName}.xml
			rm -f report.zzz
		fi

		# Start the new report	
		cat< preamble.xml >${report}.xml
		if [ $? != 0 ]
		then
			echo "Error creating ${report}.xml file, terminating" >&2
			exit 1
		fi

	
	fi

	# Remember the last report
	lastReport=${report}


	# Retrieve the connection parameters for the query
	dbserver=`get_xls_registry_value ${database} Server`
	dbuser=`get_xls_registry_value ${database} User`
	dbpassword=`get_xls_registry_value ${database} Password`

	# Run the query for the sheet
	query=`echo "${query}" | sed -e"s/alacralog_archive_XXXXXX/${alacralogTable}/"`
	echo "query=${query}"
	sqlcmd -S ${dbserver} -U ${dbuser} -P ${dbpassword} -Q "set transaction isolation level read uncommitted; set nocount on; DECLARE @periodStart datetime; DECLARE @periodEnd datetime; select @periodStart='${monthName} ${year}'; select @periodEnd=DATEADD(month,1,@periodStart); ${query}" -o ${sheet}.rpt -W -s"|" -h -1 < /dev/null
	if [ $? != 0 ]
	then
		echo "Error running SQLQuery ${query}, terminating" >&2
		exit 1
	fi 


	# Convert The Rows of Data into XML
	sed -f ${format}.sed < ${sheet}.rpt > ${sheet}.xml

	# Convert to the UTF-8 Character Set
	iconv -f ISO-8859-1 -t UTF-8 < ${sheet}.xml > ${sheet}_utf.xml
	if [ $? != 0 ]
	then
		echo "Error converting output to UTF-8, terminating" >&2
		exit 1
	fi 


	# Append together the Excel bits of the worksheet
	sed -e "s/XXXXXX/${sheet}/" < ${format}Start.xml >> ${report}.xml
	if [ $? != 0 ]
	then
		echo "Error adding sheet ${sheet} to report file, terminating" >&2
		exit 1
	fi 

	for bit in ${sheet}_utf.xml WorksheetEnd.xml
	do
		cat <${bit}  >> ${report}.xml
		if [ $? != 0 ]
		then
			echo "Error concatenating ${bit} to report file, terminating" >&2
			exit 1
		fi 
	done


	# Clean up for sheet
	rm -f ${sheet}.rpt
	rm -f ${sheet}.xml
	rm -f ${sheet}_utf.xml


done < specfile.txt

# Wrap up the last report
if [ ${lastReport} != 'X' ]
then
	echo "wrap up last report"
	cat < postamble.xml >>${lastReport}.xml
	if [ $? != 0 ]
	then
		echo "Error wrapping up report.xml file, terminating" >&2
		exit 1
	fi

	# Zip the report file
	mv ${lastReport}.xml ${lastReport}_${year}${monthName}.xml
	if [ $? != 0 ] 
	then
		echo "Error renaming report.xml file, terminating" >&2
		exit 1
	fi

	zip report.zzz ${lastReport}_${year}${monthName}.xml
	if [ $? != 0 ]
	then
		echo "Error zipping ${lastReport}.xml file, terminating" >&2
		exit 1
	fi


	# Mail out the report.  Must be a binary attachment as UTF-8 uses the high bit
	blat -to ${emailDestination} -subject "Morgan Stanley Usage Statistics - ${lastReport}" -body "See Attached" -uuencode -attach report.zzz
	if [ $? != 0 ]
	then
		echo "Error sending report file via Blat, terminating" >&2
		exit 1
	fi

	# Clean up the file
	rm -f ${lastReport}_${year}${monthName}.xml
	rm -f report.zzz
fi

exit 0
