set nocount on
set tran isolation level read uncommitted

PRINT "Mintel Weekly Usage Report"
PRINT ""
PRINT ""
select CONVERT(varchar(16),GETDATE(),107) "Today's Date"
PRINT ""
PRINT ""

PRINT "================================================================="
select distinct 
	a.id,
	convert(char(30),a.name)'Name',
	convert(char(20),u.login)'Login',
	convert(char(30),u.first_name+' '+u.last_name)'User Name',
	convert(char(100),g.description)'Description',
	g.access_time,
	convert(char(10),g.price)'Price',
	convert(char(10),g.list_price)'List Price'
from usage g join users u on g.userid=u.id
join account a on u.account=a.id
where g.ip=196 and 
u.demo_flag is null and g.access_time>=DATEADD(day,-7,GETDATE())
order by g.access_time desc,a.id
