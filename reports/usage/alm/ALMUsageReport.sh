XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

report="ALM_Usage_Report"
emailDestination="iliusage@alacra.com"

# Start the new report	
cat< preamble.xml >${report}.xml
if [ $? != 0 ]
then
	echo "Error creating ${report}.xml file, terminating" >&2
	exit 1
fi

# Retrieve the connection parameters for the query
dbserver=`get_xls_registry_value xls Server`
dbuser=`get_xls_registry_value xls User`
dbpassword=`get_xls_registry_value xls Password`

# Start the sheet in the workbook
cat Start.xml >> ${report}.xml
if [ $? != 0 ]
then
	echo "Error adding sheet to report file, terminating" >&2
	exit 1
fi

# Do for the separate parts of the report
for part in Headers Body
do

	# Run the query for the content
	sqlcmd -S ${dbserver} -U ${dbuser} -P ${dbpassword} -i AlmReport${part}.sql -o ${part}.rpt -W -s"|" -h -1 < /dev/null
	if [ $? != 0 ]
	then
		echo "Error running SQLQuery for ${part}, terminating" >&2
		exit 1
	fi 

	# Convert The Rows of Data into XML
	sed -f ${part}.sed < ${part}.rpt > ${part}.xml

	# Convert to the UTF-8 Character Set
	iconv -f ISO-8859-1 -t UTF-8 < ${part}.xml > ${part}_utf.xml
	if [ $? != 0 ]
	then
		echo "Error converting output to UTF-8, terminating" >&2
		exit 1
	fi 


	cat <${part}_utf.xml  >> ${report}.xml
	if [ $? != 0 ]
	then
		echo "Error concatenating ${bit} to report file, terminating" >&2
		exit 1
	fi 

	# Clean up for part
	rm -f ${part}.rpt
	rm -f ${part}.xml
	rm -f ${part}_utf.xml

done 

# End the worksheet and the workbook
cat < WorksheetEnd.xml >>${report}.xml
if [ $? != 0 ]
then
	echo "Error wrapping up worksheet, terminating" >&2
	exit 1
fi
cat < postamble.xml >>${report}.xml
if [ $? != 0 ]
then
	echo "Error wrapping up report.xml file, terminating" >&2
	exit 1
fi

# Zip the report file
zip report.zip ${report}.xml
if [ $? != 0 ]
then
	echo "Error zipping ${report}.xml file, terminating" >&2
	exit 1
fi


# Mail out the report.  Must be a binary attachment as UTF-8 uses the high bit
blat -to ${emailDestination} -subject "ALM Usage Statistics - ${report}" -body "See Attached" -uuencode -attach report.zip
if [ $? != 0 ]
then
	echo "Error sending report file via Blat, terminating" >&2
	exit 1
fi

# Clean up the file
rm -f ${report}.xml
rm -f report.zip

exit 0
