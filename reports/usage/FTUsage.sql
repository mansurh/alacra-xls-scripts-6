SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
GO

set transaction isolation level read uncommitted
set nocount on


declare @report_feed_id varchar(255)
select @report_feed_id = '2562'

declare @datestr varchar(6)
select @datestr = convert(varchar(6),dateadd(day,-10,getdate()),112)

declare @datestr2 varchar(255)
select @datestr2 = SUBSTRING(@datestr, 5, 2) + '/' + SUBSTRING(@datestr, 1, 4)

PRINT 'Alacra Pulse FT.com Usage'
PRINT 'Report for: ' + @datestr2
PRINT ''
PRINT ''

PRINT 'Part 1: Daily number of headlines viewed'
PRINT ''

declare @logtablename varchar(255)
declare @feedtablename varchar(255)

select @logtablename = 'pulseimpressionlog_archive_' + @datestr
select @feedtablename = 'pulseimpressionfeeds_archive_' + @datestr

declare @sqlstatement varchar(8000)
select @sqlstatement = 'select convert(varchar(8), l.eventtime, 112) "date", count(distinct l.primaryid) "pages", sum(f.event_count) "stories" ' +
			'from ' + @logtablename + ' l, ' + @feedtablename + ' f ' +
			'where l.primaryid = f.impression_id ' +
			'and l.host_server = f.host_server ' +
			'and f.feed_id = ' + @report_feed_id +
			'group by convert(varchar(8), l.eventtime, 112) ' +
			'order by convert(varchar(8), l.eventtime, 112) desc'

exec (@sqlstatement)

PRINT ''
PRINT ''

PRINT 'Part 2: Number of unique visitors during ' + @datestr2
PRINT ''

select @sqlstatement = 'select count(distinct l.userid) "Pay Sites" ' +
			'from ' + @logtablename + ' l, ' + @feedtablename + ' f ' +
			'where l.primaryid = f.impression_id ' +
			'and l.host_server = f.host_server ' +
			'and f.feed_id = ' + @report_feed_id +
			'and l.accountid != 6752 ' 


exec (@sqlstatement)

select @sqlstatement = 'select count(distinct l.cookie) "Free Sites" ' +
			'from ' + @logtablename + ' l, ' + @feedtablename + ' f ' +
			'where l.primaryid = f.impression_id ' +
			'and l.host_server = f.host_server ' +
			'and f.feed_id = ' + @report_feed_id +
			'and l.accountid = 6752 ' 


exec (@sqlstatement)


PRINT ''
PRINT ''

PRINT 'Part 3: Daily number of clicks on headlines back to FT.com site'
PRINT ''

select @logtablename = 'pbclickthroughlog_archive_' + @datestr

select @sqlstatement = 'select convert(varchar(8), l.eventtime, 112) "date", count(*) "clicks" ' +
			'from ' + @logtablename + ' l ' +
			'where l.feed_id = ' + @report_feed_id +
			'group by convert(varchar(8), l.eventtime, 112) ' +
			'order by convert(varchar(8), l.eventtime, 112) desc'

exec (@sqlstatement)

