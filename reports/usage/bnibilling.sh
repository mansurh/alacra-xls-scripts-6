# Parse the command line arguments.
#	1 = server
#	2 = user
#	3 = password
if [ $# != 3 ]
then
	echo "Usage: bnibilling.sh server user password"
	exit 1
fi

# Save the runstring parameters in local variables
logon=$2
password=$3
dataserver=$1

# Run isql to generate the report
rm -f bnibilling.tmp xlsbniusage.dat
isql -U${logon} -P${password} -S${dataserver} -n -h-1 -w 80 -s" "  < bnibilling.sql -o bnibilling.tmp
if [ $? != 0 ]
then
	echo "Error in isql report generation, exiting"
	exit 1
fi

# Get rid of the trailing blank line
sed -e "/^$/d" < bnibilling.tmp >xlsbniusage`date +%m%d`.dat
if [ $? != 0 ]
then
	echo "Error in sed removal of last line"
	exit 1
fi
rm -f bnibilling.tmp

# Copy the file to the RDS Ftp server
echo "user xls penguin" > bnibilling.ftp
echo "cd Accounting/XLS" >> bnibilling.ftp
echo "put xlsbniusage`date +%m%d`.dat" >> bnibilling.ftp
ftp -i -n -s:bnibilling.ftp ftp.rdsinc.com
if [ $? != 0 ]
then
	echo "Error in FTP delivery of usage file"
	exit 1
fi

# Mail the usage report to the xls administrators
blat xlsbniusage`date +%m%d`.dat -t administrators@xls.com -s "Monthly BNI Billing Report"

echo "Complete"
exit 0
