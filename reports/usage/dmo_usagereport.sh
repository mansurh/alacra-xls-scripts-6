XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments.
#	1 = server
#	2 = user
#	3 = password
#if [ $# != 3 ]
#then
#	echo "Usage: usagereport.sh server user password"
#	exit 1
#fi

# Save the runstring parameters in local variables
#logon=$2
#password=$3
#dataserver=$1

logon=`get_xls_registry_value concordance User`
password=`get_xls_registry_value concordance Password`
dataserver=`get_xls_registry_value concordance Server`

isql /b /U${logon} /P${password} /S${dataserver} /n /w 500 < dmo_usagereport.sql | blat - -t "concordance_int@alacra.com" -s "DMO Daily Usage Report"
#isql /U${logon} /P${password} /S${dataserver} /n /w 500 < dmo_usagereport.sql | blat - -t tun.he@alacra.com -s "DMO Daily Usage Report"
