<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet exclude-result-prefixes="a" version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:a="http://www.alacra.com/"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt">

    <xsl:output method="html" doctype-system="about:legacy-compat" encoding="UTF-8" />
    
    <!-- Using // rather than / because the jobstream script calls
         QUERYXLS directly for the xml which does not return a datapack node -->
    <xsl:variable name="error" select="//a:DOCUMENT/a:RESULTSET/a:ERRORS/a:ERROR/a:TEXT" />
    <xsl:variable name="matches" select="//a:DOCUMENT/a:RESULTSET/a:METADATA/a:TOTALMATCHES" />

    <xsl:variable name="prices">
        <xsl:for-each select="//a:DOCUMENT/a:RESULTSET[@a:NAME='resultset_1']/a:RESULTS/a:dbresults/a:total_price">
            <item><xsl:value-of select="number(translate(./text(), ',', ''))"/></item>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:template match="/">
        <!-- <!DOCTYPE html>abc -->
        <html lang="en">
            <head>
                <meta charset="utf-8"/>
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <!-- <link rel="stylesheet" href="/pubsite/OnDemand/css/ondemand.css" type="text/css"/> -->
                <xsl:call-template name="inline-css"/>
                <title>Alacra On Demand Invoice - Invoice Billing</title>
            </head>
            <body id="invoice-billing">
                <xsl:choose>
                    <xsl:when test="$error">
                        <xsl:text>There was an error.</xsl:text>
                    </xsl:when>
                    <xsl:when test="not(boolean($matches))">
                        <xsl:text>There were no results.</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="invoice"/>
                    </xsl:otherwise>
                </xsl:choose>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="invoice">
        <div id="logo">
            <!-- converttopdf for some reason needs this to be http -->
            <xsl:variable name="logoprotocol">
                <xsl:choose>
                    <!-- no datapack means this is for pdf emails -->
                    <xsl:when test="boolean(/a:DATAPACK)"><xsl:value-of select="'https'"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="'http'"/></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <img id="image" src="{$logoprotocol}://www.alacra.com/pubsite/OnDemand/images/logo.png" 
                 alt="Company, Credit, Deal, Investmenet &amp; Market Research"/>
        </div>
        <hr />

        <div id="container">
            <h1>Invoice</h1>
            <div id="address">
                <div>
                    Alacra Inc. <br/>
                    48 Wall Street, 6th Floor <br/>
                    New York, NY 10005 <br/>
                    http://www.alacra.com
                </div>
                <div id="bill-to">
                    <xsl:variable name="resultset3" select="//a:DOCUMENT/a:RESULTSET[@a:NAME='resultset_3']/a:RESULTS/a:dbresults" />
                    <xsl:value-of select="$resultset3/a:name"/>
                    <br/>
                    <xsl:value-of select="$resultset3/a:billing_address1"/>
                    <xsl:if test="string($resultset3/a:billing_address2)">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="$resultset3/a:billing_address2"/>
                    </xsl:if>
                    <xsl:if test="string($resultset3/a:billing_address3)">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="$resultset3/a:billing_address3"/>
                    </xsl:if>
                    <br/>
                    <xsl:value-of select="$resultset3/a:billing_city"/>
                    <xsl:if test="string-length($resultset3/a:billing_state) > 0">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="$resultset3/a:billing_state"/>
                    </xsl:if>
                    <xsl:if test="string($resultset3/a:billing_postal_code)">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="$resultset3/a:billing_postal_code"/>
                    </xsl:if>
                </div>
            </div>
            <div id="customer">
                <table id="meta">
                    <tbody>
                        <tr>
                            <td class="meta-head">Invoice #</td>
                            <td><xsl:value-of select="//a:DOCUMENT/a:RESULTSET/a:RESULTS/a:dbresults/a:invoiceNumber"/></td>
                        </tr>
                        <tr>
                            <td class="meta-head">Date</td>
                            <td><xsl:value-of select="//a:DOCUMENT/a:RESULTSET/a:RESULTS/a:dbresults/a:invoiceDate"/></td>
                        </tr>
                        <tr>
                            <td class="meta-head">Account</td>
                            <td><xsl:value-of select="//a:DOCUMENT/a:RESULTSET/a:RESULTS/a:dbresults/a:account"/></td>
                        </tr>
                        <tr>
                            <td class="meta-head">Terms</td>
                            <td>Net 30</td>
                        </tr>
                        <tr>
                            <td class="meta-head">Payment Due</td>
                            <td><xsl:value-of select="//a:DOCUMENT/a:RESULTSET/a:RESULTS/a:dbresults/a:dueDate"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="clear"><xsl:text></xsl:text></div>

        <table id="items">
            <tbody>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Project Code</th>
                    <th>Date</th>
                    <th>Price</th>
                </tr>
                <xsl:for-each select="//a:DOCUMENT/a:RESULTSET[@a:NAME='resultset_1']/a:RESULTS/a:dbresults">
                    <tr class="item-row">
                        <td class="name"><xsl:value-of select="a:name"/></td>
                        <td class="description"><xsl:value-of select="a:description"/></td>
                        <td><xsl:value-of select="a:project"/></td>
                        <td><xsl:value-of select="a:access_time"/></td>
                        <td>$<xsl:value-of select="a:total_price"/></td>
                    </tr>
                </xsl:for-each>
                <tr>
                    <td colspan="3" class="blank"> </td>
                    <td colspan="1" class="total-line balance">Total</td>
                    <td class="total-value balance"><div id="subtotal">
                        <xsl:value-of select="format-number(sum(msxsl:node-set($prices)/item), '$#,###,###.00')"/>
                    </div></td>
                </tr>
            </tbody>
        </table>

        <hr class="section"/>

        <div id="directions">
            <p>
                <span class="info">Please send checks to:</span><br/>
                Alacra Inc. <br/>
                PO Box 200214 <br/>
                Pittsburgh, PA 15251-0214 <br/>
                <br/>
                <span class="info">For wire transfer payments:</span> <br/>
                To: SIL VLY BK SJ <br/>
                Routing and transit no.: 121140399 <br/>
                For credit of: Alacra, Inc. Cash Collateral account <br/>
                Credit account no.: 3300300302
            </p>
            <p>
                <span class="info">Please make all checks payable to:</span> <br/>
                Alacra, Inc., Federal Tax ID: 22-3443095
            </p>
            <p>
                <span class="info">For questions concerning this invoice, please contact:</span> <br/>
                Nadia Besman at nadia.besman@alacra.com
            </p>
        </div>
    </xsl:template>

    <xsl:template name="inline-css">
        <style>
            div#logo {
            margin-top: 15px;
            margin-bottom: 15px;
            width: auto;
            }
            #invoice-billing {
            font: 14px/1.4 Arial, Helvetica Neue, Helvetica, sans-serif;
            width: 800px;
            margin: 0px auto;
            }
            #invoice-billing .grey-bg {
            background: #EEE none repeat scroll 0% 0%;
            }
            #invoice-billing .info {
            color: #606060;
            font-style: oblique;
            }
            #invoice-billing #container {
            margin-top: 20px;
            }
            #invoice-billing table {
            border-collapse: collapse;
            }
            #invoice-billing table td,
            #invoice-billing table th {
            border: 1px solid #000;
            padding: 5px;
            }
            #invoice-billing #header {
            height: 15px;
            width: 100%;
            margin: 20px 0px;
            background: #222 none repeat scroll 0% 0%;
            text-align: center;
            color: #FFF;
            font: bold 15px Helvetica, sans-serif;
            letter-spacing: 20px;
            padding: 8px 0px;
            }
            #invoice-billing #address {
            width: 250px;
            float: left;
            }
            #invoice-billing #customer {
            float: right;
            }
            #invoice-billing #directions {
            margin-top: 25px;
            margin-bottom: 65px;
            }
            #invoice-billing #logo {
            position: relative;
            margin-top: 25px;
            border: 1px solid #FFF;
            max-width: 540px;
            max-height: 100px;
            overflow: hidden;
            }
            #invoice-billing #logo #image {
            text-align: right;
            }
            #invoice-billing #bill-to {
            float: left;
            margin-top: 30px;
            margin-bottom: 15px;
            }
            #invoice-billing #meta {
            text-align: right;
            float: right;
            margin-top: 1px;
            width: 300px;
            }
            #invoice-billing #meta td {
            text-align: right;
            }
            #invoice-billing #meta td.meta-head {
            text-align: left;
            background: #EEE none repeat scroll 0% 0%;
            }
            #invoice-billing #items {
            clear: both;
            width: 100%;
            margin: 30px 0px 0px;
            border: 1px solid #000;
            }
            #invoice-billing #items th {
            background: #EEE none repeat scroll 0% 0%;
            }
            #invoice-billing #items tr.item-row td {
            border: 0px none;
            vertical-align: top;
            }
            #invoice-billing #items td {
            width: 80px;
            }
            #invoice-billing #items td.balance {
            background: #EEE none repeat scroll 0% 0%;
            }
            #invoice-billing #items td.name {
            width: 175px;
            }
            #invoice-billing #items td.blank {
            border: 0px none;
            }
            #invoice-billing #items td.description {
            width: 280px;
            }
            #invoice-billing #items td.total-line {
            border-right: 0px none;
            text-align: right;
            }
            #invoice-billing #items td.total-value {
            border-left: 0px none;
            padding: 10px;
            }
        </style>
    </xsl:template>
</xsl:stylesheet>
