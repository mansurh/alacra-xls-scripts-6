set nocount on
set transaction isolation level read uncommitted

declare @c1 int
declare @c2 int

select @c1 = COUNT(*) FROM ipid_cici
select @c2 = COUNT(*) FROM ipid_cici_bak


SELECT 1 as [Number], 'Date:      ', CONVERT(varchar, GETDATE(), 106)
union
SELECT 2 as [Number], 'Current:   ', STR(@c1, 8, 0)
union
SELECT 3 as [Number], 'Previous:  ', STR(@c2, 8, 0)
union
SELECT 4 as [Number], 'Difference:', STR(@c1 - @c2, 8, 0)
ORDER BY 1
