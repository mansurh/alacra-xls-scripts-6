set nocount on
set transaction isolation level read uncommitted

select RecordState, COUNT(*) as [Count]
FROM
	ipid_cici
GROUP BY RecordState
ORDER BY COUNT(*) DESC
