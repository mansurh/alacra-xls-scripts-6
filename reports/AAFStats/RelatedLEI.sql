set nocount on
set transaction isolation level read uncommitted

select CASE WHEN COALESCE(RelatedLEI,'')='' THEN 'Blank' ELSE 'Non-blank' END, COUNT(*) as [Count]
FROM
	ipid_cici
GROUP BY CASE WHEN COALESCE(RelatedLEI,'')='' THEN 'Blank' ELSE 'Non-blank' END
ORDER BY COUNT(*) desc
