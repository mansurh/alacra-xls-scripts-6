 XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 4 ]
then
	echo "Usage: PulseDailyEmailAFriend.sh alacrastoreserver daysback accountid emailDest msgTrailer"
	exit 1
fi

shname=$0
xserver=$1
daysback=$2
accountid=$3
emailDest=$4
msgTrailer=""
if [ $# -gt 4 ]
then
	msgTrailer=$5
fi

dbname="alacralog"
#xserver=`get_xls_registry_value ${dbname} "Server" "0"`
xuser=`get_xls_registry_value ${dbname} "User" "0"`
xpassword=`get_xls_registry_value ${dbname} "Password" "0"`

tempsuffix=_temp
tempdatafile=temp.dat

reportxmlfile=pulseEmails_`date +%y%m%d`.xml

# --------------------------------



# SCRIPT BEGINNING


run() {


	# get start datetime string
	mysql="set nocount on select convert(varchar(32),dateadd(day,-${daysback},getdate()),101) + ' 00:00:00.0'"
	isql /Q "${mysql}" /S${xserver} /U${xuser} /P${xpassword} -b -n -h-1 -w4096 -s"|" | sed -e "s/\n//g" | grep -v "^$" > date.tmp
	read startdate < date.tmp
	rm date.tmp
	echo "Start date = [$startdate] ..."

	# get end datetime string
	mysql="set nocount on select convert(varchar(32),getdate(),101) + ' 00:00:00.0'"
	isql /Q "${mysql}" /S${xserver} /U${xuser} /P${xpassword} -b -n -h-1 -w4096 -s"|" | sed -e "s/\n//g" | grep -v "^$" > date.tmp
	read enddate < date.tmp
	rm date.tmp
	echo "End date = [$enddate] ..."



	# Run the query for the spreadsheet
	Sheet=pulse_emails

	fullp=$XLSDATA/pulse/${Sheet}.sql

	echo "\
set nocount on \
set transaction isolation level read uncommitted \
set ansi_warnings off \
\
select senderip, eventtime, senderemail, recipientemail, subject, account, email_optout \
from landingpage_emails \
where account IN ( ${accountid} ) \
and eventtime >= '${startdate}' \
and eventtime <  '${enddate}' \
order by eventtime desc" > ${fullp}


	# convert all forward slashes to backslashes
	fwslashpath1=$(sed -e"s/\//\\\\/g" << HERE
${fullp}
HERE
)

	echo "sqlcmd -S ${xserver} -U ${xuser} -P ${xpassword} -i ${fwslashpath1} -o ${Sheet}.rpt -W -s""|"" -h -1"
	sqlcmd -S ${xserver} -U ${xuser} -P ${xpassword} -i ${fwslashpath1} -o ${Sheet}.rpt -W -s"|" -h -1
	if [ $? != 0 ]
	then
		echo "Error running SQL Query, terminating"
		return 1
	fi 

	# convert it to xml
	sed -f $XLS/src/scripts/reports/pulse/${Sheet}.sed < ${Sheet}.rpt > ${Sheet}.xml

	# Must be in the UTF-8 Character Set
	iconv -f ISO-8859-1 -t UTF-8 < ${Sheet}.xml > ${Sheet}_utf.xml
	if [ $? != 0 ]
	then
		echo "Error converting output to UTF-8, terminating"
		return 1
	fi 

	# Start the report output file
	cat< $XLS/src/scripts/reports/Weblogdb/preamble.xml >${reportxmlfile}
	if [ $? != 0 ]
	then
		echo "Error creating ${reportxmlfile} file, terminating"
		return 1
	fi

	# Append together the Excel bits
	for bit in $XLS/src/scripts/reports/pulse/${Sheet}Start.xml ${Sheet}_utf.xml $XLS/src/scripts/reports/Weblogdb/WorksheetEnd.xml
	do
		cat <${bit}  >> ${reportxmlfile}
		if [ $? != 0 ]
		then
			echo "Error concatenating ${bit} to report file, terminating"
			return 1
		fi 
	done

	# Clean Up
	rm -f ${Sheet}.rpt
	rm -f ${Sheet}.xml
	rm -f ${Sheet}_utf.xml

	# End the report output file
	cat < $XLS/src/scripts/reports/Weblogdb/postamble.xml >>${reportxmlfile}
	if [ $? != 0 ]
	then
		echo "Error wrapping up ${reportxmlfile} file, terminating"
		return 1
	fi
}


out_dir=$XLSDATA/pulse
logfile=${out_dir}/PulseDailyEmailAFriend_`date +%y%m%d`.log

if [ ! -d ${out_dir} ]
then
	mkdir -p ${out_dir}
fi


run > ${logfile}
if [ $? -ne 0 ]
then
	blat ${logfile} -t administrators@alacra.com -s "Pulse Email-A-Friend Report Script failed${msgTrailer}"
	exit 1
else
#	desc="Pulse Email-A-Friend: ${startdate%% 00:00:00.0} ET - ${enddate%% 00:00:00.0} ET${msgTrailer}"
	desc="Pulse Email-A-Friend: ${startdate} ET - ${enddate} ET${msgTrailer}"

	# Mail out the report.  Must be a binary attachment as UTF-8 uses the high bit
	blat -to ${emailDest} -subject "${desc}" -body "${desc}.  See Attached." -uuencode -attach ${reportxmlfile}
	if [ $? != 0 ]
	then
		echo "Error sending report file via Blat, terminating"
		exit 1
	fi
fi

exit 0

