# Parse the command line arguments
#  1 = controldb server
#  2 = controldb user
#  3 = controldb passwd

if [ $# -lt 3 ]
then
    echo "Usage: SpiderErr.sh server login passwd"
    exit 1
fi

server=$1
user=$2
passwd=$3

TEMPDIR=${XLSDATA}/spider

rm -f ${TEMPDIR}/jakeErr.log
rm -f ${TEMPDIR}/spiderErr.log

perl ${XLS}/src/scripts/reports/spider/spider-err.pl ${server} ${user} ${passwd} 7 > ${TEMPDIR}/jakeErr.log
perl ${XLS}/src/scripts/reports/spider/spider-err.pl ${server} ${user} ${passwd} 8 > ${TEMPDIR}/spiderErr.log
perl ${XLS}/src/scripts/reports/spider/spider-err.pl ${server} ${user} ${passwd} 9 > ${TEMPDIR}/spiderErr.log
perl ${XLS}/src/scripts/reports/spider/spider-err.pl ${server} ${user} ${passwd} 10 >> ${TEMPDIR}/spiderErr.log
perl ${XLS}/src/scripts/reports/spider/spider-err.pl ${server} ${user} ${passwd} 11 >> ${TEMPDIR}/spiderErr.log
perl ${XLS}/src/scripts/reports/spider/spider-err.pl ${server} ${user} ${passwd} 12 >> ${TEMPDIR}/spiderErr.log
perl ${XLS}/src/scripts/reports/spider/spider-err.pl ${server} ${user} ${passwd} 13 >> ${TEMPDIR}/spiderErr.log

# Mail the #7 errs to Jake, save the rest for Taro to report.
blat ${TEMPDIR}/jakeErr.log -t "jharris@xls.com" -s "SPIDER Misc. Errors"

