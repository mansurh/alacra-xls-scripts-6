set nocount on
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
GO
PRINT "Alacra Store Daily Usage Report"
PRINT ""
PRINT ""

set transaction isolation level read uncommitted
set nocount on


-- date to run report for
declare @rundateIN datetime
declare @rundate datetime
select @rundateIN = dateadd(day,-1,getdate())
-- force to be midnight
select @rundate = convert(varchar(32),@rundateIN,101) + ' 00:00:00.0'


-- date format
declare @datefmt int
select @datefmt = 107

CREATE TABLE #storestatsdetail (
	datepartyear int null,
	datepartname varchar(32) null,
	datepartint int null,
	dow int null,
	datestr varchar(32) null,
	totalcount int null,
	totalamt money null,
	freeflag tinyint null,
	ip int null
)

CREATE TABLE #storestats (
	datepartyear int null,
	datepartname varchar(32) null,
	datepartint int null,
	dow int null,
	datestr varchar(32) null,
	totalcount int null,
	totalamt money null,
	freeflag tinyint null
)

CREATE TABLE #storeipstats (
	ipname varchar(64) null,
	purchases int null,
	avgweekly_purchases int null,
	freepurchases int null,
	avgweekly_freepurchases int null,
	revenue money null,
	avgweekly_revenue money null,
	delta money null
)


-- COMPUTE RELEVANT DATES

-- How many months do we go back to compute running averages?
declare @ave_start_date datetime
select @ave_start_date = DATEADD(month, -3, @rundate)
-- normalize to start on a Sunday so the first week isn't unusually small
declare @ave_start_date_sun datetime
select @ave_start_date_sun = dateadd(day, -1 * (datepart(dw,@ave_start_date) - 1), @ave_start_date)
-- cutoff day to exclude current week
declare @prev_sat datetime
select @prev_sat = dateadd(day, -1 * datepart(dw,@rundate), @rundate)
-- get current values
declare @cur_year int
declare @cur_wk int
declare @cur_dy int
select @cur_year=year(@rundate), @cur_wk=datepart(wk, @rundate), @cur_dy=datepart(dy, @rundate)

-- INSERT SUMMARIZED DATA TO TEMP TABLES

-- non-free weekly
insert #storestatsdetail
select year(oi.creationdate), 'week', datepart(wk, oi.creationdate), null, 
	convert(varchar(32), dateadd(week, datepart(wk, oi.creationdate)-1, '01/01/' + STR(year(oi.creationdate), 4, 0)), @datefmt),
	count(*), sum(oi.actualprice), 0, oi.ip
from cc_orderitem oi 
  inner join cc_order o on (oi.orderid=o.orderid) 
  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
on o.orderid = ce.orderid
where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
and oi.creationdate >= @ave_start_date_sun
and oi.creationdate < dateadd(day,1,@rundate)
and oi.actualprice > 0.001
group by year(oi.creationdate), datepart(wk, oi.creationdate), oi.ip

-- free weekly
insert #storestatsdetail
select year(oi.creationdate), 'week', datepart(wk, oi.creationdate), null, 
	convert(varchar(32), dateadd(week, datepart(wk, oi.creationdate)-1, '01/01/' + STR(year(oi.creationdate), 4, 0)), @datefmt),
	count(*), sum(oi.actualprice), 1, oi.ip
from cc_orderitem oi 
  inner join cc_order o on (oi.orderid=o.orderid) 
  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
on o.orderid = ce.orderid
where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
and oi.creationdate >= @ave_start_date_sun
and oi.creationdate < dateadd(day,1,@rundate)
and oi.actualprice <= 0.001
group by year(oi.creationdate), datepart(wk, oi.creationdate), oi.ip

-- non-free daily
insert #storestatsdetail
select year(oi.creationdate), 'day', datepart(dy, oi.creationdate), datepart(dw, oi.creationdate),
	convert(varchar(32), dateadd(day, datepart(dy, oi.creationdate)-1, '01/01/' + STR(year(oi.creationdate), 4, 0)), @datefmt),
	count(*), sum(oi.actualprice), 0, oi.ip
from cc_orderitem oi 
  inner join cc_order o on (oi.orderid=o.orderid) 
  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
on o.orderid = ce.orderid
where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
and oi.creationdate >= @ave_start_date
and oi.creationdate < dateadd(day,1,@rundate)
and oi.actualprice > 0.001
group by year(oi.creationdate), datepart(dy, oi.creationdate), datepart(dw, oi.creationdate), oi.ip

-- free daily
insert #storestatsdetail
select year(oi.creationdate), 'day', datepart(dy, oi.creationdate), datepart(dw, oi.creationdate),
	convert(varchar(32), dateadd(day, datepart(dy, oi.creationdate)-1, '01/01/' + STR(year(oi.creationdate), 4, 0)), @datefmt),
	count(*), sum(oi.actualprice), 1, oi.ip
from cc_orderitem oi 
  inner join cc_order o on (oi.orderid=o.orderid) 
  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
on o.orderid = ce.orderid
where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
and oi.creationdate >= @ave_start_date
and oi.creationdate < dateadd(day,1,@rundate)
and oi.actualprice <= 0.001
group by year(oi.creationdate), datepart(dy, oi.creationdate), datepart(dw, oi.creationdate), oi.ip

insert #storestats
select datepartyear, datepartname, datepartint, dow, datestr, sum(totalcount), sum(totalamt), freeflag
from #storestatsdetail
group by datepartyear, datepartname, datepartint, dow, datestr, freeflag

-- @@@@ TEST
-- select * from #storestats
-- order by freeflag, datepartyear, datepartint
-- @@@@

PRINT 'Store Transactions - ' + CONVERT(varchar(32), @rundate, @datefmt)

declare @cur_cnt int
declare @cur_amt money
declare @cur_free_cnt int

select @cur_cnt = 0
select @cur_amt = 0.0
select @cur_free_cnt = 0

select @cur_cnt = coalesce(totalcount,0), @cur_amt = coalesce(totalamt,0.0)
from #storestats
where datepartname='day'
and (datepartyear = @cur_year and datepartint = @cur_dy) and freeflag = 0

select @cur_free_cnt = coalesce(totalcount,0)
from #storestats
where datepartname='day'
and (datepartyear = @cur_year and datepartint = @cur_dy) and freeflag = 1


declare @running_day_avg_amt money
declare @running_day_avg_cnt int
declare @running_day_avg_free_cnt int
declare @ezname varchar(32)

select @running_day_avg_amt = 0.0
select @running_day_avg_cnt = 0
select @running_day_avg_free_cnt = 0

if datepart(dw, @rundate) = 1 or datepart(dw, @rundate) = 7
begin
	select @ezname = 'weekend', @running_day_avg_amt = coalesce(avg(totalamt),0), @running_day_avg_cnt = coalesce(avg(totalcount),0)
	from #storestats
	where datepartname='day'
	and (datepartyear != @cur_year or datepartint != @cur_dy)
	and dow in (1, 7)
	and freeflag = 0

	select @running_day_avg_free_cnt = coalesce(avg(totalcount),0)
	from #storestats
	where datepartname='day'
	and (datepartyear != @cur_year or datepartint != @cur_dy)
	and dow in (1, 7)
	and freeflag = 1
end
else
begin
	select @ezname = 'weekday', @running_day_avg_amt = coalesce(avg(totalamt),0), @running_day_avg_cnt = coalesce(avg(totalcount),0)
	from #storestats
	where datepartname='day'
	and (datepartyear != @cur_year or datepartint != @cur_dy)
	and dow != 1 and dow != 7
	and freeflag = 0

	select @running_day_avg_free_cnt = coalesce(avg(totalcount),0)
	from #storestats
	where datepartname='day'
	and (datepartyear != @cur_year or datepartint != @cur_dy)
	and dow != 1 and dow != 7
	and freeflag = 1
end


PRINT LTRIM(STR(coalesce(@cur_cnt,0), 10, 0)) + ' purchases (' + @ezname + ' average: ' + LTRIM(STR(coalesce(@running_day_avg_cnt,0), 10, 0)) + ')'
PRINT '$' + LTRIM(STR(coalesce(@cur_amt,0.0), 15, 2)) + ' gross sales (' + @ezname + ' average: $' + LTRIM(STR(coalesce(@running_day_avg_amt,0.0), 15, 2)) + ')'
PRINT LTRIM(STR(coalesce(@cur_free_cnt,0), 10, 0)) + ' free downloads (' + @ezname + ' average: ' + LTRIM(STR(coalesce(@running_day_avg_free_cnt,0), 10, 0)) + ')'

select @cur_cnt = 0
select @cur_amt = 0.0
select @cur_free_cnt = 0
select @running_day_avg_amt = 0.0
select @running_day_avg_cnt = 0
select @running_day_avg_free_cnt = 0


PRINT ' '
PRINT 'Store Transactions - Past 7 Days'

declare @running_weekly_avg_amt money
declare @running_weekly_avg_cnt int
declare @running_weekly_avg_free_cnt int

select @running_weekly_avg_amt = 0.0
select @running_weekly_avg_cnt = 0
select @running_weekly_avg_free_cnt = 0

select @running_weekly_avg_amt = coalesce(avg(totalamt),0), @running_weekly_avg_cnt = coalesce(avg(totalcount),0)
from #storestats
where datepartname='week' and (datepartyear != @cur_year or datepartint != @cur_wk)
and freeflag = 0

select @running_weekly_avg_free_cnt = coalesce(avg(totalcount),0)
from #storestats
where datepartname='week' and (datepartyear != @cur_year or datepartint != @cur_wk)
and freeflag = 1


select @cur_cnt=count(*), @cur_amt=sum(coalesce(oi.actualprice,0.0))
from cc_orderitem oi 
  inner join cc_order o on (oi.orderid=o.orderid) 
  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
on o.orderid = ce.orderid
where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
and oi.creationdate >= dateadd(day,-6,@rundateIN)
and oi.creationdate < dateadd(day,1,@rundate)
and oi.actualprice > 0.001

select @cur_free_cnt=count(*)
from cc_orderitem oi 
  inner join cc_order o on (oi.orderid=o.orderid) 
  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
on o.orderid = ce.orderid
where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
and oi.creationdate >= dateadd(day,-6,@rundateIN)
and oi.creationdate < dateadd(day,1,@rundate)
and oi.actualprice <= 0.001


PRINT LTRIM(STR(coalesce(@cur_cnt,0), 10, 0)) + ' purchases (weekly average: ' + LTRIM(STR(coalesce(@running_weekly_avg_cnt,0), 10, 0)) + ')'
PRINT '$' + LTRIM(STR(coalesce(@cur_amt,0.0), 15, 2)) + ' gross sales (weekly average: $' + LTRIM(STR(coalesce(@running_weekly_avg_amt,0.0), 15, 2)) + ')'
PRINT LTRIM(STR(coalesce(@cur_free_cnt,0), 10, 0)) + ' free downloads (weekly average: ' + LTRIM(STR(coalesce(@running_weekly_avg_free_cnt,0), 10, 0)) + ')'

select @cur_cnt = 0
select @cur_amt = 0.0
select @cur_free_cnt = 0
select @running_weekly_avg_amt = 0.0
select @running_weekly_avg_cnt = 0
select @running_weekly_avg_free_cnt = 0



truncate table #storestats


-- daily registrations
insert #storestats
select year(u.commence), 'day', datepart(dy, u.commence), datepart(dw, u.commence),
	convert(varchar(32), dateadd(day, datepart(dy, u.commence)-1, '01/01/' + STR(year(u.commence), 4, 0)), @datefmt),
	count(*), null, CASE WHEN u.commence > u.terminate THEN 1 ELSE 0 END
from users u
left join userregistrations r on u.id = r.userid
where u.service = 57
and u.commence >= @ave_start_date
and u.commence < dateadd(day,1,@rundate)
group by year(u.commence), datepart(dy, u.commence), datepart(dw, u.commence), CASE WHEN u.commence > u.terminate THEN 1 ELSE 0 END


-- weekly registrations
insert #storestats
select year(u.commence), 'week', datepart(wk, u.commence), null, 
	convert(varchar(32), dateadd(week, datepart(wk, u.commence)-1, '01/01/' + STR(year(u.commence), 4, 0)), @datefmt),
	count(*), null, CASE WHEN u.commence > u.terminate THEN 1 ELSE 0 END
from users u
left join userregistrations r on u.id = r.userid
where u.service = 57
and u.commence >= @ave_start_date_sun
and u.commence < dateadd(day,1,@rundate)
group by year(u.commence), datepart(wk, u.commence), CASE WHEN u.commence > u.terminate THEN 1 ELSE 0 END


-- @@@@ TEST
-- select * from #storestats
-- order by freeflag, datepartyear, datepartint
-- @@@@


PRINT ' '
PRINT 'Store Registrations - ' + CONVERT(varchar(32), @rundate, @datefmt)

select @cur_cnt = coalesce(totalcount,0), @cur_amt = coalesce(totalamt,0.0)
from #storestats
where datepartname='day'
and (datepartyear = @cur_year and datepartint = @cur_dy) and freeflag = 0

select @cur_free_cnt = coalesce(totalcount,0)
from #storestats
where datepartname='day'
and (datepartyear = @cur_year and datepartint = @cur_dy) and freeflag = 1

if datepart(dw, @rundate) = 1 or datepart(dw, @rundate) = 7
begin
	select @ezname = 'weekend', @running_day_avg_cnt = coalesce(avg(totalcount),0)
	from #storestats
	where datepartname='day'
	and (datepartyear != @cur_year or datepartint != @cur_dy)
	and dow in (1, 7)
	and freeflag = 0

	select @running_day_avg_free_cnt = coalesce(avg(totalcount),0)
	from #storestats
	where datepartname='day'
	and (datepartyear != @cur_year or datepartint != @cur_dy)
	and dow in (1, 7)
	and freeflag = 1
end
else
begin
	select @ezname = 'weekday', @running_day_avg_cnt = coalesce(avg(totalcount),0)
	from #storestats
	where datepartname='day'
	and (datepartyear != @cur_year or datepartint != @cur_dy)
	and dow != 1 and dow != 7
	and freeflag = 0

	select @running_day_avg_free_cnt = coalesce(avg(totalcount),0)
	from #storestats
	where datepartname='day'
	and (datepartyear != @cur_year or datepartint != @cur_dy)
	and dow != 1 and dow != 7
	and freeflag = 1
end


PRINT LTRIM(STR(coalesce(@cur_cnt,0), 10, 0)) + ' Registrations completed (' + @ezname + ' average: ' + LTRIM(STR(coalesce(@running_day_avg_cnt,0), 10, 0)) + ')'
PRINT LTRIM(STR(coalesce(@cur_free_cnt,0), 10, 0)) + ' Registrations not completed (' + @ezname + ' average: ' + LTRIM(STR(coalesce(@running_day_avg_free_cnt,0), 10, 0)) + ')'

select @cur_cnt = 0
select @cur_amt = 0.0
select @cur_free_cnt = 0
select @running_day_avg_amt = 0.0
select @running_day_avg_cnt = 0
select @running_day_avg_free_cnt = 0


PRINT ' '
PRINT 'Store Registrations - Past 7 Days'

select @running_weekly_avg_cnt = coalesce(avg(totalcount),0)
from #storestats
where datepartname='week' and (datepartyear != @cur_year or datepartint != @cur_wk)
and freeflag = 0

select @running_weekly_avg_free_cnt = coalesce(avg(totalcount),0)
from #storestats
where datepartname='week' and (datepartyear != @cur_year or datepartint != @cur_wk)
and freeflag = 1


select @cur_cnt=count(*)
from users u
left join userregistrations r on u.id = r.userid
where u.service = 57
and u.commence >= dateadd(day,-6,@rundateIN)
and u.commence < dateadd(day,1,@rundate)
and u.commence <= u.terminate

select @cur_free_cnt=count(*)
from users u
left join userregistrations r on u.id = r.userid
where u.service = 57
and u.commence >= dateadd(day,-6,@rundateIN)
and u.commence < dateadd(day,1,@rundate)
and u.commence > u.terminate


PRINT LTRIM(STR(coalesce(@cur_cnt,0), 10, 0)) + ' Registrations completed (weekly average: ' + LTRIM(STR(coalesce(@running_weekly_avg_cnt,0), 10, 0)) + ')'
PRINT LTRIM(STR(coalesce(@cur_free_cnt,0), 10, 0)) + ' Registrations not completed (weekly average: ' + LTRIM(STR(coalesce(@running_weekly_avg_free_cnt,0), 10, 0)) + ')'

select @cur_cnt = 0
select @cur_amt = 0.0
select @cur_free_cnt = 0
select @running_weekly_avg_amt = 0.0
select @running_weekly_avg_cnt = 0
select @running_weekly_avg_free_cnt = 0


PRINT ' '
PRINT ' '
PRINT 'Past 7 days activity by publisher:'
PRINT ' '

declare @this_ip int
declare @this_ip_name varchar(64)

DECLARE my_ip_cursor CURSOR FAST_FORWARD FOR
 SELECT DISTINCT pt.ip, i.name
	from sw_xmldatapackage pk, sw_xmldatapart pt, ip i
	where pk.xmldatapart = pt.xmldatapart
	and pt.ip = i.id
	and pk.xmldatapackageid = 'storepremiumsearchv2'
	and pt.ip > 1
 ORDER BY i.name asc
OPEN my_ip_cursor
FETCH NEXT FROM my_ip_cursor INTO @this_ip, @this_ip_name
WHILE (@@FETCH_STATUS <> -1)
BEGIN
 IF (@@FETCH_STATUS <> -2)
 BEGIN
	select @cur_cnt = 0
	select @cur_amt = 0.0
	select @cur_free_cnt = 0
	select @running_weekly_avg_amt = 0.0
	select @running_weekly_avg_cnt = 0
	select @running_weekly_avg_free_cnt = 0

	select @cur_cnt=count(*), @cur_amt=coalesce(sum(oi.actualprice),0.0)
	from cc_orderitem oi 
	  inner join cc_order o on (oi.orderid=o.orderid) 
	  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
	on o.orderid = ce.orderid
	where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
	and oi.creationdate >= dateadd(day,-6,@rundateIN)
	and oi.creationdate < dateadd(day,1,@rundate)
	and oi.actualprice > 0.001
	and oi.ip = @this_ip

	select @cur_free_cnt=count(*)
	from cc_orderitem oi 
	  inner join cc_order o on (oi.orderid=o.orderid) 
	  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
	on o.orderid = ce.orderid
	where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
	and oi.creationdate >= dateadd(day,-6,@rundateIN)
	and oi.creationdate < dateadd(day,1,@rundate)
	and oi.actualprice <= 0.001
	and oi.ip = @this_ip

	select @running_weekly_avg_amt = coalesce(avg(totalamt),0), @running_weekly_avg_cnt = coalesce(avg(totalcount),0)
	from #storestatsdetail
	where datepartname='week' and (datepartyear != @cur_year or datepartint != @cur_wk)
	and freeflag = 0
	and ip = @this_ip

	select @running_weekly_avg_free_cnt = coalesce(avg(totalcount),0)
	from #storestatsdetail
	where datepartname='week' and (datepartyear != @cur_year or datepartint != @cur_wk)
	and freeflag = 1
	and ip = @this_ip

	INSERT #storeipstats
		SELECT @this_ip_name, @cur_cnt, @running_weekly_avg_cnt, @cur_free_cnt, @running_weekly_avg_free_cnt, @cur_amt, @running_weekly_avg_amt, @running_weekly_avg_amt - @cur_amt
 END
 FETCH NEXT FROM my_ip_cursor INTO @this_ip, @this_ip_name
END
CLOSE my_ip_cursor
DEALLOCATE my_ip_cursor

SELECT 
	convert(varchar(40),ipname) "Publisher", 
	convert(varchar(10),purchases) "Purchases",
	convert(varchar(10),avgweekly_purchases) "Weekly Avg",
	convert(varchar(10),freepurchases) "Free Downloads",
	convert(varchar(10),avgweekly_freepurchases) "Weekly Avg Free",
	convert(varchar(10),revenue) "Revenue",
	convert(varchar(10),avgweekly_revenue) "Weekly Avg Rev",
	convert(varchar(10),delta) "Change"
from #storeipstats
order by ipname


drop table #storestats
drop table #storestatsdetail
drop table #storeipstats


PRINT ' '
PRINT ' '
PRINT "Store Purchases in the past 24 Hours"
PRINT "================================================================="
select 
	convert(varchar(50),u.email) "User", 
	convert(varchar(40),i.name) "Content Provider", 
	convert(varchar(10),oi.actualprice) "Price",
	convert(varchar(22),oi.creationdate,100) "Time",
	case when convert(varchar(12),u.acceptance,101) = convert(varchar(12),oi.creationdate,101) THEN 'Y' ELSE 'N' END "First Order"
from cc_orderitem oi 
  inner join cc_order o on (oi.orderid=o.orderid) 
  inner join users u on (o.userid=u.id)
  inner join ip i on (oi.ip=i.id)
  left join (select orderid,time from cc_event where eventtypeid = 109) ce 
on o.orderid = ce.orderid
where buildstatus = 100 and o.statusid = 100 and coalesce(ce.orderid,'')<>''
and oi.creationdate >= DATEADD(hour,-24,GETDATE())
order by creationdate desc



PRINT ""
PRINT ""
PRINT "Store registrations in the past 24 Hours"
PRINT "================================================================="
select 
	convert(varchar(22),u.commence,100) "Time",
	convert(varchar(50),u.email) "User", 
	CASE WHEN u.commence > u.terminate THEN 'No' ELSE 'Yes' END "Completed Reg",
	CASE WHEN COALESCE(r.id,'') != '' THEN 'http://www.alacrastore.com/r/' + r.id ELSE '' END "Completion URL",
	convert(varchar(24),u.first_name) "First", 
	convert(varchar(44),u.last_name) "Last"
from users u
left join userregistrations r on u.id = r.userid
where u.service = 57
and u.commence >= DATEADD(hour,-24,GETDATE())
order by u.commence desc




PRINT ""
PRINT ""
PRINT "PCAN Clicks by affiliate in the past 24 Hours"
PRINT "================================================================="
select 
	convert(varchar(50),max(u.email)) "Affiliate", 
	convert(varchar(40),coalesce(a.website,'')) "Domain", 
	convert(varchar(10),count(*)) "Count",
	convert(varchar(22),max(clickTime),100) "Recent"
from afPageClick p, affiliate a, users u
where a.id = p.afid
and a.id = u.id
and clickTime >= DATEADD(hour,-24,GETDATE())
group by a.website

 

PRINT ""
PRINT ""
PRINT "PCAN Purchases by affiliate in the past 24 Hours"
PRINT "================================================================="
select 
	convert(varchar(50),u.email) "Buyer", 
	convert(varchar(40),i.name) "Content Provider", 
	convert(varchar(10),g.price) "Price",
	convert(varchar(22),g.access_time,100) "Time",
	convert(varchar(8),g.project2) "afid", 
	convert(varchar(40),a.email) "Affiliate"
from usage g, users u, users a, ip i
where u.id = g.userid
and g.access_time >= DATEADD(hour,-24,GETDATE())
and g.ip=i.id
and u.service = 57
and rtrim(coalesce(g.project2,'')) != ''
and g.project2=ltrim(str(a.id,10,0))
order by g.access_time desc

