# Parse the command line arguments.
#	1 = server
#	2 = user
#	3 = password
if [ $# != 3 ]
then
	echo "Usage: updatevelocity.sh server user password"
	exit 1
fi

# Save the runstring parameters in local variables
logon=$2
password=$3
dataserver=$1

#curday=`awk -f getaug.awk < a.txt`

isql /U${logon} /P${password} /S${dataserver} /n /w 500 < velocitycheck.sql > a.txt 

integer count=`awk -f getarg.awk < a.txt`

if [ ${count} != 0 ] 
then
	isql /U${logon} /P${password} /S${dataserver} /Q"select convert(char(15),ip)'ip',convert(char(50),i.name)'Database',convert(char(85),description)'description' from securityevent s join ip i on s.ip=i.id where isNew = 1 update securityevent set isNew = NULL where isNew = 1" /n /w 300 | blat - -t michael.angle@alacra.com,tun.he@alacra.com -s "Alacra Store Velocity Check - Test"
fi
