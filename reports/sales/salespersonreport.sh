# Parse the command line arguments.
#	1 = server
#	2 = user
#	3 = password
if [ $# != 3 ]
then
	echo "Usage: usagereport.sh server user password"
	exit 1
fi

# Save the runstring parameters in local variables
logon=$2
password=$3
dataserver=$1


#set buffer and output file names
SALES='/users/xls/src/scripts/reports/sales/$$.sales'
rm *.sales

isql /U$logon /P$password /S$dataserver -n -h /w 50000 << EOF > $SALES

SET NOCOUNT ON
go

declare @startdate smalldatetime
declare @enddate smalldatetime

-- Run for previous month 
select @startdate = DATEADD(month, -3, GETDATE())

-- Move to beginning of month 
select @startdate = DATEADD(day, -1 * (DATEPART(day, @startdate) -1), @startdate)
select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

-- Calculate the end of the monthly period 
select @enddate = DATEADD (month, 1, @startdate)


PRINT ".XLS Sales Report for Commissioning Purposes"
select CONVERT(varchar(16),@startdate,107) "For Month Starting"
PRINT "Company/Account Name|User Last Name|Content Provider|Salesperson Last Name|Description|Date|List Price|Price"

select 
	convert (varchar(40),COALESCE (p.company, a.name)) "Company/Account Name", "|",
	convert (varchar(16),p.last_name) "User Last Name", "|",
	convert (varchar(16),i.name) "Content Provider", "|",
	convert (varchar(16),s.last_name) "Salesperson Last Name", "|",
	convert (varchar(40),u.description) "Description", "|",
	convert (varchar(10),u.access_time, 1) "Date", "|",
	u.list_price "List Price", "|",
	u.price "Price", "|"
from 
	usage u, users p, ip i, salesperson s, account a
where 
	u.access_time >= @startdate
and 	
	u.access_time < @enddate
and
	u.userid = p.id
and
	u.ip = i.id
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	p.account = a.id
and	
	s.id = p.salesrep
and
	s.id in (39,40,42,47,48,62,64)
and	
	u.no_charge_flag is null

order by
	s.last_name, COALESCE (p.company, a.name)  
compute sum(u.price), 
	sum(u.list_price)
by
	s.last_name, COALESCE (p.company, a.name) 
compute sum(u.price), 
	sum(u.list_price)
by
	s.last_name 
go

EOF

#Save results as excel files for Steve Goldstein
cat *.sales | sed 's/,//g' | awk 'BEGIN{ v1 = 0; v2 = 0; c = 1 } \
/^[ 	]*sum[ 	]*$/{continue} \
/[a-zA-Z]/{ print $0 ; continue ; } \
/^[ 	]*[0-9]+\.*[0-9]*[ 	]*$/{
	if( (c++ % 2) == 0 ){ 
		printf "||||||%s|%s\n",$0,v1 
	}
	else{ v1 = $0 }
}' \
 | tr -s " " | sed 's/\|/,/g' > /users/xls/src/scripts/reports/sales/may99.csv




