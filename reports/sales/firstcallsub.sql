PRINT "Subscribers for FirstCall"
PRINT ""
PRINT ""
select  distinct 
	convert(char(10),p.account)'Account',
	convert(char(80),u.company)'Company',
	convert(char(80),(u.last_name+' '+u.first_name))'User Name',
	convert(char(50),u.email)'Email',
	convert(char(20),(tel_country+' '+tel_area+' '+tel_number))'Phone',
	convert(char(30),f.value)'Login/Pwd'
from 
	users u join account_perm p on p.account=u.account
	join accounts_flag f on f.accountid=p.account
	join charge c on c.account=p.account
	where p.ip in (126,128) and p.permission=1 and u.demo_flag is null
	and u.terminate>getdate() and u.account<>3
	and f.name='FirstCallID_Pwd'
	and c.end_date>=getdate()
