PRINT "Alacra Daily Sales Report for CreditSights - Downloads in last 7 days"
PRINT ""
PRINT ""
select CONVERT(varchar(16),GETDATE(),107) "Today's Date"
PRINT ""
PRINT ""
select " " "Classic Alacra Usage"
select
	convert (varchar(12),u.access_time,107) "Date", 
	convert (varchar(24),COALESCE(p.company,a.name)) "Company",
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(12),p.first_name) "First Name",
	convert (varchar(40),p.email) "Email",
	convert (varchar(12),p.login) "User-ID",
	CASE WHEN p.demo_flag is NULL THEN 'N' ELSE "Y" END "Alacra Trial",
	convert (varchar(12),u.list_price) "List Price",
	convert (varchar(12),CASE WHEN p.demo_flag is NULL THEN u.price ELSE 0.0 END) "Billed",
	convert (varchar(256),u.description) "Report Description"
from
	users p, usage u, account a
where
	u.ip = 273
	and
	p.account = a.id
	and
	u.userid = p.id
	and
	u.access_time >= DATEADD(day, -7, GETDATE())
	and
	a.id <> 3
	and
	p.service <> 57
order by
	u.access_time, p.id
PRINT ""
PRINT ""
select " " "Alacra Store Usage"
select
	convert (varchar(12),u.access_time,107) "Date", 
	convert (varchar(24),COALESCE(p.company,a.name)) "Company",
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(12),p.first_name) "First Name",
	convert (varchar(40),p.email) "Email",
	convert (varchar(12),p.login) "User-ID",
	CASE WHEN p.demo_flag is NULL THEN 'N' ELSE "Y" END "Alacra Trial",
	convert (varchar(12),u.list_price) "List Price",
	convert (varchar(12),CASE WHEN p.demo_flag is NULL THEN u.price ELSE 0.0 END) "Billed",
	convert (varchar(256),u.description) "Report Description"
from
	users p, usage u, account a
where
	u.ip = 273
	and
	p.account = a.id
	and
	u.userid = p.id
	and
	u.access_time >= DATEADD(day, -7, GETDATE())
	and
	a.id <> 3
	and
	p.service = 57
order by
	u.access_time, p.id
