XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database"
	exit 1
fi

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`
emailgrp=fsaperms@alacra.com

mkdir -p $XLSDATA/reports
cd $XLSDATA/reports

outputfile=fsa_temp.txt
reportfile=fsa_permissions`date +%m`.xls

bcp "select u.login, u.last_name,u.first_name,u.email,u.account,u.id, ip.id,ip.name, case when up.permission is not null then 'User' else 'Account' end, coalesce(up.permission, ap.permission), coalesce(up.dataset, ap.dataset),coalesce(up.tier, ap.tier), coalesce(up.dmouserid, ap.userid) from users u cross join ip left outer join account_perm ap on (u.account=ap.account and ap.ip=ip.id) left outer join user_perm up on (u.id = up.userid and up.ip=ip.id) where ip.id in (111,460) and u.account=6829 and coalesce(up.permission, ap.permission,2) !=2 order by ip.name,u.login " queryout ${outputfile} /U${logon} /P${password} /S${dataserver} /c
rc=$?
if [ $rc -ne 0 ]
then
	blat -body "Error generating FSA permissions report" -t administrators@alacra.com -s "Error generating FSA permissions report"
	exit $rc
fi

cat $XLS/src/scripts/reports/fsaperms/fsa_header.tsv ${outputfile} > ${reportfile}

blat -body "attached" -attach ${reportfile} -t ${emailgrp} -s "FSA permissions"

