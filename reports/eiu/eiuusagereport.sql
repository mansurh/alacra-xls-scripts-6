set tran isolation level read uncommitted
PRINT "EIU Daily Usage Report"
PRINT ""
PRINT ""
select CONVERT(varchar(16),GETDATE(),107) "Today's Date"
PRINT ""
PRINT ""


PRINT "Subscribers expiring in the next 30 days (IP users)"
PRINT "========================================"
select
	v.name 'Service',
	COALESCE(p.company,a.name) 'Company',
	p.last_name 'Last Name',
	p.first_name 'First Name',
	p.login 'User-ID',
	convert (varchar(12),p.terminate,107) 'Expiration',
	COALESCE (convert (varchar(3),sp.abbreviation),''),
	COALESCE (c.name,'') 'Country'
from
	users p, account a left outer join country c on (a.country = c.id) left outer join state_prov sp on (a.state_prov = sp.id), service v
where
	p.terminate >= GETDATE()
	and
	p.terminate <= DATEADD(day,30,GETDATE())
	and
	p.demo_flag is null
	and
	p.account = a.id
	and
	p.service = v.id
	and
	(p.service = 17 or p.service = 50)
	and 
	p.account != 3250
    and 
    p.id in (select userid from ipauth)
order by v.name, COALESCE(p.company,a.name), p.last_name

PRINT "Subscribers expiring in the next 30 days"
PRINT "========================================"
select
	v.name 'Service',
	COALESCE(p.company,a.name) 'Company',
	p.last_name 'Last Name',
	p.first_name 'First Name',
	p.login 'User-ID',
	convert (varchar(12),p.terminate,107) 'Expiration',
	COALESCE (convert (varchar(3),sp.abbreviation),''),
	COALESCE (c.name,'') 'Country'
from
	users p, account a left outer join country c on (a.country = c.id) left outer join state_prov sp on (a.state_prov = sp.id), service v
where
	p.terminate >= GETDATE()
	and
	p.terminate <= DATEADD(day,30,GETDATE())
	and
	p.demo_flag is null
	and
	p.account = a.id
	and
	p.service = v.id
	and
	(p.service = 17 or p.service = 50)
	and 
	p.account != 3250
    and 
    p.id not in (select userid from ipauth)
order by v.name, COALESCE(p.company,a.name), p.last_name
	
PRINT ""
PRINT ""

PRINT "Demo User IDs expiring in the next 7 days"
PRINT "========================================="
select
	convert (varchar(12),v.name) "Service",
	convert (varchar(32),COALESCE(p.company,a.name)) "Company",
	convert (varchar(32),p.last_name) "Last Name",
	convert (varchar(32),p.first_name) "First Name",
	convert (varchar(32),p.login) "User-ID",
	convert (varchar(12),p.terminate,107) "Expiration"
from
	users p, account a, service v
where
	p.terminate >= GETDATE()
	and
	p.terminate <= DATEADD(day,7,GETDATE())
	and
	p.demo_flag is not null
	and
	p.account = a.id
	and
	p.service = v.id
	and
	(p.service = 17 or p.service = 50)
	and 
	p.account != 3250
order by 
	convert (varchar(12), v.name), convert (varchar(32),COALESCE(p.company,a.name)), p.last_name

PRINT ""
PRINT ""

PRINT "Subscribers who have never logged in"
PRINT "===================================="
select
	convert (varchar(12),v.name) "Service",
	convert (varchar(32),COALESCE(p.company,a.name)) "Company",
	convert (varchar(32),p.last_name) "Last Name",
	convert (varchar(32),p.first_name) "First Name",
	convert (varchar(32),p.login) "User-ID",
	convert (varchar(12),p.terminate,107) "Expiration"
from
	users p, account a, service v
where
	p.demo_flag is null
--	and
--	p.acceptance is null
	and
    p.login not in (select login from logins) 
	and
	p.terminate >= GETDATE()
	and
	p.account = a.id
	and
	p.service = v.id
	and
	(p.service = 17 or p.service = 50)
	and 
	p.account != 3250
order by 
	convert (varchar(12), v.name), convert (varchar(32),COALESCE(p.company,a.name)), p.last_name
	
PRINT ""
PRINT ""

PRINT "Subscribers who have never downloaded data"
PRINT "===================================="
select
	convert (varchar(12),v.name) "Service",
	convert (varchar(32),COALESCE(p.company,a.name)) "Company",
	convert (varchar(32),p.last_name) "Last Name",
	convert (varchar(32),p.first_name) "First Name",
	convert (varchar(32),p.login) "User-ID",
	convert (varchar(12),p.terminate,107) "Expiration"
from
	users p, account a, service v
where
	p.demo_flag is null
	and
    p.id not in (select userid from usage)
	and
	p.terminate >= GETDATE()
	and
	p.account = a.id
	and
	p.service = v.id
	and
	(p.service = 17 or p.service = 50)
	and 
	p.account != 3250
order by 
	convert (varchar(12), v.name), convert (varchar(32),COALESCE(p.company,a.name)), p.last_name
	
PRINT ""
PRINT ""

PRINT "Demo User Downloads in the past 7 days"
PRINT "================================================================"
select 
	convert (varchar(24),COALESCE(p.company,a.name)) "Company",
	convert (varchar(32),p.last_name) "Last Name",
	convert (varchar(32),p.first_name) "First Name",
	convert (varchar(32),p.login) "User-ID",
	convert (varchar(32),i.name) "Content Provider", 
	convert (varchar(4), count(u.list_price)) "Count", 
	convert (varchar(10),sum(u.list_price)) "Total $",
	substring (convert (varchar(8),max(u.access_time),112),5,4) "Last Access"
from 
	usage u, users p, ip i, account a
where
	u.access_time >= DATEADD(day,-7,GETDATE())
and
	u.userid = p.id
and
	u.ip = i.id
and
	p.demo_flag is not null
and
	p.account = a.id
and
	(p.service = 17 or p.service = 50)
and 
	p.account != 3250
group by 
	convert (varchar(24),COALESCE(p.company,a.name)),p.last_name, p.first_name, p.login, i.name
order by
	convert (varchar(24),COALESCE(p.company,a.name)),p.last_name, p.first_name, p.login, i.name

go
