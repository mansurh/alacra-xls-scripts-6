XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 = database

# Save the runstring parameters in local variables
database=xls
if [ $# -gt 0 ]
then
	database=$1
fi
server=`get_xls_registry_value $database Server`
user=`get_xls_registry_value $database User`
pass=`get_xls_registry_value $database Password`

main() {
rm -f *.tsv
filename=account.lst
query="select id, replace(name,' ','_') from account where id in (select distinct account from users where account in (select account from account_perm where ip in (58,133,134) and permission=1) and account!=3250 and demo_flag is null and terminate>dateadd(day,-30,getdate()) and service in (17,50)) order by name"
bcp "$query" queryout $filename /U${user} /P${pass} /S${server} /c
if [ $? -ne 0 ]
then
  echo "error in getting account list"
  exit 1
fi

typeset -i day
day=`date +%e`
IFS="	"
while read id name
do
  query="select u.login, u.last_name, u.first_name, us.access_time,us.remote_address,ip.name,us.description,us.list_price from usage us, users u, ip where us.ip=ip.id and access_time >= convert(varchar(7),dateadd(day,-10,getdatE()),120)+'-01' and access_time <convert(varchar(10),dateadd(day,-$day+1,getdate()),120) and us.userid=u.id and u.account="$id
  usagefile=$name-$id.tsv
  bcp "$query" queryout $usagefile /U${user} /P${pass} /S${server} /c
  if [ $? -ne 0 ]
  then
    echo "error getting data for account $id"
    exit 1
  fi
  if [ ! -s $usagefile ]
  then
    echo "No usage for $id account"
  fi
done < $filename

zip eiuusage.zip *.tsv
blat -attach eiuusage.zip -body "Monthly usage attached" -s "EIU Monthly Account Usage" -t eiumonthlyusage@alacra.com

}

mkdir -p $XLSDATA/eiuusage
cd $XLSDATA/eiuusage
logfile=eiuusage`date +%y%m`.log
(main) > $logfile 2>&1
if [ $? -ne 0 ]
then
	blat $logfile -t administrators@alacra.com -s "Error running EIU monthly usage reports"
	exit 1
fi