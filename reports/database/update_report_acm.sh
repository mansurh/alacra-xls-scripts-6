XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn


if [ $# -gt 0 ]
then
  updatereport=${1}
else
  updatereport=update_report_acm`date +%y%m%d`.txt
fi

if [ $# -gt 1 ]
then
  DatabaseReportList=${2}
else
  DatabaseReportList=DatabaseReportListACM.xls
fi

rm -f ${DatabaseReportList}
rm -f ${updatereport}

echo "ACM Collection Name                                          IP   Last Update  Update freq  Overdue by   Number of Docs" > ${updatereport}
echo "-----------------------------------------------------------  ---- ------------ ------------ ------------ --------------" >> ${updatereport}


#create output for Excel file
echo -e "ACM Collection Name\tIP\tLast update\tUpdate freq\tOverdue by\tNumber of Docs" > ${DatabaseReportList}

for dbname in acm acm2
do
  server=`get_xls_registry_value ${dbname} server`
  user=`get_xls_registry_value ${dbname} user`
  password=`get_xls_registry_value ${dbname} password`

  bcp "exec dbo.get_update_report" queryout ${DatabaseReportList}.tmp  /U${user} /P${password} /S${server} /c
  if [ $? -ne 0 ]; then echo "error executing get_update_report"; exit 1; fi;

  cat ${DatabaseReportList}.tmp | cut -f 1-6 | sed 's/\t/ /g' >> ${updatereport}
  cat ${DatabaseReportList}.tmp >> ${DatabaseReportList}
done




exit 0
