rm -f database.txt
touch database.txt

# Run for data 2
echo "" >> database.txt
echo "Data 2 Databases:" >> database.txt
echo "=================" >> database.txt
sybtcl dbsize.tcl data2 sa newpass >> database.txt

# Run for data 3
echo "" >> database.txt
echo "Data 3 Databases:" >> database.txt
echo "=================" >> database.txt
sybtcl dbsize.tcl data3 sa newpass >> database.txt

# Run for data 4
echo "" >> database.txt
echo "Data 4 Databases:" >> database.txt
echo "=================" >> database.txt
sybtcl dbsize.tcl data4 sa newpass >> database.txt

# Run for data 5
echo "" >> database.txt
echo "Data 5 Databases:" >> database.txt
echo "=================" >> database.txt
sybtcl dbsize.tcl data5 sa newpass >> database.txt

# Run for data 7
echo "" >> database.txt
echo "Data 7 Databases:" >> database.txt
echo "=================" >> database.txt
sybtcl dbsize.tcl data7 sa newpass >> database.txt

# Run for data 8
echo "" >> database.txt
echo "Data 8 Databases:" >> database.txt
echo "=================" >> database.txt
sybtcl dbsize.tcl data8 sa newpass >> database.txt

# Mail to the administrators
blat database.txt -t administrators@xls.com -s "Daily Database Stats"
