TITLEBAR="ControlDB snapshot"

#parse the command line arguments
# 1 = sourcedb
# 2 = source server
# 3 = source login
# 4 = source passwd
# 5 = dest db
# 6 = dest server
# 7 = dest user
# 8 = dest passwd

if [ $# -lt 8 ]
then
    echo "Usage: transferSpider.sh srcdb srcserver srclogin srcpasswd destdb destserver destlogin destpasswd"
    exit 1
fi

srcdb=$1
srcserver=$2
srcuser=$3
srcpasswd=$4
destdb=$5
destserver=$6
destuser=$7
destpasswd=$8

echo "Performing SPIDER snapshot from ${srcdb} (${srcserver}) to ${destdb} (${destserver})"

for table in control_errs control_status control_runs include_map def_specialty exclude_map geo_codes geo_hi geo_map geo_map_hier industry_codes industry_map jobfunc_codes jobfunc_map sites sitetype_codes sitetype_map specialty_codes specialty_map subject_codes subject_map url_aliases index_codes index_map 
# spider_stats parse_stats
do

transfer-table.sh ${table} ${srcdb} ${srcserver} ${srcuser} ${srcpasswd} ${table} ${destdb} ${destserver} ${destuser} ${destpasswd}

done

echo "Snapshot complete."