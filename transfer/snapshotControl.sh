TITLEBAR="ControlDB snapshot"

#parse the command line arguments
# 1 = spider universe

if [ "$#" -gt 1 ]
then
	echo "Usage: snapshotControl.sh [SpiderUniverse]"
	exit 1
fi

if [ "$#" -eq 1 ]
then
	siteuniverse=$1
	destdb="controldb_${siteuniverse}"
else
	destdb="controldb"
fi

echo "Reading Registry entries for SpiderDB and ControlDB"
. ./getRegistryEntries.sh $destdb

echo "Copying sites table from spiderdb (${srcserver}) to ${destdb} (${destserver})"
echo "parameters: spiderdb ${srcserver} ${srcuser} ${srcpasswd} ${destdb} ${destserver} ${destuser} ${destpasswd} ${siteuniverse}"

if [ "$#" -eq 1 ]
then
	. ./transferSiteTable.sh spiderdb ${srcserver} ${srcuser} ${srcpasswd} ${destdb} ${destserver} ${destuser} ${destpasswd} ${siteuniverse}
else
	. ./transferSiteTable.sh spiderdb ${srcserver} ${srcuser} ${srcpasswd} ${destdb} ${destserver} ${destuser} ${destpasswd}
fi

echo "Performing SPIDER snapshot from spiderdb (${srcserver}) to ${destdb} (${destserver})"

srcdb="spiderdb"

for table in sites_universe include_map exclude_map geo_codes geo_hi geo_map geo_map_hier industry_codes industry_map jobfunc_codes jobfunc_map sitetype_codes sitetype_map def_specialty specialty_codes specialty_map subject_codes subject_map url_aliases def_specialty index_map index_codes
do

. ./transferTable.sh ${table} ${srcdb} ${srcserver} ${srcuser} ${srcpasswd} ${destdb} ${destserver} ${destuser} ${destpasswd}

done

echo "Initializing destination database ${destdb}..."

if [ $siteuniverse ]
then
	isql /n /U${destuser} /P${destpasswd} /S${destserver} /Q"exec INIT_STATUS '$siteuniverse'"
else
	isql /n /U${destuser} /P${destpasswd} /S${destserver} /Q"exec INIT_STATUS"
fi

if [ $? != 0 ]
then
    echo "Error initializing ${destdb}"
    exit 1
fi

isql /n /U${destuser} /P${destpasswd} /S${destserver} /Q"insert into control_runs values(GETDATE(), 'FULL')"

if [ $? != 0 ]
then
    echo "Error inserting values into control_runs"
    exit 1
fi

echo "Snapshot complete."
