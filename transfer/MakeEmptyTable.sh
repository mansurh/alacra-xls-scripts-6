#parse the command line arguments
# 1 = source server
# 2 = source login
# 3 = source passwd
# 4 = source db
# 5 = source table name
# 6 = empty table name

shname=MakeEmptyTable.sh
if [ $# -lt 6 ]
then
    echo "Usage: ${shname} srcserver srclogin srcpasswd srcdb tablename emptyname"
    exit 1
fi

srcserver=${1}
srcuser=${2}
srcpasswd=${3}
srcdb=${4}
tablename=${5}
emptyname=${6}

asdf=$(date +%Y%W%H%M%S)
tempfilename=${emptyname}${asdf}.sql

if [ -e ${tempfilename} ]
then
	rm -f ${tempfilename}
fi


echo "IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id('${emptyname}'))" > ${tempfilename}
echo "BEGIN" >> ${tempfilename}
echo "   PRINT 'Dropping existing ${emptyname}'" >> ${tempfilename}
echo "   DROP table ${emptyname}" >> ${tempfilename}
echo "END" >> ${tempfilename}
echo "GO" >> ${tempfilename}
echo "" >> ${tempfilename}
echo "SELECT * INTO ${emptyname} FROM ${tablename} WHERE 1 = 0" >> ${tempfilename}
echo "GO" >> ${tempfilename}

echo "${shname}: Creating empty table ${emptyname} with the structure from table ${tablename}"
echo "${shname}: isql -S ${srcserver} -U ${srcuser} -P ${srcpasswd} -d xls < ${tempfilename}"
isql -S ${srcserver} -U ${srcuser} -P ${srcpasswd} -d ${srcdb} < ${tempfilename}
if [ $? != 0 ]
then
	echo "${shname}: Error creating empty table ${emptyname} with structure of table ${tablename}"
	exit 1
fi

rm -f ${tempfilename}

exit 0
