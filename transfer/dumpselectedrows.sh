#parse the command line arguments
# 1 = source query
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = temp file

shname=$0
if [ $# -lt 6 ]
then
    echo "Usage: ${shname} srcquery srcdb srcserver srclogin srcpasswd tempfile"
    exit 1
fi

query=${1}
srcdb=${2}
srcserver=${3}
srcuser=${4}
srcpasswd=${5}
tempfile=${6}


# delete temp file
rm -f ${tempfile}*

tempfileout=${tempfile}.out

echo "${shname}: Dumping of specific rows for query '${query}' from ${srcdb} (${srcserver}) to disk"

# query the data out
bcp "${query}" queryout ${tempfile} /S${srcserver} /U${srcuser} /P${srcpasswd} /N /CRAW /CRAW /b10000 /o${tempfileout} 
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

cat ${tempfileout} 
exit 0
