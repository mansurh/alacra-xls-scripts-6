# XlsBackup.sh
# Parse the command line arguments.
#	1 = source server
#	2 = noget (optional)
#	3 = daily|weekly

shname=XlsBackup.sh

TITLEBAR="Backing up XLS Database"

if [ $# -lt 1 ] || [ $# -gt 4 ]
then
	echo "Usage: ${shname} sourceserver noget(Y|N) [updatetype(daily|weekly)] [backupserver] "
	exit 1
fi
. $XLS/src/scripts/loading/dba/get_registry_value.fn
. $XLS/src/scripts/loading/dba/check_server_status.fn
# log file name
mkdir -p $XLSDATA/XlsBackup/log
#rm -f $XLSDATA/XlsBackup/*.log

logfilename=$XLSDATA/XlsBackup/log/XlsBackup`date +%y%m%d%H`.log
progressfile=$XLSDATA/XlsBackup/backup.progress

# Save the runstring parameters in local variables
sourceserver=`echo ${1} | awk '{ print tolower($1)}'`
destserver=`echo $COMPUTERNAME | awk '{ print tolower($1)}'`
echo ${sourceserver} 
echo ${destserver}
if [ ${sourceserver} = ${destserver} ]
then
	echo "source and destination servers are the same:  ${sourceserver}, ${destserver}, exiting ..." > ${logfilename}
	blat ${logfilename} -t "administrators@alacra.com" -s "XLS Database Backup Failed"
	exit 1
fi
#check_server_status xls ${destserver}
#server_status=$?
#if [ ${server_status} -eq 1 ]
#then
#	echo "destination ${destserver} is currently online, exiting ..." > ${logfilename}
#	blat ${logfilename} -t "administrators@alacra.com" -s "XLS Database Backup Failed"
#	exit 1
#fi

destuser=`get_xls_registry_value xlsdbo user`
destpsw=`get_xls_registry_value xlsdbo password`
sourceuser=${destuser}
sourcepsw=${destpsw}
  
# optional 'noget' as 2nd param skips source safe step
if [ $# -gt 1 ]
then
	noget=$2
else
	noget=N
fi

updatetype="daily"
# optional updatetype as 3rd param for daily vs weekly update
if [ $# -gt 2 ]
then
	updatetype=$3
fi

backupserver=""
if [ $# -gt 3 ]
then
	backupserver=$4
fi

if [ -e ${progressfile} ]
then
	echo "${shname}: Another XLS Backup script is running, exiting ... " > noupdate.log
	date >> noupdate.log
	blat noupdate.log -t "administrators@alacra.com" -s "XLS Database Backup Stopped (${updatetype})"
	exit 1
fi
date > ${progressfile}

if [ ${updatetype} = "incr" ]
then
  echo "Start incremental update" > ${logfilename}
else
  $XLS/src/scripts/loading/dba/startupdate.sh xlslocal ${destserver} xlscompany > ${logfilename} 2>&1
  if [ $? -ne 0 ]
  then
	echo "error in startupdate, exiting ..."
	blat ${logfilename} -t "administrators@alacra.com" -s "XLS Database Backup Failed"
	rm -f ${progressfile} 
	exit 1
  fi
fi

echo "${shname}: Backing up the XLS Database" >> ${logfilename}
echo "${shname}: Copying xls database from ${sourceserver} to ${destserver}" >> ${logfilename}
echo ""

# Perform the update
if [ ${updatetype} = "incr" ]
then
	$XLS/src/scripts/transfer/XlsBackup/transfer_incremental.sh ${sourceserver} ${destserver} ${destuser} ${destpsw} >> ${logfilename} 2>&1
else
	$XLS/src/scripts/transfer/XlsBackup/XlsBackupLow.sh ${sourceserver} ${sourceuser} ${sourcepsw} ${destserver} ${destuser} ${destpsw} ${noget} ${updatetype} ${backupserver} >> ${logfilename} 2>&1
fi
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	blat ${logfilename} -t "administrators@alacra.com" -s "XLS Database Backup Failed (${updatetype})"
    #remove progress file, so incremental updates can still run
    rm -f ${progressfile}
	exit 1
else
#	blat ${logfilename} -t "administrators@alacra.com" -s "XLS Database Backup Succeeded (${updatetype})"
	echo "good return code, all done" >> ${logfilename}
    rm -f ${progressfile}
fi

if [ ${updatetype} = "incr" ]
then
  echo "done with incremental update" >> ${logfilename}
else
  $XLS/src/scripts/loading/dba/endupdate.sh xlslocal ${destserver} xlscompany >> ${logfilename}
fi
exit 0
