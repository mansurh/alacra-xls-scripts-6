#parse the command line arguments
# 1 = table name
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = dest table
# 7 = dest db
# 8 = dest server
# 9 = dest user
# 10 = dest passwd
# 11 = temp file

shname=transferdataonly.sh
if [ $# -lt 11 ]
then
    echo "Usage: ${shname} srctable srcdb srcserver srclogin srcpasswd desttable destdb destserver destlogin destpasswd tempfile"
    exit 1
fi

srctable=${1}
srcdb=${2}
srcserver=${3}
srcuser=${4}
srcpasswd=${5}
desttable=${6}
destdb=${7}
destserver=${8}
destuser=${9}
destpasswd=${10}
tempfile=${11}

LOGFILE=${desttable}.log

rm -f ${tempfile}

echo "${shname}: Transfer of ${srctable} table from ${srcdb} (${srcserver}) to ${desttable} table in ${destdb} (${destserver})"

isql /b /S${destserver} /U${destuser} /P${destpasswd} << EOF >> $LOGFILE
use $destdb
go
set transaction isolation level read uncommitted
print 'copying ${srctable} table' 
go
insert into ${desttable} select * from  ${srcserver}.${srcdb}.dbo.${srctable}
go
EOF

if [ $? -ne 0 ]; then echo "$0: Error in transfer, exiting ..."; exit 1; fi;
exit 0
