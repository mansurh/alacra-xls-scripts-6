import sys
import os
import pyodbc
from ftplib import FTP
import os.path
import subprocess as sproc
import logging

class API_Wizard(object):
	regDir = None
	scriptdir = None
	loaddir = None
	extract_data = None
	args = {}
	

	def __init__(self, regDir, extract_data, args):
		self.regDir = regDir
		self.extract_data = extract_data
		self.args = args
		self.scriptdir = os.environ.get('XLS') + '/src/scripts/loading/' + regDir
		self.loaddir = os.environ.get('XLSDATA') + '/' + regDir
		#initialize logging file name
		logging.basicConfig(filename='logger_bad.log', level=logging.DEBUG, 
		format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p: ')
		
	def _extract_data(self):
		if '${scriptdir}' in self.extract_data:
			unix_command = self.parse_extract_data()
			command = os.popen(unix_command)
			return self.loaddir + '/' + self.args['filename'] #returns the path, but it is not used anywhere...
			
	def parse_extract_data(self):
		dataString = ''
		data = self.extract_data.split(' ')
		print(data)
		scriptDirectory = False
		for s in data:
			scriptStr = '${scriptdir}'
			loadStr = '${loaddir}'
			if scriptStr in s:
				s_list = s.split('/')
				for sub_s in s_list:
					if sub_s == scriptStr:
						dataString += self.scriptdir + '/'
					elif sub_s[0] == '$':
						dataString += self.args[sub_s[1:]] + '/'
					else:
						dataString += sub_s + '/'
				dataString = dataString[:-1]
				dataString += ' '
			elif loadStr in s:
				s_list = s.split('/')
				for sub_s in s_list:
					if sub_s == loadStr:
						dataString += self.loaddir + '/'
					elif sub_s[0] == '$':
						dataString += self.args[sub_s[1:]] + '/'
					else:
						dataString += sub_s + '/'
				dataString = dataString[:-1]
				dataString += ' '
			elif s[0] == '$':
				dataString += self.args[s[1:]] + ' '
			else:
				dataString += s + ' '	
		print(dataString)
		return dataString
		
		
		
		
		
		
		
		
		