#test_pyodbc.py
#It will atempt to read 10 table names from the sys.tables in the given db
#defaults to dba2 on agl1 

#Two ways to call it
#python3 test_pyodbc.py
#python3 test_pyodbc.py <server name> <db name>

#we asumed that user and password are the same as db name


import pyodbc
from time import sleep
import sys


def main(server='agl1',database='dba2'):
    conn = pyodbc.connect('DRIVER=SQL Server;SERVER={0};DATABASE={1};UID={1}; PWD={1}'.format(server,database))

    sqlstring = "select top 10 name from sys.tables"

    cursor = conn.cursor()
    cursor.execute(sqlstring)
    rows = list(cursor.fetchall())

    print ("Here is a list of 10 tables in db:{0} on server:{1}".format(database,server))
    print("")
    for row in rows:
        print(row[0])

if __name__ == "__main__":
    
    if len(sys.argv) == 1:
        main()
    
    elif len(sys.argv) == 3:
        inserver   = sys.argv[1]
        indatabase = sys.argv[2] 
        main(server=inserver,database=indatabase)

    else:
        print("Wrong number of arguments!!")
        print("Usage:")
        print("python3 test_pyodbc.py")
        print("or python3 test_pyodbc.py <server name> <db name>")









