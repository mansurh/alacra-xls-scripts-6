import logging
import sys

class File_Handler(object):
	file_one = None
	file_two = None
	output_file = None
	delimiter = None
	split = None
	key = None
	exclude = None
	master_dict = {}
	
	def __init__(self, file_one, file_two, output_file, delimiter, key, exclude):
		#initialize logging file name
		logging.basicConfig(filename='logger.log', filemode='w', level=logging.DEBUG, 
		format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p: ')
		
		self.file_one = file_one
		self.file_two = file_two
		self.output_file = output_file
		self.delimiter = delimiter
		if delimiter == 'tab':
			self.split = '\t'
		else:
			self.split = self.delimiter
		self.key = list(map(int, key.split('|')))
		self.exclude = list(map(int, exclude.split('|')))
		
	def parse(self, s_to_parse):
		try:
			return s_to_parse.split(self.delimiter)
		except TypeError:
			logging.error("TypeError - " + s_to_parse + " should be a string.")
			sys.exit()
			
		
	def read_files(self):
		try:
			f_one_dict = self.make_file_dict(self.file_one)	
			# always make the second one after the first.
			f_two_dict = self.make_file_dict(self.file_two)
		except FileNotFoundError:
			logging.error("FileNotFoundError - Check file names.")
			sys.exit()
		return [f_one_dict,f_two_dict]
		
	def make_file_dict(self, file_name):
		file_dict = {}
		with open(file_name, encoding='latin-1') as f:
			first_l = f.readline().split(self.split)
			values = list(range(1,len(first_l)+1))
			try:
				for num in self.key:
					values.remove(num)
				for num in self.exclude:
					values.remove(num)
				values_no_exclude = values + self.exclude + self.key
				values_no_exclude.sort(key=int)
			except ValueError:
				logging.error("ValueError - invalid key or exclude argument given.")
				sys.exit()
		with open(file_name, encoding='latin-1') as f:
			for line in f:
				dict_key = ''
				dict_val = ''
				line = line.replace('\n','')
				l_list = line.split(self.split)
				try:
					dict_key = [ l_list[i-1] for i in self.key ]
					dict_key = '|'.join(dict_key)

					dict_val = [ l_list[i-1] for i in values ]
					dict_val = '|'.join(dict_val)
				except IndexError:
					logging.error("IndexError - list index out of range.")
					sys.exit()

				file_dict[dict_key] = dict_val
				self.master_dict[dict_key] = line
				
		return file_dict
		
	def create_output_file(self, dict_one, dict_two, output_file):
		try:
			for key in dict_one:
				if key in dict_two:
					if dict_one[key] != dict_two[key]:
						output_file.write('U' + self.split + self.master_dict[key] + '\n')
						dict_two.pop(key, None)
					else:
						dict_two.pop(key, None)
						
				else:	
					output_file.write('D' + self.split + self.master_dict[key] + '\n')
		
			for key in dict_two:
				output_file.write('I' + self.split + self.master_dict[key] + '\n')
		except KeyError:
			logging.error('KeyError - Invalid key given for a dictionary')
			sys.exit()
		

		