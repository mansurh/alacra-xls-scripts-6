#!/usr/bin/python3 

import argparse
import logging
import sys
from lib.file_handler import File_Handler

def main():
	#initialize logging file name
	logging.basicConfig(filename='logger.log', filemode='w', level=logging.DEBUG, 
	format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p: ')
	
	def msg(name='File Difference Handler'):
		return "python " + name + ".py TODO ... msg()"
		
	parser = argparse.ArgumentParser()
	parser.add_argument('file_one', type=str) 
	parser.add_argument('file_two', type=str)
	parser.add_argument('output_file', type=str)
	parser.add_argument('delimiter', type=str)
	parser.add_argument('key', type=str)
	parser.add_argument('exclude', type=str)

	args = parser.parse_args()
	
	file_handler = File_Handler(args.file_one, args.file_two, args.output_file, args.delimiter, args.key, args.exclude)
	file_dicts = file_handler.read_files()
	output_file = open(file_handler.output_file, 'w')
	
	file_handler.create_output_file(file_dicts[0], file_dicts[1], output_file)



if __name__ == '__main__':
	main()