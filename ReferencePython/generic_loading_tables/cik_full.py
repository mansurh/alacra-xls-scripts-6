import argparse
import sys
import os
import time
import subprocess
from datetime import timedelta, date

def main():
	# Command line parsing.
	def msg(name='cik_full.py'):
		return "python " + name + ".py TODO msg()"
	# Parser for user argument input.
	parser = argparse.ArgumentParser(prog='cik_full', usage=msg())
	
	# The destination argument is used to define the directory where files will be downloaded to.
	# $XLSDATA/destination
	parser.add_argument('--destination', type=str, metavar='',nargs='?')

	args = parser.parse_args()
	
	# If the user provided a destination argument it is set here.
	destination = args.destination
	# If the user did not define a destination argument it is set to 'cik_full'.
	if destination == None: destination = 'cik' 
	
	
	# ---------- VARIABLES ---------- #
	XLSDATA = os.environ.get('XLSDATA') + '/'
	XLS = os.environ.get('XLS') + '/' 
	month = date.today().month
	year = date.today().year
	today = date.today().strftime('%Y-%m-%d')
	quarters = {1: 'QTR1', 2: 'QTR1', 3: 'QTR1', 
				4: 'QTR2', 5: 'QTR2', 6: 'QTR2', 
				7: 'QTR3', 8: 'QTR3', 9: 'QTR3', 
				10: 'QTR4', 11: 'QTR4', 12: 'QTR4'}
	# ---------- VARIABLES ---------- #
	
	# Create destination directory.
	unix_command = 'mkdir -p ' + XLSDATA + destination + '/data_files/' + str(today)
	os.popen(unix_command)
	unix_command = 'mkdir -p ' + XLSDATA + destination + '/bcp_files/' + str(today) 
	os.popen(unix_command)
	
	# Clean up the destination directory from previous times the script has been run.
	unix_command = "cd " + XLSDATA + destination + '/data_files/' + str(today) + "; rm -f *.idx"
	os.popen(unix_command)
	unix_command = "cd " + XLSDATA + destination + '/bcp_files/' + str(today) + "; rm -f *.txt"
	os.popen(unix_command)

	# Fetch this quarter's master index file.
	base_url = 'https://www.sec.gov/Archives/edgar/full-index/'
	url = base_url + str(year) + '/' + quarters[month] + '/master.idx'
	
	# Set the working directory
	os.chdir(XLSDATA + destination + '/data_files/' + today)	
	unix_command = 'cd ' + XLSDATA + destination + '/data_files/' + today
	print(unix_command)
	os.popen(unix_command)

	# Download the file
	unix_command = 'wget "' + url + '"'
	print(unix_command)
	url_status = subprocess.Popen(unix_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
	stdout_value = url_status.communicate()[0].decode("utf-8")
	print(stdout_value)
	
	if 'ERROR 404' in stdout_value:
		print("File at " + url + " could not be found")
		sys.exit(1)

	# Parse the file so the data is in correct bcp format
	with open(XLSDATA+destination + '/data_files/' + str(today) + '/master.idx', 'r') as masterfile:
		emptyHeader = 0
		while not emptyHeader:
			headerData = masterfile.readline()
			if headerData != '\n' and headerData == ((len(headerData) - 1) * '-') + '\n': # if we are at the end of the line, all the characters are '-' and we know its the divider
				break 
		with open(XLSDATA+destination + '/bcp_files/' + str(today) + '/cik_master_load.txt', 'w') as newmasterfile:
			documentEnd = 0
			while not documentEnd:
				readData = masterfile.readline().strip('\n')
				if not readData:
					documentEnd = 1
					break
				readData = readData + "\r\n"
				newmasterfile.write(readData)
			newmasterfile.close()
		masterfile.close()
		unix_command = 'cp ' +XLSDATA+destination + '/bcp_files/' + str(today) + '/cik_master_load' + '.txt '
		unix_command = unix_command + XLS+'src/scripts/referencepython/generic_loading_tables/arg_files/cik/' 
		os.popen(unix_command)

if __name__ == '__main__':
	main()