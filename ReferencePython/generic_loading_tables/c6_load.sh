XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn
. $XLSUTILS/check_bcp_errors.fn
. $XLSUTILS/get_specific_date.fn
. $XLS/src/scripts/loading/namematching/NameMatchingLoadV2.fn

inputArguments=$#
mode="daily"
if [ $inputArguments -gt 0 ]
then
    mode=$1
fi
typeset -i daysBack=0
if [ $inputArguments -gt 1 ]
then
    daysBack=$2
fi

today=$(date +%Y%m%d)
day=$(date --date="${today} -${daysBack} day" +%Y%m%d)
datadir="$XLSDATA/c6/data/"${day}
logdir="$XLSDATA/c6/log/"${day}
logfile=${logdir}"/c6_parse_"${day}".log"

mkdir -p ${datadir}
mkdir -p ${logdir}
cd ${datadir}
	
main()
{	

    dataBase=c6
    dbServer=`get_xls_registry_value c6 Server 0`
    dbUser=`get_xls_registry_value c6 User 0`
    dbPassword=`get_xls_registry_value c6 Password 0`
    ftpServer=`get_xls_registry_value c6 Server 6`
    ftpUser=`get_xls_registry_value c6 User 6`
    ftpPassword=`get_xls_registry_value c6 Password 6`
    ftpDirectory=`get_xls_registry_value c6 Directory 6`
    execdir=$XLS/src/scripts/loading/ReferencePython/generic_loading_tables

    #Decide which files to download
    case ${mode} in
        "sample") files=('BusinessSample.xml',  'PersonSample.xml')
        ;;
        "daily") files=("Business_UPDATE_"${day}".xml" "Individual_UPDATE_"${day}".xml")
        ;;
        "full") files=('Business.xml' 'Individual_1.xml' 'Individual_2.xml' 'Individual_3.xml' 'Individual_4.xml' 'Individual_5.xml' 'Individual_6.xml')
        ;;
    esac

    #Retrieve the files from the FTP
    for file in ${files[@]}
    do
        ret=`wget --user=${ftpUser} --password=${ftpPassword} ftp://${ftpServer}/${ftpDirectory}/${file}`
        check_return_code $? "Could not retrieve a file from the FTP. Aborting" 1
    done

    #Call the Python script the parse the files into BCP files
    printf "python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/c6_parse.py ${mode}\n"
		
    python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/c6_parse.py ${mode} ${day}

    #BCP the files
    cd ${datadir}
    tables=`ls -1 *.bcp | sed -e 's/\.bcp$//'`
    for table in ${tables}
    do
        #printf "sed -i 's/^\xff\xfe//' ${table}.bcp\n"
        #sed -i 's/^\xff\xfe//' ${table}.bcp
        #printf "sed -i '1s/^/\xff\xfe/' ${table}.bcp\n"
        #sed -i '1s/^/\xff\xfe/' ${table}.bcp
        printf "bcp ${table}_temp in ${table}.bcp -S${dbServer} -U${dbUser} -P${dbPassword} -b 1000 -t\"[||]\" -e error_${table}.txt\n"
        bcp ${table}_temp in ${table}.bcp -S${dbServer} -U${dbUser} -P${dbPassword} -b 1000 -k -w -t"[||]" -e error_${table}.txt
        check_return_code $? "Failed to BCP file ${table}"
    done

	#Cleanup temp tables from last load
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spDropC6Temp"
	
    #Create Default Temp Tables
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spCreateDefaultTempTables"

	#Add special case columns to certain tables
    #Add date_updated to temp individual and company tables
    #Add iso_country to temp individual and company address tables
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spAddSpecialColumns"

    #Populate the values for these special columns
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spUpdateSpecialColumns"

    #Create the name matching tables
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spCreateNMTables"

    #Union tables into _new table
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spUnionC6Tables"

    #Update tables (rename tables)
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spUpdateC6Tables"

    #Create Indexes
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spCreateIndexes"
    
    #Set Default profile image
    isql -b -U ${dbUser} -P ${dbPassword} -S ${dbServer} -Q "exec spSetDefaultImage"

    #Cleanup the files
    #Delete BCP files
    #rm ${datadir}/*.bcp

    #Delete XML files
    #rm ${datadir}/*.xml
}

buildNMTable()
{
    NM_DATABASE=$dataBase
    NM_SERVER=$dbServer
    NM_USER=$dbUser
    NM_PASSWORD=$dbPassword
    NM_APPDIR=$execdir
    NM_SCHEMADIR=$XLS/schema/$dataBase
    NM_INCREMENTAL_OR_FULL="full"

    echo "NM_INCREMENTAL_OR_FULL:$NM_INCREMENTAL_OR_FULL"
    loadNameMatching
    check_return_code $? "Error running name matching script. Aborting" 1

    check_bcp_errors $logfile 2000 "Too many BCP errors overall.  Load completed." 2

    date
}

main > ${logfile} 2>&1
if [ $? -ne 0 ] 
then
    blat ${logfile} -t "colin.wong@opus.com" -f "colin.wong@opus.com" -s "C6 Load Failure"
    exit 1
fi
exit 0
