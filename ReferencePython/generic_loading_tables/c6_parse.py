import time
import glob
import os
import traceback
import sys
import xml.etree.ElementTree as eTree
import subprocess as sproc
import pyodbc
import codecs
import subprocess
from ftplib import FTP
from db_handler import DB_Handler
import spoonfeedxml as spfxml

#Example command: python3 c6_parse.py daily
def main():
	# Initialize some variables
	today = time.strftime("%Y%m%d")
	date = time.strftime("%Y-%m-%d")
	data_files = []
	entity_type = ''
	
	# Read in arguments
	if len(sys.argv) == 3:
		load_mode = sys.argv[1]
		today = sys.argv[2]
	else:
		print("Usage: python3 c6_parse.py 'mode' 'today'")

	data_dir = os.environ.get('XLSDATA') + '/c6/data/' + today
	create_dir(data_dir)
	script_dir = os.environ.get('XLS') + '/src/scripts/ReferencePython/'
	
	#Retrieve the needed registry values 
	reg_handler = DB_Handler("c6", "")
	creds = {}
	creds['db_name'] = 'c6'
	creds['db_server'] = reg_handler.get_registry_value('Data Servers', 'c6', 'Server')
	creds['db_user'] = reg_handler.get_registry_value('Data Servers', 'c6', 'User')
	creds['db_password'] = reg_handler.get_registry_value('Data Servers', 'c6', 'Password')
	creds['ftp_server'] = reg_handler.get_registry_value('File Servers', 'c6', 'Server')
	creds['ftp_user'] = reg_handler.get_registry_value('File Servers', 'c6', 'User')
	creds['ftp_password'] = reg_handler.get_registry_value('File Servers', 'c6', 'Password')
	creds['ftp_directory'] = reg_handler.get_registry_value('File Servers', 'c6', 'Directory')
		
	load_modes = {
		'sample': ['BusinessSample.xml', 'PersonSample.xml'],
		'today': ['Business_UPDATE_' + today + '.xml', 'Individual_UPDATE_' + today + '.xml'],
		'full': ['Business.xml', 'Individual_1.xml', 'Individual_2.xml', 'Individual_3.xml', 'Individual_4.xml', 'Individual_5.xml', 'Individual_6.xml'],
		'singlefile': ['Individual_3.xml', 'Individual_2.xml'],
	}
	
	rootTag = None
	
	#Decide which files we are processing
	os.chdir(data_dir)
	#data_files = load_modes[load_mode]
	data_files = glob.glob("*.xml") # Since we download files in the shell script, grab the files in the directory

	# 'table' : the current, accurate representation of the data (_old + _new)
	# 'table'_old : the data from the previous load
	# 'table'_temp : the new data that was just added
	# 'table'_new : _old + _new (should be renamed to 'table')
	c6_handler = DB_Handler("c6", None)
	c6_creds = c6_handler.get_credentials("Data Servers")
	c6_conn = c6_handler.open_connection(c6_creds)
	c6_handler.set_connection(c6_conn)
	
	#Download all the files first
	#for file in data_files:
	#	ftp_file(creds['ftp_server'], creds['ftp_user'], creds['ftp_password'], creds['ftp_directory'], file)

	print(data_files)
	
	print("Initiating fields")
	#Initiate which fields the entity needs
	fields = init_fields(entity_type)
		
	#Store the maximum column sizes for each field in a dictionary
		#Every column length is defaulted to 1
		#Naming schema for the key: table + '/' + column
	max_column_lengths = init_column_lengths(fields)
	
	#Parse every file and create one BCP file for each table
	for file in data_files:
		with codecs.open(file, 'rb', encoding='utf16') as xml_file:
			rootString = spfxml.find_root(xml_file)
		
		#Remember what kind of entity we are working with
		if rootString == "Companies":
			entity_type = 'c'
		else:
			entity_type = 'i'
			
		#Process parts of the file and write it to the bcp file until the file is completely parsed
		for part_of_file in spfxml.xml_spoon_feeder(file):
			#part_of_file is just a small chunk of the original file
			rootTag = eTree.fromstring(part_of_file)

			#Create the bcp files and populate the max_column_lengths
			create_bcp_files(entity_type, fields, rootTag, max_column_lengths)

	#Create temporary SQL tables to bcp the new data in
	create_tables(c6_handler, fields, max_column_lengths, data_dir)

	
#Execute a query given a connection to the database
def exec_db_query(conn, query):
	print(query)
	conn.exec_query(query)

#Create the temp tables for the BCP
# Change it so that it looks into the directory instead of passing in fields
# This will create tables ONLY ONCE which will also always contain the LONGEST fields
def create_tables(conn, fields, column_lengths, directory):
	os.chdir(directory)
	for file in glob.glob("*.bcp"):
		table = file[:-4]
		if not conn.check_table_exists(table + "_temp"):
			tables_query = "CREATE TABLE " + str(table) + "_temp (id VARCHAR(10), "
			for column in fields[table]:
					tables_query += column + " NVARCHAR(" + str(column_lengths[table + "/" + column]) + "), "			
			tables_query = tables_query[:-2] + ");"
			tables_query = tables_query.replace("From ", "From_Date ")
			tables_query = tables_query.replace("To ", "To_Date ")
			
			exec_db_query(conn, tables_query)

	
#List the attribute tags that each PARENT tag will contain
#Order of the attributes matters for BCPing
def init_fields(entity_type):
	fields = {}
	fields['c6_company'] = ['Business_Name','Description','Telephone','Fax','Website','Source','Soft_Delete']
	fields['c6_company_address'] = ['Address_Line_1','Address_Line_2','Address_Line_3','Address_Line_4','Town_City','County_State','Post_Zip_Code','Country','Soft_Delete']
	fields['c6_company_alias'] = ['Business_Name','Soft_Delete']
	fields['c6_company_business_association'] = ['Business_ID_2','Link_Description','Soft_Delete']
	fields['c6_company_article'] = ['Original_URL','C6_URL','File_Name','Source','Date_of_Capture']
	fields['c6_company_note'] = ['Source_of_Notes','Notes','Soft_Delete']
	fields['c6_individual'] = ['Title','Forename','Middlename','Surname','Date_of_Birth','Date_of_Death','Year_of_Birth','Year_of_Death','Gender', 'Home_Telephone','Business_Telephone','Mobile_Telephone','Fax','Email','Nationality','Source','Date_of_Capture','Soft_Delete','Date_of_Soft_Delete','Category','Picture']
	fields['c6_individual_address'] = ['Address_Line_1','Address_Line_2','Address_Line_3','Address_Line_4','Town_City','County_State','Post_Zip_Code','Country','Soft_Delete']
	fields['c6_individual_alias'] = ['AliasID','Forename','Middle_Name','Surname','Soft_Delete']
	fields['c6_individual_individual_association'] = ['Individual_ID_2','Link_Description','Soft_Delete']
	fields['c6_individual_business_association'] = ['Business_ID','Description','Soft_Delete']
	fields['c6_individual_article'] = ['Original_URL','C6_URL','File_Name','Source','Date_of_Capture']
	fields['c6_individual_note'] = ['Source_of_Notes','Notes','Soft_Delete']
	fields['c6_individual_political_position'] = ['Description','From','To','Country','Soft_Delete']
	
	return fields

#Intiailize the dictionary used to hold the longest length of values
def init_column_lengths(fields):
	column_lengths = {}
	for table in fields:
		for column in fields[table]:
			column_lengths[table + "/" + column] = 1
			
	return column_lengths
	
#Parse the data files and create BCP files
def create_bcp_files(entity_type, fields, rootTag, column_lengths):
	#Create empty BCP files for writing
	if entity_type == 'c':
		c6_company = open('c6_company.bcp','a+',encoding='utf-16')
		c6_company_address = open('c6_company_address.bcp','a+',encoding='utf-16')
		c6_company_alias = open('c6_company_alias.bcp','a+',encoding='utf-16')
		c6_company_business_association = open('c6_company_business_association.bcp','a+',encoding='utf-16')
		c6_company_article = open('c6_company_article.bcp','a+',encoding='utf-16')
		c6_company_note = open('c6_company_note.bcp','a+',encoding='utf-16')
			
		bcp_files = [c6_company, c6_company_address, c6_company_alias, c6_company_article, c6_company_business_association, c6_company_note]
	else:
		c6_individual = open('c6_individual.bcp','a+',encoding='utf-16')
		c6_individual_address = open('c6_individual_address.bcp','a+',encoding='utf-16')
		c6_individual_alias = open('c6_individual_alias.bcp','a+',encoding='utf-16')
		c6_individual_individual_association = open('c6_individual_individual_association.bcp','a+',encoding='utf-16')
		c6_individual_business_association = open('c6_individual_business_association.bcp','a+',encoding='utf-16')
		c6_individual_article = open('c6_individual_article.bcp','a+',encoding='utf-16')
		c6_individual_note = open('c6_individual_note.bcp','a+',encoding='utf-16')
		c6_individual_political_position = open('c6_individual_political_position.bcp','a+',encoding='utf-16')
		
		bcp_files = [c6_individual, c6_individual_address, c6_individual_alias, c6_individual_individual_association, c6_individual_business_association, c6_individual_article, c6_individual_note, c6_individual_political_position]
		
	count = 0
	if entity_type == 'c':
		#Write the lines of data for each PARENT tag in every Company
		for company in rootTag.findall('Company'):	
			count += 1
			id = 'C' + company.find('UniqueID').text[1:]
			
			write_bcp_file(company, None, None, id, c6_company, fields['c6_company'], column_lengths)
			write_bcp_file(company, 'Addresses', 'Address', id, c6_company_address, fields['c6_company_address'], column_lengths)
			write_bcp_file(company, 'Aliases', 'Alias', id, c6_company_alias, fields['c6_company_alias'], column_lengths)
			write_bcp_file(company, 'Business_Associations', 'Business_Association', id, c6_company_business_association, fields['c6_company_business_association'], column_lengths)
			write_bcp_file(company, 'Articles', 'Article', id, c6_company_article, fields['c6_company_article'], column_lengths)
			write_bcp_file(company, 'Notes', 'Note', id, c6_company_note, fields['c6_company_note'], column_lengths)
			
			#Flush files so the data stored in RAM goes to HD
			#If runtime is slow, increase the amount of companies before each flush
			if count % 1000 == 0:
				flush_files(bcp_files)
	else:
		#Write the lines of data for each PARENT tag in every Individual 
		for individual in rootTag.findall('Individual'):
			count += 1
			id = individual.find('UniqueID').text
			
			write_bcp_file(individual, None, None, id, c6_individual, fields['c6_individual'], column_lengths)
			write_bcp_file(individual, 'Addresses', 'Address', id, c6_individual_address, fields['c6_individual_address'], column_lengths)
			write_bcp_file(individual, 'Aliases', 'Alias', id, c6_individual_alias, fields['c6_individual_alias'], column_lengths)
			write_bcp_file(individual, 'Individual_Associations', 'Individual_Association', id, c6_individual_individual_association, fields['c6_individual_individual_association'], column_lengths)
			write_bcp_file(individual, 'Business_Associations', 'Business_Association', id, c6_individual_business_association, fields['c6_individual_business_association'], column_lengths)
			write_bcp_file(individual, 'Articles', 'Article', id, c6_individual_article, fields['c6_individual_article'], column_lengths)
			write_bcp_file(individual, 'Notes', 'Note', id, c6_individual_note, fields['c6_individual_note'], column_lengths)
			write_bcp_file(individual, 'Political_Positions', 'Political_Position', id, c6_individual_political_position, fields['c6_individual_political_position'], column_lengths)
			
			#Flush files so the data stored in RAM goes to HD
			#If runtime is slow, increase the amount of companies before each flush
			if count % 1000 == 0:
				flush_files(bcp_files)
	
	close_files(bcp_files)

#Writes lines to the corresponding BCP file for a specific attribute
def write_bcp_file(entity, parent, child, entity_id, file, fields, column_lengths):
	try:
		if parent == None and child == None:
			write_line(entity_id, file, entity, fields, column_lengths)
		else:
			for field in entity.find(parent).findall(child):
				write_line(entity_id, file, field, fields, column_lengths)
	except (TypeError, AttributeError):
		pass

#Write a line to the BCP file
#Print a blank field if an attribute is missing - number and order of fields matter for BCPing
def write_line(id, file, parent, fields, column_lengths):
	line = id + '[||]'
	filename = file.name[:-4]
	for f in fields:
		try:
			value = parent.find(f).text
			
			# If its an ID, remove the type identifier
			if f in ['Business_ID', 'Business_ID_2', 'AliasID', 'Individual_ID_2']:
				value = value[1:]
				# Add 'C' as an ID identifier for all company IDs
				if f in ['Business_ID', 'Business_ID_2']:
					value = 'C' + value
				
			line += value + '[||]'
			#Compare the size of 'f' to the max current length of any same field
			try:
				length = len(value)
				if length > column_lengths[filename + "/" + f]: #Check if the length of this value is longer than any other instance of this field
					column_lengths[filename + "/" + f] = length
			except KeyError: #If it reaches here, the length cannot be retrieved or there is no max length for that field
				column_lengths[filename + "/" + f] = 1
		except: #If it reaches here, the 'value' cannot be retrieved
			try: #So give up and assume its supposed to be null
				line += '[||]'
				if column_lengths[filename + "/" + f] == None: #In case this field doesnt exist in any parent, set a default so we the table can still be created
					column_lengths[filename + "/" + f] = 1
			except KeyError: #If it reaches here, that means there are no instances of this field in the load data
				column_lengths[filename + "/" + f] = 1
		
	line = line[0:-4] + '\r\n'	
	file.write(line)


#BCP files into tables based on their file name + "_new"  
def bcp_files(directory, credentials):
	os.chdir(directory)
	for file in glob.glob("*.bcp"):
		unix_command = "bcp " + file[:-4] + "_temp in " + file + " -S" + credentials["db_server"] + " -U" + credentials["db_user"] + " -P" + credentials["db_password"] + " -b 1000" + ' -w -t"[||]"'
		print(unix_command)
		os.popen(unix_command)
		#subprocess.Popen(unix_command)
			
	return 0  # No errors in BCPing all the files

  
#Create a directory if it does not exist already
def create_dir(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)
	
#Delete all .bcp files in the directory
def cleanup_bcp_files(directory):
	#Move to the directory
	os.chdir(directory)
	for file in glob.glob("*.bcp"):
		os.remove(file)

#Delete all .xml files in the directory
def cleanup_xml_files(directory):
	os.chdir(directory)
	for file in glob.glob("*.xml"):
		os.remove(file)

#Close files
def close_files(files):
	for file in files:
		file.close()
		
#Flush BCP files (move the data from RAM to HD)
def flush_files(files):
	for file in files:
		file.flush()

if __name__ == '__main__':
	main()