from db_retriever import DB_Retriever
from file_hand import File_Hand
from log_me import logger
from datetime import date, datetime
import time
import sys
import os

class Loader(object):
	client_dir = None
	origin = None
	destination = None
	table_name = None
	
	def __init__(self, client_dir, origin, destination, table_name):
		self.client_dir = client_dir
		self.origin = origin
		self.destination = destination
		if len(self.destination) == 0: self.destination = 'concordance'
		self.table_name = table_name
		
	def load(self):
		######################variables##########################
		scriptdir = os.environ.get('XLS') + '/src/scripts/ReferencePython/generic_loading_tables/'
		loaddir = os.environ.get('XLSDATA') + '/' + self.client_dir + '/'
		max_deviation = 100
		stage = 'prod'
		######################variables##########################
		
		logging = logger(self.table_name+'.log',loaddir,loaddir+'log',stage)
		
		fh = File_Hand(self.client_dir, logging)
		files = fh.get_arg_files(self.client_dir, self.table_name)

		for key in files:
			file_name = key + '.txt'
			file_name_err = key + '.err'
			files[key] = sorted(files[key])
			logging.info("Argument files are: " + str(files))
			try:
				columns = fh.get_table_columns(scriptdir + 'arg_files/' + self.client_dir + '/' + files[key][0])   # create_ipid_db_client.txt
				sql_command = fh.get_sql_command(scriptdir + 'arg_files/' + self.client_dir + '/' + files[key][1])   # load_ipid_db_client.txt
			except IndexError:
				logging.error("Create and/or Load file(s) do not exist or are in the incorrect directory.")
			try:
				param = fh.get_parser_param(scriptdir + 'arg_files/' + self.client_dir + '/' + files[key][2])   # parse_ipid_db_client.txt
			except IndexError:
				param = None
			try:
				procedure = fh.get_procedure(scriptdir + 'arg_files/' + self.client_dir + '/' + files[key][3])   # procedure_ipid_db_client.txt
			except IndexError:
				procedure = None
		
			sql_command = sql_command.replace('\n','').split('<><>')
			origin_commands = []
			destination_commands = []
			cmd_types = ['upda','dele','exec','with']
			for sql_cmd in sql_command:
				if sql_cmd.split('|',1)[0] == 'origin':
					origin_commands += [sql_cmd.split('|',1)]
				elif sql_cmd.split('|',1)[0] == 'destination':
					destination_commands += [sql_cmd.split('|',1)]
				else:
					print(sql_command)
					print("ERROR: invalid sql command parsed from load file")
			
			djw_worker = DB_Retriever(self.origin, columns, sql_command, self.client_dir, logging) # origin database worker
			djw_conn = djw_worker.open_connection(djw_worker.get_creds("Data Servers")) # origin connection
			logging.info("Established connection with the origin database")
			djw_worker.set_connection(djw_conn)
			
			con_worker = DB_Retriever(self.destination, columns, sql_command, self.client_dir, logging) # destination database worker
			creds = con_worker.get_creds("Data Servers") # credentials for the destination db
			con_conn = con_worker.open_connection(creds) # destination connection
			logging.info("Established connection with the destination database")
			con_worker.set_connection(con_conn)
			
			con_worker.create_table(key,'_new') # key = table name, columns come from the create file
			logging.info("Creating new table: " + key + "_new")
			if procedure != None:
				djw_worker.execute_command(procedure) # runs the procedure if there is one in the procedure file
				logging.info("Procedure has been run: " + procedure)
				
			for sql_command in origin_commands:
				if sql_command[1][0:6] == 'select':
					data = djw_worker.get_data(sql_command[1]).fetchall()
				else:
					djw_worker.execute_command(sql_command[1])
				#else:
				#	print("error - sql_command didnt start w/ select or update: " + sql_command[1][0:4])
				#	sys.exit()
				
			#data = djw_worker.get_data(sql_command).fetchall() # data from the table resulting from the load file
			logging.info("Data from the load file has been fetched: " + str(len(data)))
			if param != None: # parse or no parse?
				parsed = fh.parser(param[0],param[1],param[2].split(','), data)
				data = fh.update_data_with_parsed(param[0],data,parsed)
				logging.info("Data has been parsed using the parse file.")
			fh.put_data_in_file(file_name, data)
			logging.info("BCP upload file has been created.")
			
			unix_command = 'bcp ' + self.destination + '..' + con_worker.myTable +  '_new in ' + loaddir + file_name + ' -U' + creds['dba_user'] + ' -P' + creds['dba_pass'] + ' -S' + creds['dba_server'] + ' -c -t"|" -CRAW -e ' + loaddir + file_name_err
			os.popen(unix_command).read()
			logging.info("Data has been uploaded to the table via BCP.")
			for sql_command in destination_commands:
				if sql_command[1][0:6] == 'select':
					data = con_worker.get_data(sql_command[1]).fetchall()
				else:
					con_worker.execute_command(sql_command[1])
			
			if con_worker.find_deviation(con_conn, max_deviation):
				print(con_worker.myTable)
				con_worker.rename_tables()
				logging.info("Deviation test has passed, tables have been renamed.")
			else:
				logging.info("Deviation test stopped the system.")
				
			logging.info("Load directories have been created.")
			if fh.check_file_size(file_name, 50):
				fh.move_files(file_name)
				#for key in files:
					#fh.move_arg_files(client_dir, files[key])
				logging.info("Files have been moved to their load directory.\n\n")
			else:
				logging.info('File size check has stopped the system.')
			
		logging.end_success()
		return data
		
	