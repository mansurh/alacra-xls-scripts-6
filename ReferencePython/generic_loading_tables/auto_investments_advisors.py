import argparse
import sys
import os
import time
import pyodbc
from db_retriever import DB_Retriever
from datetime import date
from log_me import logger

def main():
	# Command line parsing
	def msg(name='auto_investments_advisors'):
		return "python " + name + ".py TODO msg()"
	
	# Parser for user argument input.
	parser = argparse.ArgumentParser(prog='auto_investments_advisors', usage=msg())
	
	# Client directory argument used to locate the files needed to BCP data into the destination table
	parser.add_argument('client_dir', type=str)
	
	# Destination is the name of the directory under Data Servers in the registry with the credentials
	# for the database you want to create the table in.
	parser.add_argument('--destination', type=str, metavar='', nargs='?')
	
	#Destination server if it is different then what is in the registry
	parser.add_argument('--server', type=str, metavar='', nargs='?')

	args = parser.parse_args()
	client_dir = args.client_dir
	destination = args.destination
	if not destination: destination = 'concordance'

	# ---------- VARIABLES ---------- #
	XLSDATA = os.environ.get('XLSDATA') + '/'
	XLS = os.environ.get('XLS') + '/'
	today = time.strftime('%Y-%m-%d')
	this_year = time.strftime('%Y')
	this_month = time.strftime('%m')
	loaddir = XLSDATA + client_dir + '/'
	source_id = 10539
	# ---------- VARIABLES ---------- #
	
	# Create destination directory.
	unix_command = 'mkdir -p ' + XLSDATA + client_dir 
	os.popen(unix_command)
	unix_command = 'mkdir -p ' + XLSDATA + client_dir + '/log/'
	os.popen(unix_command)

	logging = logger('log.txt',loaddir,loaddir+'log/','prod')
	
	base_url = 'https://www.sec.gov/files/data/frequently-requested-foia-document-information-about-registered-investment-advisers-and-exempt/ia'
	curr_day = 1 
	url_not_found = True
	while curr_day < 5 and url_not_found:
		url = base_url + str(this_month) + '0' + str(curr_day) + str(this_year)[2:] + '.zip'
		print(url)
		unix_command = 'curl --head ' + url
		url_info = os.popen(unix_command).read()
		url_info = url_info.split('\n')
		if '200 OK' in url_info[0]:
			url_not_found = False
		curr_day = curr_day + 1
		
		
	
	columns = {}
	db_worker = DB_Retriever(destination, columns, '', client_dir, logging) # destination database worker
	creds = db_worker.get_creds("Data Servers")
	server = args.server
	if server:
		creds['dba_server'] = server
	
	logging.info("Establishing a connection with the destination database.")
	# Establish a connection to the destination
	db_conn = db_worker.open_connection(creds)
	db_worker.set_connection(db_conn)
	
	sql_command = "update scraped_sources set url = '" + url + "' where source_id = " + str(source_id)
	logging.info("Executing sql command - " + sql_command)
	db_worker.execute_command(sql_command)
	
	logging.end_success()
	
	
	
if __name__ == '__main__':
	main()
	
	
	
	
	