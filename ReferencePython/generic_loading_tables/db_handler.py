import pyodbc
import subprocess as sproc
import sys
import time
import os
import traceback

class DB_Handler(object):
	database = None;
	connection = None;
	creds = None;
	sql_command = None;
	scriptdir = None;
	loaddir = None;
	
	def __init__ (self, database, sql_command):
		self.database = database
		self.sql_command = sql_command
		self.scriptdir = 'c:/users/xls/src/scripts/ReferencePython/generic_loading_tables/'
		self.loaddir = os.environ.get('XLSDATA') + '/'
		
	"""
	Returns a registry value
	serverType = Application/Data/File Servers
	server = the registry key ex: c6
	val = the registry value ex: user
	"""
	def get_registry_value(self, serverType, server, val):
		try:
			val = sproc.check_output("regtool get '/machine/software/xls/" + serverType + "/" + server + "/{0}'".format(val), shell=True)
		except sproc.CalledProcessError:
			traceback.print_exc()
			print('subprocess.CalledProcessError: Invalid server directory - ' + serverType + '/' + server + '/' + val)
			
		return val.decode('utf-8').strip()
		
	def get_credentials(self, serverType):
		creds = {}
		try:
			creds["server"] = self.get_registry_value(serverType, self.database, "Server")
			creds["user"] = self.get_registry_value(serverType, self.database, "User")
			creds["password"] = self.get_registry_value(serverType, self.database, "Password")
		except FileNotFoundError:
			traceback.print_exc()
			print('subprocess.CalledProcessError: Invalid server directory - check database|client|registry names.\n')
		self.creds = creds
		return creds
	
	
	def open_connection(self, creds):
		try:
			connection = pyodbc.connect('DRIVER=SQL Server;SERVER={0};DATABASE={1};UID={2};PWD={3};'.format(creds['server'], self.database, creds['user'], creds['password']), autocommit=True)
		except FileNotFoundError:
			traceback.print_exc()
			self.logging.error('FileNotFoundError: Invalid database name provided.')
		return connection
		
	def set_connection(self, connection):
		if self.connection is not None:
			self.connection.close()
		self.connection = connection
    
	def close_connection(self):
		if self.connection is not None:
			self.connection.close()
		
	def exec_query(self, query):
		cursor = self.connection.cursor()
		cursor.execute(query)
		self.connection.commit()
		cursor.close()
    
	def exec_command(self):
		cursor = self.connection.cursor()
		cursor.execute(self.sql_command)
		self.connection.commit()
		cursor.close()
		
	def check_table_exists(self, table):
		cursor = self.connection.cursor()
		cursor.execute("SELECT COUNT(*) FROM sys.tables WHERE name = '" + table + "'")
		if cursor.fetchone()[0] == 1:
			cursor.close()
			return True
		
		cursor.close()
		return False
		
	def drop_table(self, table):
		cursor = self.connection.cursor()
		query = "DROP TABLE " + table
		cursor.execute(query)
		self.connection.commit()
		cursor.close()
    
	def rename_table(self, old_name, new_name):
		cursor = self.connection.cursor()
		query = "EXEC sp_rename '" + old_name + "', '" + new_name + "';"
		cursor.execute(query)	
		self.connection.commit()
		cursor.close()