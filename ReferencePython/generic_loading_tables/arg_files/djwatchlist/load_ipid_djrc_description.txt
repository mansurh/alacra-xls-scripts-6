origin|select distinct DescriptionsDJW.id,  
master.dbo.anglicize(replace(Description1ListDJW.name, '|', ' ')),  
master.dbo.anglicize(replace(Description2ListDJW.name, '|', ' ')), 
master.dbo.anglicize(replace(Description3ListDJW.name, '|', ' ')) 
from DescriptionsDJW 
left join Description1ListDJW on DescriptionsDJW.description1 = Description1ListDJW.id 
left join Description2ListDJW on DescriptionsDJW.description2 = Description2ListDJW.id 
left join Description3ListDJW on DescriptionsDJW.description3 = Description3ListDJW.id 
where DescriptionsDJW.id in 
(
select id from NameDetailsDJW where idType=2 and nameType='Primary Name'
union 
select id from NameDetailsSOC where idType=2 and nameType='1'
)