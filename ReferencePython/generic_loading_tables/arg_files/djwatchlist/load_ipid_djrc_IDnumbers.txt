origin|select distinct id, idType, idNumberType, idNumberCode 
from IDNumbersTypeDJW       
where idType=2 and idNumberType is not null 
union 
select distinct id, idType, itls.value,idNumberCode 
from IDNumbersTypeSOC 
join IdentificationTypeListSOC itls on itls.IdentificationTypeID = idNumberType  
where itls.IdentificationTypeID is not null 