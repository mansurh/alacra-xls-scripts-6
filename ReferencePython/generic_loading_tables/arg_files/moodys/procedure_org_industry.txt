exec moodys_ipid_table_proc
----------------------------
  

USE [concordance]
GO
/****** Object:  StoredProcedure [dbo].[moodys_ipid_table_proc]    Script Date: 12/13/2016 18:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[moodys_ipid_table_proc]
AS        
BEGIN        
    
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[moodys_ipid_table_python]') AND type in (N'U'))    
DROP TABLE [dbo].[moodys_ipid_table_python]        
  
create table moodys_ipid_table_python (  
matchkey varchar(12),  
ticker varchar(255),  
name varchar(255),  
country varchar(51),  
broad_business_line varchar(51),  
specific_business_line varchar(51),  
specific_SIC varchar(12),  
specific_SIC_text varchar(51),  
broad_SIC varchar(11),  
broad_SIC_text varchar(51),  
industry varchar(51)  
)  
  
  
insert into moodys_ipid_table_python(matchkey)  
select distinct Organization_ID  
from CFG_moodysload_organization_rating_root 
  
  
update moodys_ipid_table_python  
set ticker = CFG_moodysload_organization_identifiers.ID_Type_Text 
from CFG_moodysload_organization_identifiers  
where CFG_moodysload_organization_identifiers.Organization_ID = matchkey   
and CFG_moodysload_organization_identifiers.ID_Type_Text = 'Exchange Ticker'  
  
  
update moodys_ipid_table_python  
set name = CFG_moodysload_organization_rating_root.Moodys_Legal_Name  
from CFG_moodysload_organization_rating_root 
where CFG_moodysload_organization_rating_root.Organization_ID = matchkey  
  
  
update moodys_ipid_table_python  
set country = CFG_moodysload_organization_rating_root.Domicile_Country_Name  
from CFG_moodysload_organization_rating_root  
where CFG_moodysload_organization_rating_root.Organization_ID = matchkey  
  
  
update moodys_ipid_table_python  
set broad_business_line = CFG_moodysload_organization_market.Broad_Business_Line_Text  
from CFG_moodysload_organization_market  
where CFG_moodysload_organization_market.Organization_ID = matchkey  
  
  
update moodys_ipid_table_python  
set specific_business_line = CFG_moodysload_organization_market.Specific_Business_Line_Code  
from CFG_moodysload_organization_market  
where CFG_moodysload_organization_market.Organization_ID = matchkey  
  
  
update moodys_ipid_table_python  
set specific_SIC = CFG_moodysload_organization_market.Specific_SIC_Industry_Code  
from CFG_moodysload_organization_market  
where CFG_moodysload_organization_market.Organization_ID = matchkey  
  
  
update moodys_ipid_table_python  
set specific_SIC_text = CFG_moodysload_organization_market.Specific_SIC_Industry_Text  
from CFG_moodysload_organization_market  
where CFG_moodysload_organization_market.Organization_ID = matchkey  
  
  
update moodys_ipid_table_python  
set broad_SIC = CFG_moodysload_organization_market.Broad_SIC_Industry_Code  
from CFG_moodysload_organization_market  
where CFG_moodysload_organization_market.Organization_ID = matchkey  
  
  
update moodys_ipid_table_python  
set broad_SIC_text = CFG_moodysload_organization_market.Broad_SIC_Industry_Text  
from CFG_moodysload_organization_market  
where CFG_moodysload_organization_market.Organization_ID = matchkey  
  
  
update moodys_ipid_table_python  
set industry = CFG_moodysload_organization_market.Diversity_SIC_Text  
from CFG_moodysload_organization_market  
where CFG_moodysload_organization_market.Organization_ID = matchkey  
  
  
  
  
  
END 