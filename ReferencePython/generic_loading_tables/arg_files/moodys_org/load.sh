client_dir="moodys_org"
origin="concordance"
destination="concordance"
dest_server=""
origin_server=""
max_dev="--max_dev 20"
stage="--stage prod"

if [ $# -gt 0 ]
then
  client_dir=$1 
fi
if [ $# -gt 1 ]
then
  origin=$2
fi
if [ $# -gt 2 ]
then
  destination=$3 
fi
if [ $# -gt 3 ]
then
  dest_server="--dest_server $4"
server="$4"
fi
if [ $# -gt 4 ]
then
  origin_server="--origin_server $5"
fi
if [ $# -gt 5 ]
then
  max_dev="--max_dev $6"
fi
if [ $# -gt 6 ]
then
  stage="--stage $7"
fi

loaddir="$XLSDATA/${client_dir}"
logfile="${loaddir}/sh_log.log"




mkdir -p $XLSDATA/${client_dir}
rm -f  ${loaddir}/sh_log.log


main()
{
# update the dba
echo "$XLS/src/scripts/loading/dba/startupdate.sh moodys ${server} moodys_famtree"
$XLS/src/scripts/loading/dba/startupdate.sh moodys ${server} moodys_famtree
if [ $? != 0 ]; then echo "Error in startupdate"; exit 1; fi



echo "python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/ipid_loader.py ${client_dir} ${origin} ${destination} ${dest_server} ${origin_server} ${max_dev} ${stage}"
python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/ipid_loader.py ${client_dir} ${origin} ${destination} ${dest_server} ${origin_server} ${max_dev} ${stage}


echo "$XLS/src/scripts/loading/dba/endupdate.sh moodys ${server} moodys_famtree"
$XLS/src/scripts/loading/dba/endupdate.sh moodys ${server} moodys_famtree
if [ $? != 0 ]; then echo "Error in endupdate"; exit 1; fi


}
(main) > ${logfile} 2>&1
if [ $? -ne 0 ] 
then
 
    	blat ${logfile} -t "daniel.lundergreen@alacra.com, administrators@alacra.com" -f "reference-data-feed@alacra.com" -s "Moodys loading Delivery Failed "
    exit 1
fi
exit 0



