exec moodys_bond_desc_python_proc
---------------------------------
CREATE PROCEDURE moodys_bond_desc_python_proc
AS
BEGIN

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[moodys_bond_desc_python_table]') AND type in (N'U'))
DROP TABLE [dbo].[moodys_bond_desc_python_table]

create table moodys_bond_desc_python_table (
moodys_number varchar(20),
moodys_debt_number varchar(20),
cusip varchar(20),
isin varchar(20),
common_code varchar(9),
org_name varchar(255),
org_prev_name varchar(100),
sale_date varchar(50),
security_description varchar(1024),
security_debt_type varchar(4096),
coupon_type varchar(40),
coupon_freq varchar(40),
original_face_amount varchar(50),
currency varchar(20),
support_ind char(1),
support_entity varchar(255),
support_type varchar(256),
market_issuance varchar(1024),
private_placement varchar(4096),
issuer_takedown_ind char(20),
underwriter varchar(255),
credit_linked_ind char(20),
defeasance varchar(20),
asset_class varchar(1024),
reserved1 varchar(20),
reserved2 varchar(20),
reserved3 varchar(20),
reserved4 varchar(20),
reserved5 varchar(20),
reserved6 varchar(20)
)


insert into moodys_bond_desc_python_table(moodys_debt_number, org_name)
select  Instrument_ID, left(Moodys_Legal_name, 100)
from CFG_moodysload_instrument_organization
where Organization_Role_Code=129

update moodys_bond_desc_python_table
set moodys_number = CASE
when r.Moodys_Rating_ID > -1
THEN replicate('0',10-len(r.Moodys_Rating_ID))+convert(varchar,r.Moodys_Rating_ID)
ELSE r.Moodys_Rating_ID
END
FROM CFG_moodysload_instrument_ratings r
where r.Instrument_ID = moodys_debt_number

update moodys_bond_desc_python_table
set cusip=ident.Instrument_ID_Value
from CFG_moodysload_instrument_identifiers ident
where ident.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number
and ID_Type_Text='CUSIP'

update moodys_bond_desc_python_table
set isin=ident.Instrument_ID_Value
from CFG_moodysload_instrument_identifiers ident
where ident.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number
and ID_Type_Text='ISIN'

--common code no longer exisits in RDS 2.0

update moodys_bond_desc_python_table
set sale_date=rr.Sale_Date
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set security_description=rr.Security_Description
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set security_debt_type=rr.Class_Text
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set coupon_type=rr.Coupon_Type_text
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set coupon_freq= rr.Coupon_Frequency_Text
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set original_face_amount= cast(rr.Face_Amount as float)
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set currency=rr.ISO_Currency_Code
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set support_entity=sup.Moodys_Legal_Name
from CFG_moodysload_instrument_support sup
where sup.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set support_type=sup.Support_Type
from CFG_moodysload_instrument_support sup
where sup.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set market_issuance=rr.Market_Description
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set private_placement=rr.Private_Placement_Text
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set issuer_takedown_ind=rr.Takedown_Indicator
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set underwriter=org.Moodys_Legal_Name
from CFG_moodysload_instrument_organization org
where org.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number
and org.Organization_Role_Code=141

update moodys_bond_desc_python_table
set credit_linked_ind=rr.Credit_Linked_Indicator
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set defeasance=r.Rating_Reason_Code
from CFG_moodysload_instrument_ratings r
where r.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set asset_class=rr.Product_Line_Description
from CFG_moodysload_instrument_rating_root rr
where rr.Instrument_ID=moodys_bond_desc_python_table.moodys_debt_number

update moodys_bond_desc_python_table
set support_ind = case when support_entity is null then 'N' else 'Y' end

update moodys_bond_desc_python_table
set moodys_debt_number = replicate('0',10-len(convert(int,moodys_debt_number)-10000000))+convert(varchar,convert(int,moodys_debt_number)-10000000)
where moodys_debt_number < 800000000 and moodys_debt_number > -1

update moodys_bond_desc_python_table
set moodys_debt_number = replicate('0',10-len(moodys_debt_number))+convert(varchar,moodys_debt_number)
where moodys_debt_number > -1

update moodys_bond_desc_python_table
set moodys_number = replicate('0',10-len(moodys_number))+convert(varchar,moodys_number)
where moodys_number > -1

END
