exec moodys_bond_desc_istakedown_python_proc
------------------------------------------------------
CREATE PROCEDURE moodys_bond_desc_istakedown_python_proc 
AS          
BEGIN          
      
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[moodys_bond_desc_istakedown_python_table]') AND type in (N'U'))      
DROP TABLE [dbo].[moodys_bond_desc_istakedown_python_table]          
    
create table moodys_bond_desc_istakedown_python_table (    
moody_id int,
is_take_down int
)    

insert into moodys_bond_desc_istakedown_python_table
select org.Organization_ID, NULL
from CFG_moodysload_instrument_organization org
inner join CFG_moodysload_instrument_rating_root rr 
on org.Instrument_ID=rr.Instrument_ID
where rr.Takedown_Indicator<>'N'
and org.Organization_Role_Code=129

END