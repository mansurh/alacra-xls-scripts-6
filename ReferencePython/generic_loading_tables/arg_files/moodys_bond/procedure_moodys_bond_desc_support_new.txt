exec moodys_bond_desc_support_python_proc
---------------------------
CREATE PROCEDURE moodys_bond_desc_support_python_proc  
AS          
BEGIN          
      
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[moodys_bond_desc_support_python_table]') AND type in (N'U'))      
DROP TABLE [dbo].[moodys_bond_desc_support_python_table]        
    
create table moodys_bond_desc_support_python_table (    
moody_id int,
description	varchar(255),
support	varchar(255)
)  

insert into moodys_bond_desc_support_python_table
select distinct org.Organization_ID, sup.Moodys_Legal_Name, sup.Support_Type
from CFG_moodysload_instrument_organization org
inner join CFG_moodysload_instrument_support sup 
on org.Instrument_ID=sup.Instrument_ID
where org.Organization_Role_Code=129

END