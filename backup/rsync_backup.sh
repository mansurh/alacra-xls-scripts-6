if [ $# -lt 3 ]
then
	echo "usage: $0 backupserver srcdir destdir [options] "
	exit 1
fi

machine=$COMPUTERNAME
server=$1
srcdir=$2
destdir=$3
options=""
if [ $# -gt 3 ]
then
  options=$4
fi

main() {
  cd $srcdir
  if [ $? -ne 0 ]
  then
    echo "error cd to $srcdir for rsync, exiting "
    return 1
  fi
  ls -l
  date
  echo "Starting syncing rsync  -v -rlt ${options} `pwd` ${server}::${machine}${destdir}/"
  #ALWAYS sync the current directory.  Make sure the script is called from the correct location
  rsync  -v -rlt ${options} "." "${server}::${machine}${destdir}/"
  if [ $? -ne 0 ]
  then
    date
    echo "error in rsync, exiting "
    return 1
  fi
  date
}

if [ "$srcdir" == "." ]
then
  srcdir=`pwd`
fi

mkdir -p $XLSDATA/backup
cd $XLSDATA/backup
if [ $? -ne 0 ]
then
	echo "error in cd to $XLSDATA/backup"
	exit 1
fi
logfile=rsync`date +%y%m%d%H%M`.log
main > $logfile 2>&1
if [ $? -ne 0 ]
then
  blat -body "Check log file at http://$COMPUTERNAME/xlsdata/backup/$logfile for more information" -t operations@alacra.com -s "Rsync failed on $machine"
  exit 1
fi
