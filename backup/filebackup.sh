TAPENAME=$1

# Backup command file name
TEMPFILE=temp.tmp
rm -f $TEMPFILE
COMMANDFILE=command.tmp
rm -f $COMMANDFILE

# The server containing the dba database
DBASERVER=`$XLS/src/scripts/utility/dbserver.sh dba`
DBAUSER=dba
DBAPASSWORD=dba

isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} -n >${TEMPFILE} <<EOF

set nocount on
go

declare @directory varchar(255)

DECLARE file_cursor SCROLL CURSOR for
select d.directory from filetapedirectory d, filetape t
where
t.name = "${TAPENAME}"
and
d.filetape = t.id

OPEN file_cursor

fetch next from file_cursor into @directory 
while (@@fetch_status = 0)
begin
	PRINT @directory
	fetch next from file_cursor into @directory
end

CLOSE file_cursor
DEALLOCATE file_cursor

go

EOF

if [ $? != 0 ]
then
	echo "Error building backup temp file"
	exit 1
fi

# Build the command file
echo "echo \"${TAPENAME} backup started\" > logfile" >> ${COMMANDFILE}
echo "date >> logfile" >> ${COMMANDFILE}

echo "ntbackup backup \c" >> ${COMMANDFILE}

# Insert the directory names
for a in `cat ${TEMPFILE}`
do
	printf "\"%s\" " ${a} >> ${COMMANDFILE}
done

echo " /hc:on /t normal /l \"logfile\" /tape:0" >> ${COMMANDFILE}

echo "echo \"${TAPENAME} backup stopped\" >> logfile" >> ${COMMANDFILE}
echo "date >> logfile" >> ${COMMANDFILE}

echo "blat logfile -t backupreport@xls.com -s \"${TAPENAME} backup log\"" >> ${COMMANDFILE}

# Execute the command file
sh ${COMMANDFILE}
if [ $? != 0 ]
then
	echo "Error executing backup command file"
	exit 1
fi

# Remove the temporary and command file
# rm -f ${TEMPFILE}
# rm -f ${COMMANDFILE}

# Update the dba database with the last backup date for the tape
isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} -n << EOF

set nocount on
go

exec logFileBackup "${TAPENAME}"
go

EOF
if [ $? != 0 ]
then
	echo "Error updating backup status in DBA database"
	exit 1
fi
