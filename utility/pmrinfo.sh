shname=pmrinfo.sh
if [ $# -lt 4 ]
then
    echo "Usage: ${shname} srcserver srclogin srcpasswd packagename"
    exit 1
fi

srcserver=${1}
srcuser=${2}
srcpasswd=${3}
packagename=${4}



mysql="select 'PMR ' + STR(id,4,0) + ': ' + short_desc from pmr where package='${packagename}'"


isql /Q "${mysql}" /S ${srcserver} /U ${srcuser} /P ${srcpasswd} -b -n -h-1 -w4096 | grep -v "^(" | sed -e "s/ PMR/PMR/g" | grep -v "^$" | sed -e "s/   //g"
if [ $? != 0 ]
then
    echo "${shname}: Error in isql against server ${srcserver}"
    exit 1
fi

exit 0
