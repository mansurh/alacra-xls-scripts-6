#!/bin/ksh

# take in server name whose databases need to be flipped to their backups.
export server=$1

# isql requires:
ISQL='isql /Usa /Pnewpass /Sdata2'

$ISQL << EOF

use jsched
go

update dataserver 
set dataserver.server = b.flipped_server,
    dataserver.backup = b.server,
    dataserver.flipped_server = b.backup
from dataserver, dataserver b
where dataserver.flipped_server = '$server'
and dataserver.dbname = b.dbname
go

EOF
