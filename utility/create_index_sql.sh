# Generic Script to create indexes sql from SQL Server's table
# This assumes that there is a stored procedure "get_info_info" (look in concordance), which extracts index info from database
# 1 = Server
# 2 = Login
# 3 = Password
# 4 = Table Name
# 5 = Output file
ARGS=5
if [ $# -lt $ARGS ]
then
	echo "Usage: $0 server login password table_name output_file"
	exit 1
fi

server=$1
login=$2
password=$3
tabname=$4
filename=$5

TMPFILENAME="${tabname}_index_info.tmp"
rm -f ${TMPFILENAME}
rm -f ${filename}

bcp "exec get_index_info '${tabname}'" queryout ${TMPFILENAME} /S${server} /U${login} /P${password} /c /t"|" 
if [ $? -ne 0 ]
then
	echo "error running get_index_info '${tabname}' query against ${server}, exiting ..."
	exit 1
fi

previndid="0"
close=""
echo "" > ${filename}
IFS="|"
while read indid indname indcol keyno
do
	clustered=""
	if [ $indid == "1" ]
	then
	cluster="clustered"
	fi

	if [ $previndid == $indid ]
	then
		echo ", [$indcol]" >> ${filename}
	else
		echo "${close} create ${clustered} index [${indname}] on [${tabname}_new] ([${indcol}]" >> ${filename}
	fi
	close=") "
	previndid=$indid
done < ${TMPFILENAME}

echo ${close} >> ${filename}
rm -f ${TMPFILENAME}
