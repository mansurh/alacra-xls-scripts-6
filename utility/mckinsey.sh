#!/bin/sh

# Script to auto-load mckinsey-UK logins

prefix="mck-"
firstid=15000
account=1747
commence="Dec 1,2000"
terminate="Dec 2, 2001"

id=${firstid}

IFS='	'


# Read each record of the text file
while read password first_name last_name login ho so
do
	# Add the prefix to the login
	login="${prefix}${login}"

	# Remove any spaces from the login
	login=`sed -e "s/ //g;s/'/ /g" << HERE
${login}
HERE`

	# Get the next userid
	id=`expr ${id} + 1`

	# Output the SQL command
	echo "insert into users (id,account,login,password,last_name,first_name,company, address1, city, country, postal_code,commence,terminate,service,require_project,salesrep, billing_reference) values \
	(${id},${account},\"${login}\",\"${password}\",\"${last_name}\",\"${first_name}\",\"McKinsey & Co.\",\"No. 1 Jermyn Street\",\"London\",230,\"SW1Y 4UH\",\"${commence}\",\"${terminate}\",28,\"y\",74,\"auto inserted id\")"
done
