XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/check_return_code.fn

check_arguments $# 2 "Usage: split_report.sh file outputpath" 1

file=$1
outputpath=$2

if [ ! -s ${file} ]
then

    echo "Error: file ${file} not found or is empty"
	exit 1

fi

extension=$(echo ${file} | sed -e "s/.*\.//" | tr '[:upper:]' '[:lower:]')

if [ ${extension} = "" ] || [ ${extension} = ${file} ]
then

    echo "Error: no filename extension found"
	exit 1

fi

file_out="${outputpath}$(echo ${file} | sed -e "s/${extension}$/kw.txt/")"
# echo "file_out: ${file_out}"
rm -f ${file_out}

# doc
# MHT
# ppt
# rtf
# xls
# xml

if [ ${extension} = "pdf" ]
then

    $XLS/src/scripts/utility/splitpdf1.sh ${file} ${outputpath}
    check_return_code $? "Error splitting pdf" 1

elif [ ${extension} = "txt" ] || [ ${extension} = "dat" ]
then

    cp ${file} ${file_out}
    check_return_code $? "Error creating text output" 1

elif [ ${extension} = "htm" ] || [ ${extension} = "html" ]
then

    lynx -dump ${file} > ${file_out}
    check_return_code $? "Error extracting contents of html file" 1

elif [ ${extension} = "doc" ] || [ ${extension} = "rtf" ] || [ ${extension} = "ppt" ]
then

    catdoc ${file}
    check_return_code $? "Error creating text output" 1

else

    echo "Error, file type \"${extension}\" not supported"
	exit 1

fi

exit 0
