# Copy Data from the usage,pbusage and shoppingcartlog tables.  This job will
# be running weekly to refresh the database on Data10 with activity from Alaxls
# The data extracted and inserted is getting data from Current Date - 8 days, 
# just to be safe. The data on the recepient side is first deleted, to eliminat
# Any duplicates.  The last step is executing a stored procedure against alaxls
# to delete data that is older than one year
#


ORIGIN_USER=$1
ORIGIN_PASSWD=$2
ORIGIN_SERVER=$3

DEST_USER=$4
DEST_PASSWD=$5
DEST_SERVER=$6
DEST_DB=$7

WORKDIR=f:/users/default/alaxls_usage
LOGFILE=`basename $0`.log
MAILFILE=`basename $0`.mail
BATCH_IN=10000
#--------------
extract_data()
{
bcp  "select * from usage where access_time >convert (char(11), dateadd  (dd,-8,getdate()),106)" queryout data/usage.dat -U $ORIGIN_USER -P $ORIGIN_PASSWD -S $ORIGIN_SERVER -c -t"\001" -r"\002" >> $LOGFILE
#-----
bcp  "select * from pbusage where pbendtime > convert (char(11),dateadd (dd,-8,getdate()),106)" queryout data/pbusage.dat -U $ORIGIN_USER -P $ORIGIN_PASSWD -S $ORIGIN_SERVER -c -t"\001" -r"\002" >> $LOGFILE
#-----
bcp  "select * from shoppingcartlog where dateentered > convert (char(11), dateadd (dd,-8,getdate()),106)" queryout data/shoppingcartlog.dat -U $ORIGIN_USER -P $ORIGIN_PASSWD -S $ORIGIN_SERVER  -c -t"\001" -r"\002" >> $LOGFILE
}
##################################
eliminate_dups()
{
isql -U $DEST_USER -P $DEST_PASSWD -S $DEST_SERVER << EOF >> $LOGFILE
delete from usage where access_time > convert (char(11),dateadd (dd,-8,getdate()),106)
go
delete from pbusage where pbendtime >  convert (char(11),dateadd (dd,-8,getdate()),106)
go
delete from shoppingcartlog where dateentered > convert (char(11),dateadd (dd,-8,getdate()),106)
go
EOF
}
##################################
insert_data()
{
for table in usage pbusage shoppingcartlog
do
bcp  $DEST_DB..$table in data/${table}.dat -U $DEST_USER -P $DEST_PASSWD -S $DEST_SERVER -b $BATCH_IN  -c -t"\001" -r"\002" >> $LOGFILE
done
isql -U $DEST_USER -P $DEST_PASSWD -S $DEST_SERVER << EOF >> $LOGFILE
checkpoint
go
dbcc shrinkfile (xls_usage_log)
go
exit
EOF
}
eliminate_aged_rows_from_source()
{
isql -U $ORIGIN_USER -P $ORIGIN_PASSWD -S $ORIGIN_SERVER << EOF >> $LOGFILE
exec PurgePrepbookTables
go
checkpoint
go
dbcc shrinkfile (xls_log)
go
EOF
}
mail_results()
{
cat $LOGFILE | grep -v sent | grep -v successfully > $MAILFILE
blat $LOGFILE -t "administrators@xls.com" -s "Backup and Clear Usage tables from `basename $0` on `hostname` "

}
cd $WORKDIR
extract_data
eliminate_dups
insert_data
eliminate_aged_rows_from_source
mail_results
