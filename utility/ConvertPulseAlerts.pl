use Win32::OLE;
use OLE;

unless (@ARGV == 4) {
	die("usage: ConvertPulseAlerts DBSERVER DATABASE USER PASSWORD");
}
$SERVER=$ARGV[0];
$DATABASE=$ARGV[1];;
$USER=$ARGV[2];
$PASSWORD=$ARGV[3];

$fieldNum=2;
$PRI_BATCH=5000;

$QUERY = "select distinct u.alacrauser_id, u.lwid from users u where u.alacrauser_id IN (143317,145161,147747,174949,176826) and coalesce(lwid,'')!=''" ;


print ("$QUERY \n");
#exit 0;


#Create connection strings
my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!";
my $connect="DRIVER={SQL SERVER};PWD=$PASSWORD;UID=$USER;SERVER=$SERVER;DATABASE=$DATABASE;Timeout=1800";

#Connect
$conn-> {ConnectionTimeout} = 1800;
$conn-> {CommandTimeout} = 1800;
$conn->open($connect);

#Execute query
my $sql=$QUERY;

my $rs;
unless($rs=$conn->Execute($sql)){
  die("error executing the SQL statement");
}

$MAX_BATCH=48000;
$total_count=0;
$batch_count=0;
$pri_count=0;
#Get values
$myfldcnt = $rs->Fields->Count;
if ($myfldcnt eq $fieldNum ){
	while ( !$rs->EOF){
		$total_count++;
		$id =  $rs->Fields(0)->value;
		$lwid =  $rs->Fields(1)->value;
		#print("--ConvertPulseAlerts.exe $SERVER $DATABASE $USER $PASSWORD $id $lwid p ");
		system( "ConvertPulseAlerts.exe $SERVER $DATABASE $USER $PASSWORD $id $lwid p ");
		$rs->MoveNext;
	}
	print("--total records: $total_count\n");
} else {
	print ("number of fields is not equal to $fieldNum\n");
	exit 1;
}

$rs->Close;
$conn->Close;
