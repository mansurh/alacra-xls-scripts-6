# Routine to patch the opening tag of stylesheets

shname=xslexcludeprefixes.sh

# Parse the command line arguments
#	1 = Root Directory

if [ $# != 1 ]
then
	echo "Usage: ${shname} rootdir"
	exit 1
fi

# Save the runstring parameters in local variables
rcsroot=$1

tempname=temp.tmp


# Make sure the top level tree exists in the rcs tree
if [ ! -d ${rcsroot} ]
then
	echo "${shname}: ${rcsroot} is not a directory"
	exit 1
fi


# Do for all the files in the directory
for filename in `ls ${rcsroot}`
do
	# See if this is a regular file - shouldn't happen
	if [ ! -d ${rcsroot}/${filename} ]
	then
		thisfile=${rcsroot}/${filename}
		thisext=${thisfile##*.}

		case $thisext in
		xsl)		doit=1;;
		XSL)		doit=1;;
		Xsl)		doit=1;;
		xSl)		doit=1;;
		xsL)		doit=1;;
		XSl)		doit=1;;
		xSL)		doit=1;;
		XsL)		doit=1;;
		*)			doit=0;;
		esac

		if [ "${doit}" = "1" ]
		then
			if [ -e ${tempname} ]
			then
				rm -f ${tempname}
			fi
#			echo "awk -f $XLS/src/scripts/utility/xslss.awk ${thisfile} > ${tempname}"
#			echo "rm -f ${thisfile}"
#			echo "mv ${tempname} ${thisfile}"

			awk -f $XLS/src/scripts/utility/xslss.awk ${thisfile} > ${tempname}
			rm -f ${thisfile}
			mv ${tempname} ${thisfile}
		fi

	else
		# file is a directory, so recurse into it
		$XLS/src/scripts/utility/${shname} ${rcsroot}/${filename}
		if [ $? != 0 ]
		then
			exit 1
		fi
	fi

done

exit 0
