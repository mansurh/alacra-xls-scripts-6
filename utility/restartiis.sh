function stop_iis {
  net stop "World Wide Web Publishing Service"
}
function start_iis {
  net start "World Wide Web Publishing Service"
}

today=`date +%Y%m`
logfile="iisrestart${today}.log"
date >> $logfile
stop_iis >> $logfile 2>&1
#sleep 1
start_iis >> $logfile 2>&1
date >> $logfile
