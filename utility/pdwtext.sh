if [ $# -ne 1 ] && [ $# -ne 2 ]
then
  echo "usage: $0 filename [outputfile]"
  exit 1
fi
#output the text of the pdf without annotation in front of every line
if [ ! -e $1 ]
then
  echo "Error: file does not exist"
  exit 1
fi
pdw $1 | awk '{ i=index($0, ":"); if (i > 0) print substr($0, i+2); }'
exit 0
