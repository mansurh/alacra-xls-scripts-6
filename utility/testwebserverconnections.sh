logname=c:/temp/testwebserverconnections.out

ipconfig /all | grep -i "Host" > ${logname}

allwebservers="wwwa1.alacra.com wwwa2.alacra.com wwwa4.alacra.com wwwa5.alacra.com wwwa6.alacra.com wwwa7.alacra.com wwwp.alacra.com wwwg.alacra.com test2.alacra.com test6.alacra.com test4.alacrastore.com"

for server in $allwebservers
do
	web get "http://${server}" > nul 2>&1
	if [ $? != 0 ]
	then
		echo "Could not connect to ${server}!" >> ${logname}
	else
		echo "Successfully connected to ${server}" >> ${logname}
	fi
done

blat ${logname} -t "administrators@alacra.com" -f "administrators@alacra.com"

rm ${logname}

exit 0
