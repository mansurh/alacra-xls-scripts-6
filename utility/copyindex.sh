dataserver=$1
sourcepath=$2
database=$3
indexfilenames=$4

if [ $# -lt 4 ]
then
	echo "Usage: copyindex.sh dataserver sourcepath database indexfilenames "
	exit 1
fi

destpath=`reginvidx get ${database}`
scratch=${destpath%/*}
destpath=${scratch}

rm -f command.ftp

echo "user administrator newpass" > command.ftp

echo "binary" >> command.ftp
echo "get ${sourcepath}/${indexfilenames}.doc ${destpath}/${indexfilenames}.doc.sav" >> command.ftp
echo "get ${sourcepath}/${indexfilenames}.idx ${destpath}/${indexfilenames}.idx.sav" >> command.ftp
echo "get ${sourcepath}/${indexfilenames}.pst ${destpath}/${indexfilenames}.pst.sav" >> command.ftp

ftp -i -n -s:command.ftp ${dataserver}.xls.com

if [ $? != 0 ]
then
	echo "Error in ftp, Exiting ..."
	exit 1
fi

rm -f command.ftp

if [ ! -s ${destpath}/${indexfilenames}.doc.sav ]
then
	echo "${destpath}/${indexfilenames}.doc.sav is missing after ftp, Exiting .."
	exit 1
fi

if [ ! -s ${destpath}/${indexfilenames}.pst.sav ]
then
	echo "${destpath}/${indexfilenames}.pst.sav is missing after ftp, Exiting .."
	exit 1
fi

if [ ! -s ${destpath}/${indexfilenames}.idx.sav ]
then
	echo "${destpath}/${indexfilenames}.idx.sav is missing after ftp, Exiting .."
	exit 1
fi

#Backup old indices
mv ${destpath}/${indexfilenames}.idx ${destpath}/${indexfilenames}.idx.bak
mv ${destpath}/${indexfilenames}.doc ${destpath}/${indexfilenames}.doc.bak
mv ${destpath}/${indexfilenames}.pst ${destpath}/${indexfilenames}.pst.bak

#Move files into their proper names
mv ${destpath}/${indexfilenames}.idx.sav ${destpath}/${indexfilenames}.idx
mv ${destpath}/${indexfilenames}.doc.sav ${destpath}/${indexfilenames}.doc
mv ${destpath}/${indexfilenames}.pst.sav ${destpath}/${indexfilenames}.pst
