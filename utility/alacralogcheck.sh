XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Command file to update a custom alacra book on a web server
# Parse the command line arguments.
#   1 = server
shname=alacralogcheck.sh
if [ $# -lt 1 ]
then
	echo "Usage: ${shname} server"
	exit 1
fi

server=$1
user=`get_xls_registry_value alacralog User`
password=`get_xls_registry_value alacralog Password`

tempfile1=tempalacralog1.dat
if [ -e $tempfile1 ]
then
	rm -f $tempfile1
fi

isql /S ${server} /U ${user} /P ${password} /n /h-1 > ${tempfile1} << EOF
set nocount on
select distinct server
from alacralog 
where DATEDIFF(day,eventtime,GETDATE()) < 1
EOF

tempfile2=tempalacralog2.dat
if [ -e $tempfile2 ]
then
	rm -f $tempfile2
fi

echo "The following servers have posted rows to alacralog within the past day:" > $tempfile2

cat $tempfile1 | sed -e 's/ //g' | grep -v "^$" >> $tempfile2
rm $tempfile1


echo "" >> $tempfile2
echo "Note: We expect to see WWWA1, WWWA3, WWWA4, WWWA8-WWWA10." >> $tempfile2
echo "If any of these are missing, the Alacra Log Loader Service may need attention." >> $tempfile2

blat $tempfile2 -t "administrators@alacra.com" -f "administrators@alacra.com" -s "Daily Alacralog Check"
rm $tempfile2

exit 0
