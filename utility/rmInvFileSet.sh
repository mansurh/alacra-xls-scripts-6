# Parse the command line arguments.
#	1 = invfilesetname
#	2 = srcdir (optional)
#	3 = db name (optional, when != invfilesetname)
shname=rmInvFileSet.sh
if [ $# -lt 1 ]
then
	echo "Usage: ${shname} invfilesetname [srcdir] [dbname]"
	exit 1
fi

#sleep for sometime (to make sure GIS lets go of the inverted index files)
sleep 60

invfilesetname=$1
if [ $# -gt 1 ]
then
	srcdir=$2
else
	srcdir="."
fi

dbname=${invfilesetname}
if [ $# -gt 2 ]
then
	dbname=$3
fi

	# make certain the GIS closes the inverted fileset
	gisget.exe ${COMPUTERNAME} "topic=_lockfileset&regkey=${invfilesetname}"
	gisget.exe ${COMPUTERNAME} -p 4888  "topic=_lockfileset&regkey=${invfilesetname}"
	# tell GIS it's OK to use the index fileset (do this here, so if rebuild fails, we don't issue another lock command)
	gisget.exe ${COMPUTERNAME} "topic=_unlockfileset&regkey=${invfilesetname}"
	gisget.exe ${COMPUTERNAME} -p 4888 "topic=_unlockfileset&regkey=${invfilesetname}"



# other assorted detrius
rm -f ${srcdir}/__db.*
if [ $? -ne 0 ]
then
	#sleep for a bit, if delete failed.
	sleep 60s

	rm -f ${srcdir}/__db.*
	if [ $? -ne 0 ]
	then
		echo "$0: Error: Could not removed __db.*"
		exit 1
	fi
fi

rm -f ${srcdir}/*.vct ${srcdir}/__db_lock.share


# most filenames start with the inverted fileset name
biglist="\
${srcdir}/${invfilesetname}.doc \
${srcdir}/${invfilesetname}*.idx \
${srcdir}/${invfilesetname}*.pst \
${srcdir}/${invfilesetname}.del \
${srcdir}/${invfilesetname}*.ldb \
${srcdir}/${invfilesetname}.smi \
${srcdir}/${invfilesetname}*.kwc \
$(ls ${srcdir}/${invfilesetname}*.gpx.gz) \
$(ls ${srcdir}/${invfilesetname}*.smx.gz) \
$(ls ${srcdir}/${invfilesetname}*.gpx) \
$(ls ${srcdir}/${invfilesetname}*.smx)
"

# except sitemap files which don't
if [ "${invfilesetname}" != "${dbname}" ]
then
	biglist2="\
$(ls ${srcdir}/${dbname}_${invfilesetname}*.gpx.gz) \
$(ls ${srcdir}/${dbname}_${invfilesetname}*.smx.gz) \
$(ls ${srcdir}/${dbname}_${invfilesetname}*.gpx) \
$(ls ${srcdir}/${dbname}_${invfilesetname}*.smx)
"

	if [ "${biglist2}" != "" ]
	then
		biglist="${biglist} ${biglist2}"
	fi
fi

#echo "${biglist}"


# Delete whatever is in there (don't touch the .cfg file!)
for i in $biglist
do
	if [ -e ${i} ]
	then
		echo "Removing ${i}"
		rm -f ${i}
		if [ $? != 0 ]
		then
			echo "${shname}: Error: Could not remove ${i}"
			exit 1
		fi
	fi
done

if [ -e $XLS/src/cfgfiles/${invfilesetname}.cfg ]
then
	echo "${shname}: copying $XLS/src/cfgfiles/${invfilesetname}.cfg to ${srcdir}/${invfilesetname}.cfg"
	rm -f ${srcdir}/${invfilesetname}.cfg
	cp -p $XLS/src/cfgfiles/${invfilesetname}.cfg ${srcdir}/${invfilesetname}.cfg
	if [ $? != 0 ]
	then
		echo "${shname}: Error: Could not copy $XLS/src/cfgfiles/${invfilesetname}.cfg to ${srcdir}/${invfilesetname}.cfg"
		exit 1
	fi
fi


exit 0
