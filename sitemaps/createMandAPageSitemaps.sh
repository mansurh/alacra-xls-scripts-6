XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

if [ $# -lt 2 ]
then
	echo "usage: $0 xlsserver database"
	exit 1
fi

xlsserver=$1
xlsdatabase=$2

xlsuser=`get_xls_registry_value ${xlsdatabase} User`
xlspass=`get_xls_registry_value ${xlsdatabase} Password`

database=sitemapdb
siteserver=`get_xls_registry_value ${database} Server`
siteuser=`get_xls_registry_value ${database} User`
sitepass=`get_xls_registry_value ${database} Password`

run() {
#  $XLS/src/scripts/loading/dba/startupdate.sh search ${COMPUTERNAME} search
#  check_return_code $? "error in startupdate.sh, exiting ..." $?

    # where is the index located?
  currentloc=`reginvidx get deals`

  scratch=${currentloc%/*}
  indexdir=${scratch}

  cd $indexdir
  mkdir -p new
  cd new
  check_return_code $? "error in cd to ${indexdir}/new, exiting ..." $?

  rm -f *smx*
  rm -f sitemap.sql


  perl $XLS/src/scripts/sitemaps/createMandAPageSitemaps.pl ${xlsserver} ${xlsdatabase} ${xlsuser} ${xlspass} $COMPUTERNAME
  check_return_code $? "error in perl createMandAPageSitemaps.pl, exiting ..." $?

  #sanity check (make sure you have at least 2 files for public companies)
  if [ `ls *smx* |wc -l` -lt 2 ]
  then
	echo "createMandAPageSitemaps.pl created less than 2 sitemaps, exiting ..."
	exit 1
  fi

  test -s sitemap.sql
  check_return_code $? "sitemap.sql is empty, exiting ..." $?

  isql /S${siteserver} /U${siteuser} /P${sitepass} /b < sitemap.sql
  check_return_code $? "error running sitemap.sql for MandAPage, exiting ..." $?

  rm -f sitemap.sql
  
  #remove sitemap index files 
  rm -f ${indexdir}/search_deals*_[0-9][0-9][0-9][0-9].smx*
  mv search_deals_[0-9][0-9][0-9][0-9].smx* ../
  check_return_code $? "error moving mapages sitemaps to $indexdir, exiting ..." $?
 
#  $XLS/src/scripts/loading/dba/endupdate.sh search ${COMPUTERNAME} search
  echo "complete"

}

if [ ! -d $XLSDATA/index ]
then
	mkdir -p $XLSDATA/index
fi
logfilename=$XLSDATA/index/MandAPageSitemap`date +%y%m%d`.log
`run > ${logfilename} 2>&1`
returncode=$?
if [ $returncode -ne 0 ]
then
	blat ${logfilename} -t administrators@alacra.com -s "MandAPage Sitemapindex generation failed"
	exit ${returncode}
fi
