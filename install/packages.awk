BEGIN {
	IFS=" ";
}
{
	if (NF >= 9)
	{
		len = length( $NF );
		if (len > 9)
		{
			if ( index($NF, "build") == 1 )
			{
				if (substr($NF, len - 2, 3) == ".sh")
				{
					pkgname=substr($NF, 6, len - 8);

					lastch=substr(pkgname,length(pkgname),1);
					if (index("0123456789", lastch) > 0)
					{
						printf "    %s (", pkgname ;

						i=index($(NF-1), ":");
						if (i > 0)
						{
							print $(NF-3), $(NF-2) ")"
						}
						else
						{
							print $(NF-3), $(NF-2), $(NF-1) ")"
						}
					}
				}
			}
		}
	}
}
