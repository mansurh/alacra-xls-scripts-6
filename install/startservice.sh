if [ "$XLSUTILS" == "" ]
then
	XLSUTILS=$XLS/src/scripts/loading/dba
fi

. $XLSUTILS/get_registry_value.fn

shname=$0

# Parse the command line arguments.
#	1 = servicecode
#	2 = checkerror

if [ $# -lt 1 ]
then
	echo "Usage: ${shname} servicecode [checkerror]"
	exit 1
fi

# Save the runstring parameters in local variables
servicecode=$1
if [ $# -gt 1 ]
then
	checkerror=$2
else
	checkerror=0
fi


startservice()
{
	if [ $# -lt 2 ]
	then
		return 1
	fi
	sname=$1
	errcheck=$2

	echo "Restarting ${sname}"
	net start "${sname}"
	if [ $? != 0 ]
	then
		echo "Error restarting ${sname}!"
		if [ $errcheck != 0 ]
		then
			exit 1
		fi
	fi
}


if [ "${servicecode}" = "availability" ]
then
	if [ "$COMPUTERNAME" != "" ]
	then
			dba2server=`get_xls_registry_value dba2 "Server" "0"`
			echo "Marking $COMPUTERNAME online"
			isql /b /U dba2 /P dba2 /S ${dba2server} /Q "exec setServerStatus '$COMPUTERNAME', 'online'"
			if [ $? -ne 0 ]; then echo "error putting machine online"; exit 1; fi
	fi

	startservice "Alacra Server Availability Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "skvalidservice" ]
then
	startservice "XLS User Validation Services - Local DB" "${checkerror}"
	startservice "XLS User Validation Services" "${checkerror}"
	# dnb service has skvalidservice in its service dependency list
	startservice "Alacra Company DNB Service" 0
	exit 0
fi


if [ "${servicecode}" = "companyinfoservice" ]
then
	startservice "Alacra Company Information Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "dnb2service" ]
then
	startservice "Alacra Company DNB Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "apidatasource" ]
then
	startservice "Alacra APIDataSource" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "appserver" ]
then
	startservice "Alacra Application Server Update Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "alachartsservice" ]
then
	startservice "Alacra Charting Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "creditcardservice" ]
then
	startservice "Alacra Credit Card Authentication Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "encryptionservice" ]
then
	startservice "Alacra CreditCard Encryption Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "alacralogloader" ]
then
	startservice "Alacra Log Loading Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "dispatch" ]
then
	startservice "XLS Dispatch Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "unlinktmp" ]
then
	startservice "XLS Unlink Temp Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "www" ]
then
	startservice "w3svc" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "gas" ]
then
	dotnetinstalled=`get_registry_value "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v3.0" Install`
	dotnetinstalled2=`get_registry_value "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v3.0" Alacra`
	if [[ "${dotnetinstalled}" -eq "1" || "${dotnetinstalled2}" -eq "1" ]]
	then
#		startservice "AlacraGAS" "${checkerror}"
		startservice "Alacra Generic Application Server Service" "${checkerror}"
	else
		echo ".NET 3.0 not detected -- AlacraGAS not started"
	fi
	exit 0
fi


if [ "${servicecode}" = "autocomplete" ]
then
	startservice "autocompleteservice" "${checkerror}"
	exit 0
fi


echo "Unknown service code"
exit 1
