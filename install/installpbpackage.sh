# Command file to transfer and install a software update onto a packager server
# Usage: installpbpackage.sh packagename

# Parse the command line arguments.
#	1 = packagename
if [ $# -lt 1 ]
then
	echo "Usage: installpbpackage.sh packagename"
	exit 1
fi

# Save the runstring parameters in local variables
packagename=$1

# optional 'no undo' flag - means do not make undo tar file
if [ $# -gt 1 ]
then
	undoflag=$2
fi

# create unique log file name, based on date
logfilename=/install.${packagename}.log

echo "Installing Package ${packagename}" > ${logfilename}

# Perform the install
installpbpackagelow.sh ${packagename} ${undoflag} >> ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	echo "" >> ${logfilename}
	echo "Bad return code from installpbpackagelow.sh - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "administrators@xls.com" -s "Package ${packagename} install failed on $COMPUTERNAME"
else
	echo "" >> ${logfilename}
	echo "Success from installpbpackagelow.sh - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "administrators@xls.com" -s "Package ${packagename} successfully installed on $COMPUTERNAME"
fi

exit $returncode
