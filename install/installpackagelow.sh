XLSUTILS=c:/users/xls/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Command file to transfer and install a software update onto a web server
# Usage: installpackagelow.sh packagename

# Parse the command line arguments.
#	1 = packagename
#	2 = optional 'noundo' flag
if [ $# -lt 1 ]
then
	echo "Usage: installpackagelow.sh packagename [noundo]"
	exit 1
fi

# Set up vars
packagename=$1

# optional 'no undo' flag - means do not make undo tar file
if [ $# -gt 1 ]
then
	undoflag=$2
fi

server=`get_xls_registry_value installs Server`
if [ -z ${server} ]
then
	server="data11.alacra.com"
fi
logon=`get_xls_registry_value installs User`
password=`get_xls_registry_value installs Password`

basefilename=distribution.web.${packagename}


# work in root dir
cd c:/
if [ "$undoflag" != "noundo" ]
then
	if [ -e undo.${packagename}.tar.gz ]
	then
		echo "Error, undo package exists, exiting ..."
		exit 1
	fi
fi

rm -f ${basefilename}.*
ncftpget -u ${logon} -p ${password} -V -Z -t 60 ${server} . "/packagedir/${basefilename}.*"
# Test the exit status of ftp
if [ $? != 0 ]
then
	echo "Error in ftp, Exiting"
	exit 1
fi


# Verify that we got the data file
if [ ! -e ${basefilename}.tar.Z   -a   ! -e ${basefilename}.tar.gz  ]
then
	echo "Cannot find ${basefilename}.tar.Z or ${basefilename}.tar.gz, Exiting"
	exit 1
fi

if [ ! -s ${basefilename}.lst ]
then
	echo "${basefilename}.lst not Downloaded or Zero Length, Exiting"
	exit 1
fi


# Check if this package has a PRE-installation shell script, and if so, run it
if [ -s ${basefilename}.pre.sh ]
then
	echo "---- Running PRE installation script ${basefilename}.pre.sh ---------------------------"
	chmod u+x ${basefilename}.pre.sh
	${basefilename}.pre.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from ${basefilename}.pre.sh, Exiting"
		exit 1
	fi
	echo "---- Done w/PRE installation script ${basefilename}.pre.sh ---------------------------"
fi


if [ -s ${basefilename}.pre.ps1 ]
then
	echo "---- Running PRE installation script ${basefilename}.pre.ps1 ---------------------------"
	powershell ${basefilename}.pre.ps1 -pkgName ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from ${basefilename}.pre.ps1, Exiting"
		exit 1
	fi
	echo "---- Done w/PRE installation script ${basefilename}.pre.ps1 ---------------------------"
fi


# unpack package archive
if [ "$undoflag" = "noundo" ]
then
	# unpack with no undo file
	qunpack.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from qunpack.sh, Exiting"
		exit 1
	fi
else
	# create undo file, unpack
	unpack.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from unpack.sh, Exiting"
		exit 1
	fi
fi


# Check for problems with any of the files
distunpacklogfile=${basefilename}.log

echo "---- Checking Unpack log -------------------------------------------"
grep -i "cannot " ${distunpacklogfile}
if [ $? != 1 ]
then
	echo "Error unpacking the above file(s), will retry in 10 sec..."
#	echo "Setting write privilege on all files under docs and cgi-bin directories"
#	chmod -R +w /usr/netscape/server/docs/*
#	chmod -R +w /usr/netscape/server/cgi-bin/*
	sleep 10
	echo "Unpacking archive again..."

	qunpack.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from qunpack.sh, Exiting"
		exit 1
	fi

	echo ""
	echo "---- Re-checking Unpack log again ----------------------------------"

	grep -i "cannot " ${distunpacklogfile}
	if [ $? != 1 ]
	then
		echo "Error unpacking the above file(s), Exiting"
		exit 1
	fi
fi


echo "No problems extracting files.  Complete extract log follows:"
echo ""
cat ${distunpacklogfile}



# Check if we need to [re-]create any virtual directories
virtdirlistfile=${basefilename}.virtdir.txt
if [ -s ${virtdirlistfile} ]
then

	while read virtdir
	do
		echo "Creating virtual directory ${virtdir}"
		echo ""
		#perl $XLS/src/scripts/install/createVirtualDirectory.pl ${virtdir}
		declare -a varr
		IFS='|' read -ra varr <<< "$virtdir"

		# required parameters
		paramlist="-appName ${varr[0]} -appPath ${varr[1]}"
		if [ -n "${varr[2]}" ]
		then
			paramlist="${paramlist} -appIsolation ${varr[2]}"
		fi
		if [ -n "${varr[3]}" ]
		then
			paramlist="${paramlist} -readAccess ${varr[3]}"
		fi
		if [ -n "${varr[4]}" ]
		then
			paramlist="${paramlist} -scriptAccess ${varr[4]}"
		fi
		if [ -n "${varr[5]}" ]
		then
			paramlist="${paramlist} -execAccess ${varr[5]}"
		fi
		if [ -n "${varr[6]}" ]
		then
			paramlist="${paramlist} -defaultDoc ${varr[6]}"
		fi
		if [ -n "${varr[7]}" ]
		then
			paramlist="${paramlist} -requireHttps ${varr[7]}"
		fi

		powershell $XLS/src/scripts/install/createVirtualDirectory.ps1 ${paramlist}
		if [ $? != 0 ]
		then
			echo "Error creating virtual directory ${virtdir}, Exiting"
			exit 1
		fi
	done < ${virtdirlistfile}

fi



# Check if we need to [re-]register any assemblies in the GAC
gacutillistfile=${basefilename}.gacutil.txt
if [ -s ${gacutillistfile} ]
then

	while read gacutil1
	do
		echo "Adding ${gacutil1} to the GAC"
		cd c:/alacra/lib
		if [ $? != 0 ]
		then
			echo "could not cd to lib directory, Exiting"
			exit 1
		fi

		# remove old one
		echo "../gacutil.exe /u ${gacutil1}"
		../gacutil.exe /u ${gacutil1}

		# add new one
		echo "../gacutil.exe /i ${gacutil1}.dll"
		../gacutil.exe /i ${gacutil1}.dll
		if [ $? != 0 ]
		then
			echo "Error adding ${gacutil1} to the GAC, Exiting"
			exit 1
		fi
		cd c:/
	done < ${gacutillistfile}

fi

gacutil4listfile=${basefilename}.gacutil4.txt
if [ -s ${gacutil4listfile} ]
then

	while read gacutil41
	do
		echo "Adding ${gacutil41} to the GAC"
		cd c:/alacra/lib
		if [ $? != 0 ]
		then
			echo "could not cd to lib directory, Exiting"
			exit 1
		fi

		# remove old one
		echo "../gacutil4.exe /u ${gacutil41}"
		../gacutil4.exe /u ${gacutil41}

		# add new one
		echo "../gacutil4.exe /i ${gacutil41}.dll"
		../gacutil4.exe /i ${gacutil41}.dll
		if [ $? != 0 ]
		then
			echo "Error adding ${gacutil41} to the GAC, Exiting"
			exit 1
		fi
		cd c:/
	done < ${gacutil4listfile}

fi



# Check if this package has a POST-installation shell script, and if so, run it
if [ -s ${basefilename}.post.sh ]
then
	echo "---- Running POST installation script ${basefilename}.post.sh ---------------------------"
	chmod u+x ${basefilename}.post.sh
	${basefilename}.post.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from ${basefilename}.post.sh, Exiting"
		exit 1
	fi
	echo "---- Done w/POST installation script ${basefilename}.post.sh ---------------------------"
fi


if [ -s ${basefilename}.post.ps1 ]
then
	echo "---- Running POST installation script ${basefilename}.post.ps1 ---------------------------"
	powershell ${basefilename}.post.ps1 -pkgName ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from ${basefilename}.post.ps1, Exiting"
		exit 1
	fi
	echo "---- Done w/POST installation script ${basefilename}.post.ps1 ---------------------------"
fi

exit 0
