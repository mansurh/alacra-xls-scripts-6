# Routine to unpack builds

shname=lounpack.sh

# Parse the command line arguments
#	1 = date YYYYMMDD

if [ $# != 1 ]
then
	echo "Usage: ${shname} YYYYMMDD"
	exit 1
fi

# Save the runstring parameters in local variables
thedate=$1

distlistfile=distribution.lo.${thedate}.lst
disttarzfile=distribution.lo.${thedate}.tar.Z
distunpacklogfile=distribution.lo.${thedate}.log

undotarfile=undo.${thedate}.tar
undologfile=undo.${thedate}.log

# Make undo file
if [ ! -s ${distlistfile} ]
then
	echo "Distribution List file ${distlistfile} not found, exiting"
	exit 1
fi

if [ -s ${undotarfile} ]
then
	echo "Undo file ${undotarfile} already exists, exiting"
	exit 1
fi

if [ -s ${undotarfile}.Z ]
then
	echo "Undo file ${undotarfile}.Z already exists, exiting"
	exit 1
fi

echo "Creating undo tar file ${undotarfile}"
#read zz9
#echo "Creating undo tar file ${undotarfile}"
cat ${distlistfile} | tar -cvf ${undotarfile} -  > ${undologfile} 2>&1
if [ ! -e ${undotarfile} ]
then
	echo "Undo tar file not created, exiting"
	exit 1
fi

echo "Compressing undo tar file ${undotarfile}"
#read zz9
#echo "Compressing undo tar file ${undotarfile}"
compress -f ${undotarfile}
if [ ! -e ${undotarfile}.Z ]
then
	echo "Undo tar file not compressed, exiting"
	exit 1
fi

echo "Unpacking distribution file ${disttarzfile}"
#read zz9
#echo "Unpacking distribution file ${disttarzfile}"
zcat ${disttarzfile} | tar -xvf -  > ${distunpacklogfile} 2>&1

exit 0
