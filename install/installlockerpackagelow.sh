# Command file to transfer and install a software update onto a locker server
# Usage: installlockerpackagelow.sh packagename

# Parse the command line arguments.
#	1 = packagename
#	2 = optional 'noundo' flag
if [ $# -lt 1 ]
then
	echo "Usage: installlockerpackagelow.sh packagename [noundo]"
	exit 1
fi

# Set up vars
packagename=$1

# optional 'no undo' flag - means do not make undo tar file
if [ $# -gt 1 ]
then
	undoflag=$2
fi

basefilename=distribution.lo.${packagename}

# work in root dir
progname=${packagename%%[0-9]*}

installdir=""
if [ $progname = "lockersynchronization" ]
then
	installdir=c:/usr/local/bin
else
	installdir=c:/winnt/system32/${progname}
fi

cd c:/

# build an ftp command file
rm -f command.ftp
echo "user administrator qt123" > command.ftp
echo "binary" >> command.ftp
echo "mget /f/${basefilename}.*" >>command.ftp
echo "bye" >>command.ftp

# Ftp the compressed data file
ftp -i -n -s:command.ftp data6.xls.com
# Test the exit status of ftp
if [ $? != 0 ]
then
	echo "Error in ftp, Exiting"
	rm -f command.ftp
	exit 1
fi

rm -f command.ftp

# Verify that we got the data file
if [ ! -s ${basefilename}.tar.Z ]
then
	echo "${basefilename}.tar.Z not Downloaded or Zero Length, Exiting"
	exit 1
fi

if [ ! -s ${basefilename}.lst ]
then
	echo "${basefilename}.lst not Downloaded or Zero Length, Exiting"
	exit 1
fi

# Check if this package has a PRE-installation shell script, and if so, run it
if [ -s ${basefilename}.pre.sh ]
then
	echo "---- Running PRE installation script ${basefilename}.pre.sh ---------------------------"
	./${basefilename}.pre.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from ${basefilename}.pre.sh, Exiting"
		exit 1
	fi
	echo "---------------------------------------------------------------------------------------"
fi

# unpack package archive
if [ "$undoflag" = "noundo" ]
then
	# unpack with no undo file
	qlounpack.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from qlounpack.sh, Exiting"
		exit 1
	fi
else
	# create undo file, unpack
	lounpack.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from lounpack.sh, Exiting"
		exit 1
	fi
fi

# Check for problems with any of the files
distunpacklogfile=${basefilename}.log

echo "---- Checking Unpack log -------------------------------------------"
grep cannot ${distunpacklogfile}
if [ $? != 1 ]
then
	echo "Error unpacking the above file(s)..."
	echo "Setting write privilege on all files under ${installdir} directory"
	chmod -R +w ${installdir}/*
	echo "Unpacking archive again..."

	qlounpack.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from qlounpack.sh, Exiting"
		exit 1
	fi

	echo ""
	echo "---- Re-checking Unpack log again ----------------------------------"

	grep cannot ${distunpacklogfile}
	if [ $? != 1 ]
	then
		echo "Error unpacking the above file(s), Exiting"
		exit 1
	fi
fi

echo "No problems extracting files.  Complete extract log follows:"
echo ""
cat ${distunpacklogfile}

# Check if this package has a POST-installation shell script, and if so, run it
if [ -s ${basefilename}.post.sh ]
then
	echo "---- Running POST installation script ${basefilename}.post.sh ---------------------------"
	./${basefilename}.post.sh ${packagename}
	if [ $? != 0 ]
	then
		echo "Error from ${basefilename}.post.sh, Exiting"
		exit 1
	fi
	echo "-----------------------------------------------------------------------------------------"
fi


exit 0
